function getList_Meetings() {
	  var selectStr = "$select=Title,ID,IsMeetingContent,MeetingDate";
	  var orderbyStr = "$orderby=MeetingDate%20desc";
	  var filterbyStr = "$filter=IsMeetingContent%20eq%201";
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?" + selectStr + "&" + orderbyStr + "&" + filterbyStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadMeetings,
			error: errorHandler
		  }
	  );	
}

function loadMeetings(data){
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	$("#q_meeting_select").append($("<option>").attr({"data-id": "0"}).text("Show All"));
	$(results).each(function(){
	  var data_id = $(this)[0].ID;
	  var meeting_date = $(this)[0].MeetingDate;
	  var meeting_date = new Date($(this)[0].MeetingDate);
	  var month_index = meeting_date.getMonth();
	  var month_names = ["January", "February", "March","April", "May", "June", "July","August", "September", "October","November", "December"];
	  var formatted_meeting_date = month_names[month_index] + " " + meeting_date.getDate() + ", " + meeting_date.getFullYear();
	  var opt = $("<option>").text(formatted_meeting_date);
	  opt.attr({"data-id": data_id, "data-date": $(this)[0].MeetingDate});
	  $("#q_meeting_select").append(opt);
	});
}

function listMeetings(){
	var filterbyStr = "";
	$("#q_meeting_select").on('change', function () {
			$("#q_meetinglist").html("");
		    $("#q_selectmeetingdt").text($(this).val());
			var option_selected = $(this).find("option:selected");
			var date = option_selected.attr("data-date");
			var selectStr = "$select=ID,Title,QOSTextContent,CategoryValue,FileRef,FileLeafRef";
			var orderbyStr = "$orderby=MeetingDate%20desc";

			if(option_selected.text() == "Show All"){
				filterbyStr = "$filter=CategoryValue%20ne%20%27Report%27%20and%20IsMeetingContent%20eq%201";
			} else {
				var startDate = date.substr(5,2) + "/" + date.substr(8,2) + "/" + date.substr(0,4);
				var tempDate = new Date(startDate);
				tempDate.setDate(tempDate.getDate() + 1);
				var endDate = (tempDate.getMonth() + 1) + "/" + tempDate.getDate() + "/" + tempDate.getFullYear();
				filterbyStr = "$filter=" + encodeURIComponent("CategoryValue ne 'Report' and IsMeetingContent eq 1 and (MeetingDate ge '" + startDate + "' and ReportDataDate lt '" + endDate + "')");
			}
			var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + filterbyStr;
			var executor = new SP.RequestExecutor(catUrl);
			console.log(itemsUrl);
			executor.executeAsync(
			  {
				url: itemsUrl,
				method: "GET",
				headers: { "Accept": "application/json; odata=verbose" },
				success: loadGeneral,
				error: errorHandler
			  }
			);
	});
}

function loadGeneral(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	for (var i = 0; i < results.length; i++ ){	
		var fileicon = getIconFile(results[i].FileLeafRef);
		var cellNode = $("<div>").addClass("grid-80");
		var divNode2 = $("<div>").addClass("q_rt_rowcell-1-title");
		var aNode1 = $("<a/>").attr({"href" : results[i].FileRef, "title" : results[i].FileLeafRef, "target" : "_blank"}).text(results[i].Title + "  "); //"q_weblinksprite_med"
		var imgNode = $("<img/>").attr({"src" : fileicon});
		aNode1.append(imgNode);
		divNode2.append(aNode1);
		var divNode3  = $("<div>").addClass("q_spriteindent-lefttext").html(results[i].QOSTextContent);
		cellNode.append(divNode2);
		cellNode.append(divNode3);
		$("#q_meetinglist").append(cellNode);
	}
}