// Javascript library for adding Page Measure Groupings
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/js/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/js/";
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
		$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}
 
jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getMeasureGroupsOptionList == 'function' && typeof loadProfileData == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () { console.log("Initiating SP.ClientContext") });
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			overrideSharepointStyles();
			loadProfileData();
			getMeasureGroupsOptionList();
			var intervalStart = setInterval(setWindowValues, 500);	
			function setWindowValues(){
				if(loadProfileDataDone == true && loadMeasureGroupsDone == true){	
						var savebuttontext = "Save";
						var updateType = "post";
						if (mdtlid != false) {
							loadItemData(mdtlid);
							savebuttontext = "Update";
							updateType = "update";
							$("#q_tbl-listMGroupingsinput").removeClass("q_hide");
							$("#q_tbl-listMGroupings").addClass("q_hide");	
						};	
						$("#q_taggedtext").spHtmlEditor({version: "onprem" });						
						$('#q_taggedtext').click(function() {setRibbonGroupVisibility_FT();});
						setRibbonGroupVisibility_PT();
						$("#q_defEffectivedate, #q_defEnddate").datepicker();
						$("#q_cancelbutton").click(function(){ cancelModalWindow() });	
						$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation(updateType)});
						$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
						$("#q_hideinputfields").removeClass("q_hide");
						$(".se-pre-con").fadeOut("slow");
						clearInterval(intervalStart);
				   }
				}
		},"sp.ui.dialog.js");
	},"sp.js");
	
}

function fieldValidation(updateType){	
	// required values are Text Category, Title, and Content Text Block.
	var isValid = true;
	$("#q_errormessage").text("");
	if ($("#q_listMGroups option:selected").val() == null || $("#q_listMGroups option:selected").val() == "") {	
			var error_node = $('<p>').text('A Measure Group Name must be supplied.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	};	
	if ($('#q_defEffectivedate').val() == null || $('#q_defEffectivedate').val() == "") {
			var error_node = $('<p>').text('Effective Date is required');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 
	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		postItem(updateType);
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}

function postItem(updateType) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Groupings'); 
	if (updateType == "post") {
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	} else {
		this.oListItem = oList.getItemById(curId);
	}
	
	var mversionLookupSingle = new SP.FieldLookupValue();  
	var get_id_mversion = $("#q_listMGroups option:selected").val();
   	mversionLookupSingle.set_lookupId(get_id_mversion);	
	this.oListItem.set_item('LookupMeasureGroup', mversionLookupSingle);
	this.oListItem.set_item('MeasureGroupValue', $("#q_listMGroups option:selected").text());
	this.oListItem.set_item('Definition', $('#q_taggedtext').spHtmlEditor("gethtml"));
	var defEffDate = new Date($('#q_defEffectivedate').val());
	this.oListItem.set_item('DefinitionEffectiveDate', defEffDate.toISOString());
	var deftemp2 = new Date($('#q_defEnddate').val());
	if (deftemp2 != "Invalid Date") { var defEndDate = new Date($('#q_defEnddate').val());} else { var defEndDate = new Date('01/01/1900'); }	
	this.oListItem.set_item('DefinitionEndDate', defEndDate.toISOString());
	if (profiledata[0] != "" && profiledata[0] != null) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1] != "" && profiledata[1] != null) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2] != "" && profiledata[2] != null) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3] != "" && profiledata[3] != null) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4] != "" && profiledata[4] != null) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5] != "" && profiledata[5] != null) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	if (profiledata[6] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[6]));	
		this.oListItem.set_item('LookupAOW', newId);
	}
	if (profiledata[7] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[7]));	
		this.oListItem.set_item('LookupInitiative', newId);
	}
	if (profiledata[8] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[8]));	
		this.oListItem.set_item('LookupProgram', newId); 
	}
	if (profiledata[9] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[9]));	
		this.oListItem.set_item('LookupSubProgram', newId); 
	}
	this.oListItem.set_item('PublishingApproval', "Draft"); 
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
} 

function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function onItemPostQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function loadItemData(mdtlid) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Groupings');
	this.oListItem = oList.getItemById(mdtlid);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceeded(sender, args) {
	$('#q_taggedtext .edit-content').html(oListItem.get_item("Definition"));
	var defEffectivedate = new Date(oListItem.get_item("DefinitionEffectiveDate"));
	var formatted_defEffectivedate = (defEffectivedate.getMonth() + 1) + "/" + defEffectivedate.getDate() + "/" + defEffectivedate.getFullYear();
	$("#q_defEffectivedate").val(formatted_defEffectivedate);
	var defEnddate = new Date(oListItem.get_item("DefinitionEndDate"));
	var formatted_defEnddate = (defEnddate.getMonth() + 1) + "/" + defEnddate.getDate() + "/" + defEnddate.getFullYear();
	if (formatted_defEnddate == '1/1/1900' || formatted_defEnddate == '12/31/1969') { formatted_defEnddate = ""; }
	$("#q_defEnddate").val(formatted_defEnddate);
	
	if (oListItem.get_item('LookupMeasureGroups') != null) {
		var mgrpId = oListItem.get_item("LookupMeasureGroup").get_lookupId();
		$("#q_listMGroups option").each(function(){
			if($(this).val() == mgrpId){
				$(this).prop("selected",true);
			} 
		});
	}
}
