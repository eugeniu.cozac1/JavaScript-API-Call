// Javascript library for adding Page Measure Details
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
}
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}

jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof getDeliverablesOptionList == 'function' && typeof loadProfileData == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
				overrideSharepointStyles();
				getWebUserData();
				loadProfileData();
				getDeliverablesOptionList();
				var intervalStart = setInterval(setWindowValues, 2000);	
				function setWindowValues(){
					if(loadProfileDataDone == true && loadDeliverablesOptionsDone == true){		
						var savebuttontext = "Save";
						var updateType = "post";
						if (curId != false) {	
							loadItemData(curId);
							savebuttontext = "Update";
							updateType = "update";	
						};
						$("#q_defEffectivedate, #q_defEnddate").datepicker();
						$("#q_taggedtext").spHtmlEditor({version: "onprem" });						
						$('#q_taggedtext').on('click', function() {	
						 var intervalEdit = setInterval(setWindowEdit, 650);
						 function setWindowEdit(){
							var editingTools = $("li[id='Ribbon.EditingTools.CPEditTab-title'] a");
							if (editingTools.length > 0){
							 	 editingTools[0].click();
							     clearInterval(intervalEdit);
							 }
						  }
						});	 
						var intervalWebPart = setInterval(setWindowWebPart, 500);	
						function setWindowWebPart(){
							var webPartPage = $("li[id='Ribbon.WebPartPage-title'] a");
							if (webPartPage.length > 0){
						  		webPartPage[0].click();
								clearInterval(intervalWebPart);
							}
						}
						$("#q_cancelbutton").click(function(){ cancelModalWindow() });
						$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation(updateType)});
						$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
						$("#q_hideinputfields").removeClass("q_hide");
						$(".se-pre-con").fadeOut("slow");
						clearInterval(intervalStart);
					 }
				  }


		},"sp.ui.dialog.js");
	},"sp.js");
}

function fieldValidation(updateType){	
	var isValid = true;
	$("#q_errormessage").text("");
	if ($('#q_measurename').val() == null || $('#q_title').val() == "") {
			var error_node = $('<p>').text('Measure Name is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 
	var othervar = $('#q_taggedtext').spHtmlEditor("gethtml");
	if (othervar.length < 7) {
			var error_node = $('<p>').text('A general description of this measure is required. Web audiences will see this description.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	};
	if (parseInt($("#q_listDeliverables option:selected").val()) < 0) {
		var error_node = $('<p>').text('A data deliverable profile must be selected.');
		$("#q_errormessage").append(error_node);
		isValid = false;
	}
	if ($('#q_defEffectivedate').val() == "") { 
			var error_node = $('<p>').text('Effective Date is required');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 	 	
	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		postItem(updateType);
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}

function postItem(updateType) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Details'); 
	if(updateType == "post"){
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	}else{
		this.oListItem = oList.getItemById(curId);
	}
	var htmlStr = $('#q_taggedtext').spHtmlEditor("gethtml");	
	var measureNameValue = $('#q_measurename').val();
	var measureTypeValue = $('#q_measuretype').val();
	var measureCodeValue = $('#q_measurecode').val();
	this.oListItem.set_item('MeasureNameValue', measureNameValue);
	this.oListItem.set_item('MeasureTypeValue', measureTypeValue);
	this.oListItem.set_item('MeasureCodeValue', measureCodeValue);
	this.oListItem.set_item('Definition', htmlStr);
	var delId = new SP.FieldLookupValue(); 
	delId.set_lookupId($("#q_listDeliverables option:selected").val());	// ID for default deliverable profile
	this.oListItem.set_item('Deliverables', delId);
	this.oListItem.set_item('DeliverableNameValue', $("#q_listDeliverables option:selected").text());
	var delname = $("#q_listDeliverables option:selected").text();
	var defEffDate = new Date($('#q_defEffectivedate').val());
	this.oListItem.set_item('DefinitionEffectiveDate', defEffDate.toISOString());
	var deftemp2 = new Date($('#q_defEnddate').val());
	if (deftemp2 != "Invalid Date") { var defEndDate = new Date($('#q_defEnddate').val());} else { var defEndDate = new Date('01/01/1900'); }	
	this.oListItem.set_item('DefinitionEndDate', defEndDate.toISOString());
	this.oListItem.set_item('PublishingApproval', "Draft");
	
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
}

function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){  $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	if (curId === false) {
		getNewIDforMeasure();
	}
	updateMeasureGroupings();
}

function onItemPostQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function getNewIDforMeasure () {
	var measureName = $('#q_measurename').val();
	var selectStr = "$select=ID,MeasureNameValue";
	var filterby = "$filter=MeasureNameValue%20eq%20%27" + measureName + "%27";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Details')/items?" + selectStr + "&" + filterby;
	$.ajax({
		url: itemsUrl,
		method: "GET",
		headers: { "Accept": "application/json; odata=verbose" },
		cache: false,
		async: false,
		success: function(data) {
  			var results = (data.d.results == null) ? new Array(data.d) : data.d.results;
			curId =  results[0].Id;
		},
		error: function (data) {
			alert("no match found");
		}
	});
	
}

function updateMeasureGroupings () {
	var delId = $("#q_listDeliverables option:selected").val();
	var selectStr = "$select=ID,LookupMeasureDetailId";
	var filterby = "$filter=MeasureGroupValue%20eq%20%27" + "default_del" + delId + "%27";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groupings')/items?" + selectStr + "&" + filterby;
	$.ajax({
		url: itemsUrl,
		method: "GET",
		headers: { "Accept": "application/json; odata=verbose" },
		cache: false,
		async: false,
		success: function(data) {
  			var results = (data.d.results == null) ? new Array(data.d) : data.d.results;
			var idArray = [];
			if (results.length == 0) { 
				postItemMG('post', '', idArray); 
			} else {
				var mgId =  results[0].Id;
				grpliststr = results[0].LookupMeasureDetailId;
  				var idArray = (grpliststr.results == null) ? new Array(grpliststr) : grpliststr.results;
				postItemMG('update', mgId, idArray);
			}
		},
		error: function (data) {
			alert("no match found");
		}
	});
}

function postItemMG(updateTypeMG, mgId, idArray) {	
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Groupings'); 
	if(updateTypeMG == "post"){
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	} else {
		this.oListItem = oList.getItemById(mgId);
	}
	var lookups = [];
	if (idArray.length == 0 ) { 
		var lookupValue = new SP.FieldLookupValue();  
		lookupValue.set_lookupId(parseInt(curId));  
		lookups.push(lookupValue);
	} else {
		var matchfound = false;
		for (var i = 0; i < idArray.length; i++) { 
			var newId = idArray[i];
			if (newId == curId) { matchfound = true;}
			var lookupValue = new SP.FieldLookupValue();  
			lookupValue.set_lookupId(parseInt(newId));  
			lookups.push(lookupValue);
		}
		if (matchfound == false) {
			var newValue = new SP.FieldLookupValue();  
			newValue.set_lookupId(parseInt(curId));  
			lookups.push(newValue); 			
		}
    }
    this.oListItem.set_item('LookupMeasureDetail', lookups);
	var grpId = new SP.FieldLookupValue(); 
	grpId.set_lookupId(default_mgId);	// ID for default measure group item
	this.oListItem.set_item('LookupMeasureGroup', grpId);
	this.oListItem.set_item('Title', 'Measure Grouping');
	var deltitle = "default_del" + $("#q_listDeliverables option:selected").val();
	this.oListItem.set_item('MeasureGroupValue', deltitle);
	if (profiledata[0] != "" && profiledata[0] != null) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1] != "" && profiledata[1] != null) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2] != "" && profiledata[2] != null) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3] != "" && profiledata[3] != null) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4] != "" && profiledata[4] != null) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5] != "" && profiledata[5] != null) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	if (profiledata[6] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[6]));	
		this.oListItem.set_item('LookupAOW', newId);
	}
	if (profiledata[7] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[7]));	
		this.oListItem.set_item('LookupInitiative', newId);
	}
	if (profiledata[8] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[8]));	
		this.oListItem.set_item('LookupProgram', newId); 
	}
	if (profiledata[9] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[9]));	
		this.oListItem.set_item('LookupSubProgram', newId); 
	}
	this.oListItem.set_item('PublishingApproval', "Draft");
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onMGItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onMGQueryFailed)
	);

}

function onMGQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function onMGItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){ alert("Measure Grouping has been updated");}	
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function loadItemData(curId) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Details');
	this.oListItem = oList.getItemById(curId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceeded(sender, args) {
	$('#q_taggedtext .edit-content').html(oListItem.get_item("Definition"));
	$("#q_measurename").val(oListItem.get_item("MeasureNameValue"));
	$("#q_measuretype").val(oListItem.get_item("MeasureTypeValue"));
	$("#q_measurecode").val(oListItem.get_item("MeasureCodeValue"));
	var defEffectivedate = new Date(oListItem.get_item("DefinitionEffectiveDate"));
	var formatted_defEffectivedate = (defEffectivedate.getMonth() + 1) + "/" + defEffectivedate.getDate() + "/" + defEffectivedate.getFullYear();
	$("#q_defEffectivedate").val(formatted_defEffectivedate);
	var defEnddate = new Date(oListItem.get_item("DefinitionEndDate"));
	var formatted_defEnddate = (defEnddate.getMonth() + 1) + "/" + defEnddate.getDate() + "/" + defEnddate.getFullYear();
	if (formatted_defEnddate == '1/1/1900' || formatted_defEnddate == '12/31/1969') { formatted_defEnddate = ""; }
	$("#q_defEnddate").val(formatted_defEnddate);
	if (oListItem.get_item('Deliverables') != null) {
		var deltypeId = oListItem.get_item("Deliverables").get_lookupId();
		$("#q_listDeliverables option").each(function(){
			if($(this).val() == deltypeId){
				$(this).prop("selected",true);
			} 
		});
	}
}
