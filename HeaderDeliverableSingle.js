function getHeader_DeliverableSingle() {
	  var selectStr = "$select=ID,QOSTextContent,IsPHI,CategoryValue,RptFrequency,DeliveryMethod,DeliverableNameValue,ContactName/ID,ContactName/Title,ContactName/EMail&$expand=ContactName/ID,ContactName/Title,ContactName/EMail";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")" + "?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListDeliverableSingle,
			error: errorHandler
		  }
	  );		
}

function loadListDeliverableSingle(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var IsPHI = results[0].IsPHI;
	var containsPHI = "";
	if(IsPHI == true){
		containsPHI = "Contains PHI and/or confidential information";
	}else{
		containsPHI = "Does not contain PHI and/or confidential information";
	}
	$("#q_header-deliverableSingle").addClass("q_banner");
    var wrappDiv = $("<div/>");
	var subHead = $("<div/>").addClass("q_subhead");
	var spriteEmail = $("<div/>").addClass("q_sprite_email");
	var title = $("<h1/>").text(results[0].DeliverableNameValue);
	var subHeadnoSprite = $("<div/>").addClass("q_subhead q_nosprite-padleft q_notopmargin");
	var related = $("<div/>").addClass("q_related q_add-margin-bottom");
	var nopaddingtop = $("<h3/>").addClass("q_nopaddingtop");
	var a_1 = $("<a/>").attr({"title" : " Click to view Measure Measure Definitions", "href" : pagesUrl + "rptmeasures.aspx?carea=" + contentarea + "&curId=" + curId}).text("Measure Definitions");
	var a_2 = $("<a/>").attr({"title" : " Click to view Measure Resources", "href" : pagesUrl + "topicresources.aspx?carea=" + contentarea + "&curId=" + curId}).text("Resources");
	var supportedBy = $("<h4/>").text("Supported by");
	var supportUl = $("<ul/>");
	var supportli = $("<li/>");
	var supporta = $("<a/>").attr({"target": "_blank", "title" : " Click to send email to "+results[0].ContactName, "href" : "mailto:"+ results[0].ContactName.EMail}).text(results[0].ContactName.Title);
	var supportp = $("<p/>").text("Analytic Consulting");
	$("#q_delcontacts").addClass("q_contacts_nobackground");
	$("#q_deldetails").addClass("q_textbox");
	var textbox_div = $("<div/>");
	var textbox_ul = $("<ul/>").addClass("q_detailsbox");
	var text_liCategoryValue = $("<li/>");
	var text_spanCategoryValue = $("<span/>").text(results[0].CategoryValue);
	var text_liRptFrequency = $("<li/>");
	var text_spanRptFrequency = $("<span/>").text("Produced " + results[0].RptFrequency);
	var text_liDeliveryMethod = $("<li/>");
	var text_spanDeliveryMethod = $("<span/>").text("Delivered via " + results[0].DeliveryMethod);
	var text_liIsPHI = $("<li/>");
	var text_spanIsPHI = $("<span/>").text(containsPHI);
	text_liCategoryValue.append(text_spanCategoryValue);
	textbox_ul.append(text_liCategoryValue);
	text_liRptFrequency.append(text_spanRptFrequency);
	textbox_ul.append(text_liRptFrequency);
	text_liDeliveryMethod.append(text_spanDeliveryMethod);
	textbox_ul.append(text_liDeliveryMethod);
	text_liIsPHI.append(text_spanIsPHI);
	textbox_ul.append(text_liIsPHI);
	textbox_div.append(textbox_ul);
    $("#q_deldetails").append(textbox_div);
	$("#q_delcontacts").append(supportedBy);
	supportli.append(supporta);
	supportli.append(supportp);
	supportUl.append(supportli);
    $("#q_delcontacts").append(supportUl);
	
	spriteEmail.append(title);
	subHead.append(spriteEmail);
	wrappDiv.append(subHead);
	
	nopaddingtop.append(a_1);
	nopaddingtop.append("&nbsp;&nbsp;|&nbsp;&nbsp;");
	nopaddingtop.append(a_2);
	related.append(nopaddingtop);
	subHeadnoSprite.append(related);
	wrappDiv.append(subHeadnoSprite);
	$("#q_header-deliverableSingle").append(wrappDiv);
	$("#q_listdeldesc").append($("<div>").addClass("q_dividerwide"));
	$("#q_listdeldesc").append(results[0].QOSTextContent);
}

