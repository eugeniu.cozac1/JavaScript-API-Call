function getDeliverables_Web() {
	  var orderbyStr = "$orderby=DeliveryMethod%20desc,DeliverableNameValue,DeliverableNavString";
	  var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items?" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListDeliverable_Web,
			error: errorHandler
		  }
	  );		
}

function loadListDeliverable_Web(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var nullstr;
	var last_delmethod = "";
		
	for (var i = 0; i < results.length; i++ ){	
		var idweb = "q_items_delweb_" + i;
		if (last_delmethod.indexOf(results[i].DeliveryMethod) < 0) {
			var divNode = $("<div>").attr("id", idweb).addClass("grid-100");
			if (last_delmethod != "" ) {
				divNode.append($("<div>").addClass("q_dividerwide"));
			} 
			$("#q_deliverables-web").append(divNode);
			setCategoryGrp_WebDeliverables("#" + idweb, results[i].DeliveryMethod);
			last_delmethod = results[i].DeliveryMethod;
		} else {
			var divNode = $("<div>").attr("id", idweb).addClass("grid-100");
			$("#q_deliverables-web").append(divNode);
			setCategoryGrp_WebDeliverables("#" + idweb, nullstr);
		}
		setContentRow_WebDeliverables('#' + idweb, results[i].DeliverableNavString, results[i].DeliverableNameValue, results[i].PageNameValue, results[i].QOSTextContent, results[i].RptFrequency, results[i].IsPHI);
	}
}

function setCategoryGrp_WebDeliverables(idweb, catvalue) {
	switch (catvalue) {
		case "Web Document":
			var catTitle = "Downloadable Reports";
			break;
		case "Email":
			var catTitle = "Via Email";
			break;
		case "Web Page":
			var catTitle = "In-page Views";
			break;
		default:
	}
	var cellNode = $("<div>").addClass("grid-20");
	var contentNode = $("<div>").addClass("q_rt_rowcell-1-title q_add-margin-right-12").text(catTitle);
	cellNode.append(contentNode);
	$(idweb).append(cellNode);
}

function setContentRow_WebDeliverables (idweb, delnavstr, delname, pagename, content, frequency, PHIflag) {
	var linkhref = pagesUrl + pagename + "?carea=" + contentarea + "&delnav=" + delnavstr;
	var emText = "Produced " + frequency;
	if (PHIflag == true) { emText = emText + "  |  Contains PHI"; }
	var cellNode = $("<div>").addClass("grid-80 q_add-margin-bottom");
	var spriteNode = $("<div>").addClass("q_spriteindent-med");
	var node1 = $("<div>").addClass("q_rt_rowcell-2-title").append($("<a>").addClass("q_weblinksprite_med").attr({"href" : linkhref,"title" : "Click to view details and specific version of the " + delname, "target" : "_self"} ).text(delname));
	var spanNode1 = $("<span>").addClass("q_readmore");	
	var aNode2 = $("<a>").addClass("q_viewmore").attr({"href" : linkhref, "title" : "Click to view details"}).text("More").append($("<span>").addClass("q_arrowsprite"));

	spanNode1.append(aNode2);
	node1.append(spanNode1);
	
	var clipNode = $("<div>").addClass("q_clipline").append($("<div>").html(content));
	var emNode = $("<div>").append($("<em>").text(emText));
	var node2 = $("<div>").addClass("q_rt_rowcell-2-body q_spriteindent-lefttext").append(emNode, clipNode);
	
	spriteNode.append(node1, node2);
	cellNode.append(spriteNode);

	$(idweb).append(cellNode);
}
