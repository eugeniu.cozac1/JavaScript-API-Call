// Javascript library for adding Page WebLInks Items	
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/jstest/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}

jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof getDeliverablesOptionList == 'function' && typeof getMeasureGroupingsOptionList == 'function' && typeof loadProfileData == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}	
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			overrideSharepointStyles();
			getWebUserData();
			getDeliverablesOptionList();
			getMeasureGroupingsOptionList();
			loadProfileData();
			var intervalStart = setInterval(setWindowValues, 500);	
			function setWindowValues(){
				if(loadProfileDataDone == true && loadMeasureGroupingsDone == true && loadDeliverablesOptionsDone == true){	
					$('#q_IsMeetingContent').on('click', function(){
						if ($(this).is(':checked')){
							$("#q_tbl-IsMeetingDate").removeClass("q_hide");
						}else{
							$("#q_tbl-IsMeetingDate").addClass("q_hide");
						}
					});
					var savebuttontext = "Save";
					var updateType = "post";
					if (curId != false) {	
						loadItemData(curId);
						savebuttontext = "Update";
						updateType = "update";	
					};
					$("#q_taggedtext").spHtmlEditor({version: "onprem" });						
					$('#q_taggedtext').click(function() {setRibbonGroupVisibility_FT();});
					setRibbonGroupVisibility_PT();	
					$("#q_meetingDate").datepicker();
					$("#q_cancelbutton").click(function(){ cancelModalWindow() });
					$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation(updateType)});
					$(".se-pre-con").fadeOut("slow");
					$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
					$("#q_hideinputfields").removeClass("q_hide");
					clearInterval(intervalStart);
				}
			}
		},"sp.ui.dialog.js");
	},"sp.js");
}

function fieldValidation(updateType){	
	var isValid = true;
	$("#q_errormessage").text("");
	if ($('#q_weburl').val() == null || $('#q_weburl').val() == ""  || $('#q_weburl').val().indexOf('http') < 0) {
			var error_node = $('<p>').text('Link is required and must be a complete URL beginning with "http://" or "https://"');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 
	if ($('#q_urltitle').val() == null || $('#q_urltitle').val() == "") {
			var error_node = $('<p>').text('Display Text for URL is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	};
	if ($('#q_IsMeetingContent').is(':checked')) { 
		if ($('#q_meetingDate').val() == "") { 
				var error_node = $('<p>').text('Meeting Date is required');
				$("#q_errormessage").append(error_node);
				isValid = false;
		}; 	
	}; 
	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		postItem(updateType);
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}
			
function postItem(updateType){
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Web Links'); 
	if (updateType == "post") {
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	} else {
		this.oListItem = oList.getItemById(curId);
	}
	var hyperLink = new SP.FieldUrlValue();
	hyperLink.set_url($('#q_weburl').val());  
	hyperLink.set_description($('#q_urltitle').val()); 
	var htmlStr = $('#q_taggedtext').spHtmlEditor("gethtml");

	this.oListItem.set_item('WebURL', hyperLink);
	this.oListItem.set_item('Comments', htmlStr); 

	
	if (parseInt($("#q_listDeliverables option:selected").val()) > 0 && parseInt($("#q_listDeliverables option:selected").val()) != default_delId ) {
		var deltype_text = $("#q_listDeliverables option:selected").text();
		var deltypeId = new SP.FieldLookupValue();
		deltypeId.set_lookupId(parseInt($("#q_listDeliverables option:selected").val()));
		var delnavstr = "del" + $("#q_listDeliverables option:selected").val();
		this.oListItem.set_item('DeliverableNameValue', deltype_text);
		this.oListItem.set_item('Deliverables', deltypeId);
		this.oListItem.set_item('DeliverableNavString', delnavstr);
	} else {
		var emptyStr = "";
		var deltypeId = new SP.FieldLookupValue();
		deltypeId.set_lookupId(parseInt(default_delId));
		this.oListItem.set_item('DeliverableNameValue', emptyStr);
		this.oListItem.set_item('Deliverables', deltypeId);
		this.oListItem.set_item('DeliverableNavString', emptyStr);
	}
	if (parseInt($("#q_listMGroupings option:selected").val()) > 0 && parseInt($("#q_listMGroupings option:selected").val()) != default_mgId) {
		var mgrp_text = $("#q_listMGroupings option:selected").text();
		var mgrpId = new SP.FieldLookupValue();
		mgrpId.set_lookupId(parseInt($("#q_listMGroupings option:selected").val()));
		var mgnavstr = "mg" + $("#q_listMGroupings option:selected").val();
		this.oListItem.set_item('MeasureGroupValue', mgrp_text); 
		this.oListItem.set_item('LookupMeasureGroupings', mgrpId);
		this.oListItem.set_item('MeasureNavString', mgnavstr);
	} else {
		var emptyStr = "";
		var mgrpId = new SP.FieldLookupValue();
		mgrpId.set_lookupId(parseInt(default_mgId));
		this.oListItem.set_item('MeasureGroupValue', emptyStr); 
		this.oListItem.set_item('LookupMeasureGroupings', mgrpId);
		this.oListItem.set_item('MeasureNavString', emptyStr);
	}
	if ($('#q_IsMeetingContent').is(':checked')) { 
		var IsMC = 1;
		var meetingDate = new Date($('#q_meetingDate').val());
		this.oListItem.set_item('MeetingDate', meetingDate.toISOString());	
	}else { 
		var IsMC = 0;
	} 
	this.oListItem.set_item('IsMeetingContent', IsMC);
	if (profiledata[0] != "" && profiledata[0] != null) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1] != "" && profiledata[1] != null) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2] != "" && profiledata[2] != null) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3] != "" && profiledata[3] != null) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4] != "" && profiledata[4] != null) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5] != "" && profiledata[5] != null) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	this.oListItem.set_item('PublishingApproval', "Draft"); 
	
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
} 

function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function onItemPostQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function loadItemData(curId) {
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Web Links');
	this.oListItem = oList.getItemById(curId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceeded(sender, args) {
    $("#q_weburl").val(oListItem.get_item('WebURL').get_url());
	$("#q_urltitle").val(oListItem.get_item('WebURL').get_description());
	$("#q_taggedtext").spHtmlEditor({method: "sethtml", html: oListItem.get_item("Comments")});
	if (oListItem.get_item('Deliverables') != null) {
		var deltypeId = oListItem.get_item("Deliverables").get_lookupId();
		$("#q_listDeliverables option").each(function(){
			if($(this).val() == deltypeId){
				$(this).prop("selected",true);
			} 
		});
	}
	if (oListItem.get_item('LookupMeasureGroupings') != null) {
		var mgrpId = oListItem.get_item("LookupMeasureGroupings").get_lookupId();
		$("#q_listMGroupings option").each(function(){
			if($(this).val() == mgrpId){
				$(this).prop("selected",true);
			} 
		});
	}
	var IsMC = oListItem.get_item("IsMeetingContent");
	if(IsMC == 1){ 
		$("#q_IsMeetingContent").prop('checked', true); 
		$("#q_tbl-IsMeetingDate").removeClass("q_hide");
	}
	var meetDate = new Date(oListItem.get_item("MeetingDate"));
	var formatted_meetDate = (meetDate.getMonth() + 1) + "/" + meetDate.getDate() + "/" + meetDate.getFullYear();
	if (formatted_meetDate == '1/1/1900' || formatted_meetDate == '12/31/1969') { formatted_meetDate = ""; }
	$("#q_meetingDate").val(formatted_meetDate);
}
