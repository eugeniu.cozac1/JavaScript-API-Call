function getHeader_MeasureVersion() {
	  var selectStr = "$select=ID,MeasureNameValue,MeasureNavString,Definition";
	  var filterby = "$filter=MeasureNavString%20eq%20%27" + mdtlnav + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Measure%20Details')/items?" + selectStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadHeader_MeasureVersion,
			error: errorHandler
		  }
	  );		
}

function loadHeader_MeasureVersion(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	for (var i = 0; i < results.length; i++ ){
		var headerTitle = "Measures Definition Detail"
		writeMeasureVersionHeader(headerTitle, results[i].MeasureNameValue, results[i].Definition);
	}
}
function writeMeasureVersionHeader (headerTitle, measurename, content) {
	$("#q_header-measure").addClass("q_banner");
	var q_subhead = $("<div>").addClass("q_subhead");
	var q_nosprite = $("<div>").addClass("q_nosprite").append($("<h1>").text(headerTitle));
	var q_body_title = $("<div>").addClass("grid-100 q_rt_rowcell-1-title").append(measurename);
	var q_body_content = $("<div>").addClass("grid-100 q_rt_rowcell-2-body").append(content);
	q_subhead.append(q_nosprite);
	q_subhead.append(q_body_title);
	q_subhead.append(q_body_content);
	$("#q_header-measure").append(q_subhead);
}

