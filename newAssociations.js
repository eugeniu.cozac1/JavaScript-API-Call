// Javascript library for adding Page Text Items	
var leftDone = false;
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/js/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/js/";
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}


jQuery(document).ready(function ($) {
	$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
	$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof getDeliverablesOptionList == 'function' && typeof getMeasuresOptionList == 'function' && typeof loadProfileData == 'function' && typeof getMeasureGroupingAssocRight == 'function' && typeof getMeasureDetailsLeft == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () { console.log("Initiating SP.ClientContext") });
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			var del = "";
			overrideSharepointStyles();
			getWebUserData();
			getDeliverablesOptionList();
			getMeasuresOptionList();
			loadProfileData();
			var intervalStart = setInterval(setWindowValues, 1500);
			function setWindowValues(){
				if(loadDeliverablesOptionsDone == true && loadMeasureDetailsDone == true && loadProfileDataDone == true){
					$("#q_listMeasuresLeft").text("");
					$("#q_listDeliverables").change(function() { 
						$("#q_listMeasuresLeft option").removeClass("get_id");
						$("#q_tbl-listMgroupings").removeClass("q_hide");
						$("#q_listMeasuresLeft").text("");
						$("#q_listMeasuresRight").text("");
							var get_id = $(this).val();
							del = "default_del" + get_id;	
							getMeasureGroupingAssocRight(del);
					});
					$('#add').click(function() {  
						$(".show_hide").removeClass("q_hide");  
						return !$('#q_listMeasuresLeft option:selected').remove().appendTo('#q_listMeasuresRight');
					});  
					$('#remove').click(function() {  
						return !$('#q_listMeasuresRight option:selected').remove().appendTo('#q_listMeasuresLeft');  
					}); 
					$("#q_cancelbutton").bind("click", cancelModalWindow);
					
					$("#q_taggedtext").spHtmlEditor({version: "onprem" });
					var savebuttontext = "Save";
					var updateType = "post";
					if (curId != false) {	
						loadItemData(curId);
						savebuttontext = "Update";
						updateType = "update";	
					};
					$("#q_submitbutton").text(savebuttontext).click(function() {updateMeasureGroupings()});
					clearInterval(intervalStart);
				}
			}
		},"sp.ui.dialog.js");
	},"sp.js");
}

function updateMeasureGroupings() {
	var delId = $("#q_listDeliverables option:selected").val();
	var selectStr = "$select=ID,LookupMeasureDetailId";
	var filterby = "$filter=MeasureGroupValue%20eq%20%27" + "default_del" + delId + "%27";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groupings')/items?" + selectStr + "&" + filterby;
	$.ajax({
		url: itemsUrl,
		method: "GET",
		headers: { "Accept": "application/json; odata=verbose" },
		cache: false,
		async: false,
		success: function(data) {
  			var results = (data.d.results == null) ? new Array(data.d) : data.d.results;
			var idArray = [];
			if (results.length == 0) { 
				postNewItem('post', idArray);
			} else {
				var mgId =  results[0].Id;
				grpliststr = results[0].LookupMeasureDetailId;
  				var idArray = (grpliststr.results == null) ? new Array(grpliststr) : grpliststr.results;
				var liststring = getDeliverableNavStringList(mgId);
				postNewItem(updateType, liststring);
			}
		},
		error: function (data) {
			alert("no match found");
		}
	});
}

function getDeliverableNavStringList(mgId){
	var selectStr = "$select=DeliverableNavStringList";
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groupings')/items(" + mgId +  ")?" + selectStr;
	var delNavString = "";
	$.ajax({
		url: itemsUrl,
		method: "GET",
		headers: { "Accept": "application/json; odata=verbose" },
		cache: false,
		async: false,
		success: function(data) {
  			var results = (data.d.results == null) ? new Array(data.d) : data.d.results;
			delNavString = results[0].DeliverableNavStringList;
		},
		error: function (data) {
			alert("no match found");
		}
	});
	return delNavString;
}

function postNewItem(updateType, liststring) {	
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Groupings'); 
	if(updateType == "post"){
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	}else{
		this.oListItem = oList.getItemById(curId);
	}
	var lookupsIds = [];
	   $("#q_listMeasuresRight option").each(function(){
		   var val = $(this).attr("id");
		   lookupsIds.push(val);
	   }); 
    var lookups = [];  
    for (var ii in lookupsIds) {  
      var lookupValue = new SP.FieldLookupValue();  
      lookupValue.set_lookupId(lookupsIds[ii]);  
      lookups.push(lookupValue);  
    }  
	var del_selected = $("#q_listDeliverables option:selected").val();
    var mgrpName = "default_del" + del_selected; 
	var htmlStr = $('#q_taggedtext .edit-content').html();
	this.oListItem.set_item('MeasureGroupValue', mgrpName);
	this.oListItem.set_item('LookupMeasureDetail', lookups);
	this.oListItem.set_item('Definition', htmlStr);	
	var deliverableNavString = "del" + del_selected;
	if(liststring == null || liststring == "" || liststring == undefined){ liststring = ""};
	var resultDeliverableNavString = liststring + "; ";
	if(resultDeliverableNavString.length < 3){ resultDeliverableNavString = ""; }
	if(resultDeliverableNavString == null || resultDeliverableNavString == "" || resultDeliverableNavString.indexOf(deliverableNavString) < -1){
		this.oListItem.set_item('DeliverableNavStringList', resultDeliverableNavString + deliverableNavString);
	}
	if (profiledata[0] != "" && profiledata[0] != null) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1] != "" && profiledata[1] != null) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2] != "" && profiledata[2] != null) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3] != "" && profiledata[3] != null) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4] != "" && profiledata[4] != null) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5] != "" && profiledata[5] != null) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	if (profiledata[6] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[6]));	
		this.oListItem.set_item('LookupAOW', newId);
	}
	if (profiledata[7] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[7]));	
		this.oListItem.set_item('LookupInitiative', newId);
	}
	if (profiledata[8] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[8]));	
		this.oListItem.set_item('LookupProgram', newId); 
	}
	if (profiledata[9] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[9]));	
		this.oListItem.set_item('LookupSubProgram', newId); 
	}
	this.oListItem.set_item('PublishingApproval', "Draft");
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onMGItemPostQuerySucceeded(del_selected)), 
		Function.createDelegate(this, this.onMGQueryFailed)
	);

}

function getMeasureGroupingAssocRight(deln) {
	  var selectStr = "$select=ID,MeasureGroupValue,LookupMeasureDetailId";
	  var filterby = "$filter=MeasureGroupValue%20eq%20%27" + deln + "%27";
	  var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groupings')/items?" + selectStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(carootUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadMeasureGroupingsAssocRight,
			error: errorHandler
		  }
	  );	
}

function loadMeasureGroupingsAssocRight(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var mdtlarray = [];
	for (var i = 0; i < results.length; i++) {
		var idArray = results[i].LookupMeasureDetailId.results;
		m = mdtlarray.length;
		if (idArray.length != 0) {
			for (var j = 0; j < idArray.length; j++) {
			  mdtlarray[m] = idArray[j];
			  m++;
			}
		}
	};
	var unique = mdtlarray.filter(function(itm,i,a){
    	return i==a.indexOf(itm);
	});
	mdtlarray = unique;
	loadListMeasureProfileRight(mdtlarray);
}

function loadListMeasureProfileRight(mdtlids) {
	for (var i = 0; i < mdtlids.length; i++) {
	  var selectStr = "$select=ID,MeasureNameValue";
	  var filterby = "$filter=ID%20eq%20%27" + mdtlids[i] + "%27";	  
	  var itemsUrl = carootUrl + "/_api/web/lists/getbytitle('Measure%20Details')/items?" + selectStr + "&" + filterby;
	  $.ajax({
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  cache: false,
		  async: false,
		  success: function(data) { 
				var list = (data.d.results == null) ? new Array(data.d) : data.d.results;
				var option = $("<option/>").attr("id" , list[0].ID).text(list[0].MeasureNameValue);
				$("#q_listMeasuresRight").append(option);
			},
		  error: function (data) {
			  console.log("Error");
		  }
	  });	
	}
	getMeasureDetailsLeft();
	setTimeout(function(){ 
		$("#q_listMeasuresRight option").sort(function(a, b){
			return ($(b).text()) < ($(a).text()) ? 1 : -1;
		}).appendTo('#q_listMeasuresRight');
	}, 100); 
}

function getMeasureDetailsLeft() {
	  var selectStr = "$select=ID,MeasureNameValue";
	  var itemsUrl = carootUrl + "/_api/web/lists/getbytitle('Measure%20Details')/items?" + selectStr;
	  var executor = new SP.RequestExecutor(carootUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadMeasureDetailsLeft,
			error: errorHandler
		  }
	  );
}

function loadMeasureDetailsLeft(data){
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
		for (var i = 0; i < results.length; i++) {
		var found = false;
		$("#q_listMeasuresRight option").each(function(j){
			var right_val = $(this).val();
			if(right_val == results[i].MeasureNameValue ){
				found = true;
			}
		 });
		 if (found != true){	 
			var option = $("<option/>").attr("id" , results[i].ID).text(results[i].MeasureNameValue);
			$("#q_listMeasuresLeft").append(option);
		  }
		}
	setTimeout(function(){ 
		$("#q_listMeasuresLeft option").sort(function(a, b){
			return ($(b).text()) < ($(a).text()) ? 1 : -1;
		}).appendTo('#q_listMeasuresLeft');
	}, 100);
}

function onMGItemPostQuerySucceeded(selected) {
	if ("default_del_" + selected != null){
		if (document.URL.indexOf("IsDlg") < 0 ){ alert("Measure Grouping has been updated");}	
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function onItemPostQueryFailed(sender, args) {
	alert('Request failed. ' + args.get_message() + 
		'\n' + args.get_stackTrace());
} 
