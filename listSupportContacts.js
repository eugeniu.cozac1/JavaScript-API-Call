function getSupportContacts() {
	  var selectStr = "$select=ID,DeliverableNameValue,GroupEmailName,ContactType,ContactName/ID,ContactName/Title,ContactName/EMail&$expand=ContactName/ID,ContactName/Title,ContactName/EMail";
	  var orderbyStr = "$orderby=ContactType%20desc,ContactName/Title";
	  var filterby = "$filter=(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Support%20Contacts')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListSupportContacts,
			error: errorHandler
		  }
	  );		
}

function loadListSupportContacts(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var titleNode = $("<h4/>").text("Contacts");
	var ulNode = $("<ul>").attr("id", "q_contacts_nav");
	$("#q_listSupportContacts").addClass("q_contacts");
	$("#q_listSupportContacts").append(titleNode);
	$("#q_listSupportContacts").append(ulNode);
	for (var i = 0; i < results.length; i++ ){
		if (results[i].DeliverableNameValue == null || results[i].DeliverableNameValue == "") {
			var linkhref = "mailto:" + results[i].ContactName.EMail;
			var nameStr = results[i].ContactName.Title;
			if (nameStr == null) {
				nameStr = results[i].GroupEmailName;
				linkhref = "mailto:" + nameStr + "@kp.org";
			}
			setListSupportContactRow (linkhref, nameStr, 'Click to email ' + nameStr, results[i].ContactType);
		}
	}  
}

function setListSupportContactRow (linkhref, displaytitle, hovertext, contacttype) {

	var liNode = $("<li>");
	var aNode = $("<a>");
	var pNode = $("<p>");
	aNode.attr("title", hovertext).text(displaytitle);
	aNode.attr("href", linkhref);
	liNode.append(aNode);
	pNode.text(contacttype);
	liNode.append(pNode);

	$("#q_contacts_nav").append(liNode);
}
