function getNavDeliverableLinks() {
	var orderbyStr = "$orderby=DeliverableNameValue";
	var filterby = "$filter=DeliveryMethod%20eq%20%27Web%20Document%27%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	  
	var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items?" + orderbyStr + "&" + filterby;
	var executor = new SP.RequestExecutor(catUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadNavDeliverableLinks,
		  error: errorHandler
		}
	);		
}

function loadNavDeliverableLinks(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var report_array = [];
	if(results.length > 0){
		var titleNode = $("<h4/>").text("Recently Released Reports");
		var ulNode = $("<ul>").attr("id", "q_boxlist_del");
		$("#q_navdeliverables").addClass("q_boxlist");
		$("#q_navdeliverables").append(titleNode);
		$("#q_navdeliverables").append(ulNode);
		for (var i = 0; i < results.length; i++ ){
			var orderby = "$orderby=ReportDataDate";
			var filterby = "$filter=DeliverableNavString%20eq%20%27" + results[i].DeliverableNavString + "%27";	  
			var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?$top=1&" + orderby + "&" + filterby;
			$.ajax({
				url: itemsUrl,
				method: "GET",
				headers: { "Accept": "application/json; odata=verbose" },
				cache: false,
				async: false,
				success: function(data){
					var docs = (data.d.results == null) ? new Array(data.d) : data.d.results;
					$(docs).each(function(){
					var delNavString = $(this)[0].DeliverableNavString, delNameVal = $(this)[0].DeliverableNameValue, reportDataDate = $(this)[0].ReportDataDate, pageNameValue = results[i].PageNameValue;
							var obj = {
							    "DeliverableNavString" : delNavString,
								"DeliverableNameValue" : delNameVal,
								"ReportDataDate" : reportDataDate,
								"PageNameValue" : pageNameValue
							}
							report_array.push(obj);
					})
				},
				error:  function() {}
			   }
			);
		} 
		report_array.sort(function(a, b) {
			if(a.ReportDataDate.substring(0, 10) < b.ReportDataDate.substring(0, 10)){
				return -1;  
			}else if(a.ReportDataDate.substring(0, 10) > b.ReportDataDate.substring(0, 10)){
				return 1;
			}else{
				if(a.DeliverableNameValue < b.DeliverableNameValue){
				   return -1
				}else if(a.DeliverableNameValue > b.DeliverableNameValue){
				  return 1;
				}
			}
		});
		$(report_array).each(function(){
			var counter = report_array.length;
			if (counter == 5) {
				var link_view = pagesUrl + "topicreports.aspx?carea=" + contentarea;
				setNavLinkDeliverableRow (link_view, 'View more...', 'Click to view more.');
				i =  results.length;
			}
			if(counter < 5){
				var linkhref = pagesUrl + $(this)[0].PageNameValue + "?carea=" + contentarea + "&delnav=" + $(this)[0].DeliverableNavString;  //results[i].PageNameValue
				setNavLinkDeliverableRow (linkhref, $(this)[0].DeliverableNameValue, 'Click to download releases of this report.');	
			} 
		});
	}
}

function setNavLinkDeliverableRow (linkhref, displaytitle, hovertext) {
	var liNode = $("<li>");
	var aNode = $("<a>");
	aNode.attr("title", hovertext).text(displaytitle);
	aNode.attr("href", linkhref);
	aNode.attr("target", "_self");
	liNode.append(aNode);
	$("#q_boxlist_del").append(liNode);
}
