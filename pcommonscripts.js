// Frequently used variables and functions 
var siteType = document.URL.split("/")[4];

var contentarea = getQueryStringParameter("carea");
var ctype = getQueryStringParameter("ctype");
var delnav = getQueryStringParameter("delnav");
var mdtlnav = getQueryStringParameter("mdtlnav");
var mdtlid = getQueryStringParameter("mdtlid");
var curId = getQueryStringParameter("curid");
var restrictedContent = false;
var loadPageText_BannerDone = false;
var loadDelCriteriaDone = false;
var frequency = "Quarterly";
var granularity = "Region";
var visibleHistory = "3";

var rootUrl = "https://sites.sp.kp.org/pub";
var catroot = "https://sites.sp.kp.org";
switch (siteType) {
	case "qos":
		var pagesUrl = rootUrl + "/qos/Pages/";
		var catUrl = rootUrl + "/qosops";
		var QOSscriptbase = rootUrl + "/qos/_catalogs/masterpage/QOS/js/";
		break;
	case "qosstgpub":
		var pagesUrl = rootUrl + "/qosstgpub/Pages/";
		var catUrl = rootUrl + "/qoscat";
		var QOSscriptbase = rootUrl + "/qosstgpub/_catalogs/masterpage/QOS/js/";
		break;
	case "qosdevcat":
		var pagesUrl = rootUrl + "/qosdevcat/qos/Pages/";
		var catUrl = rootUrl + "/qosdevstg";
		var QOSscriptbase = rootUrl + "/qosdevcat/_catalogs/masterpage/QOS/js/";
		break;
};

function getQueryStringParameter(paramToRetrieve){
	  var params = document.URL.split("?")[1].split("&");
	  var strParams = "";
	  for (var i = 0; i < params.length; i = i + 1) {
		var singleParam = params[i].split("=");
		if (singleParam[0].toLowerCase() == paramToRetrieve.toLowerCase()) 
		  return singleParam[1]; 
	  }
	  return false;
}

function getIconFile(filename) {
	  switch (true) {
		  case (filename.indexOf("xls") > 0):
			  source = "/_layouts/15/images/icxls.png";
			  break;
		  case (filename.indexOf("xlsx") > 0):
			  source = "/_layouts/15/images/icxlsx.png";
			  break;
		  case (filename.indexOf("xlsm") > 0):
			  source = "/_layouts/15/images/icxlsm.png";
			  break;
		  case (filename.indexOf("doc") > 0):
			  source = "/_layouts/15/images/icdoc.png";
			  break;
		  case (filename.indexOf("docx") > 0):
			  source = "/_layouts/15/images/icdocx.png";
			  break;
		  case (filename.indexOf("ppt") > 0):
			  source = "/_layouts/15/images/icppt.png";
			  break;
		  case (filename.indexOf("pptx") > 0):
			  source = "/_layouts/15/images/icpptx.png";
			  break;
		  case (filename.indexOf("pdf") > 0):
			  source = "/_layouts/15/images/icpdf.png";
			  break;
		  default:
			  source = "/_layouts/15/images/ictxt.gif";
	  }
	  return source;	
}

function writeNoAccessMessage() {
		var content = "<h2>Restricted Content</h2>" +
			"<h3>The content you have selected to view is subject to restricted viewing.</h3>" +
			"<p>If you believe you should be eligible to see content in this area, please <a href='mailto:qos.web@kp.org'>email QOS Web</a> for help. Please include your role and business need for accessing this content.</p>";
		$('#q_pageHeaderBar').html(content);
		$('#q_pageHeaderBar').css("margin", "40px 0");
}

function writeNoMeasuresFoundMsg() {
		var content = "<h4>No measure profiles associated with this data deliverable.</h4>";
		$('#q_listmeasures').html(content);
		$('#q_listmeasures').css("margin", "40px 0");
}


function errorHandler(data, errorCode, errorMessage) {
  $("#q_errormessage").text("Could not complete cross-domain call: " + errorMessage);
}
