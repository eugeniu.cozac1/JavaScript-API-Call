function getDataDel_CurWeMedCtr() {
	  var selectStr = "$select=ID,DeliverableNavString";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: getDataDel_Documents,
			error: errorHandler
		  }
	  );		
}

function getDataDel_Documents(data) {
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var delnav = results[0].DeliverableNavString;
	  var current_date = new Date();
	  current_date.setDate(current_date.getDate() - 7);
	  var dateFilterStr = (current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear();
	  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,ServiceAreaValue";
	  var orderbyStr = "$orderby=ReportDataDate%20desc,ServiceAreaValue";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListDataDel_curWe,
			error: errorHandler
		  }
	  );		
}

function loadListDataDel_curWe(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var columnsNode = $("<div/>").addClass("q_colcontainer_4 q_add-margin-bottom q_add-margin-top"); 
	if (results.length > 0) {
		var curReportDataDt = Date(results[0].ReportDataDate);	
		for (var i = 0; i < results.length; i++ ){
			if (Date(results[i].ReportDataDate) == curReportDataDt) {
				if (i == 0) { setCurQtrHeader(curReportDataDt, results[i].ReleaseDate); }	
					setCurContentItem(results[i].ServiceAreaValue, results[i].FileRef, results[i].FileLeafRef, columnsNode);
			} 
		}
	} else { columnsNode.append($('h4').text('No items found for this time period')); }
	$("#q_rptQtrCurrent_medctr").append(columnsNode);
}

function setCurQtrHeader(curReportDataDt, curReleaseDt) {
	var report_date = new Date(curReportDataDt);
	var formattedDate = (report_date.getMonth() + 1) + "/" + report_date.getDate() + "/" + report_date.getFullYear();	
	var title = "Reports for Week of " + formattedDate + " - Released: " + formattedDate;
	$('#q_rptQtrCurrent_medctr').append($("<h2>").addClass("q_doclistbox").text(title));
}

function setCurContentItem (medctr, fileRef, filename, columnsNode) {
	var linkhref = catroot + fileRef;
	var source = getIconFile(filename);
	var divNode1 = $("<div>");
	var divNode2 = $("<div>").addClass("q_column_aligntop");
	var aNode2 = $("<a/>").attr({"title" : medctr, "href" : linkhref, "target" : "_blank"});
	var divNode3 = $("<div>").addClass("q_column_linkwidth");
	var aNode3 = $("<a/>").attr({"title" : medctr, "href" : linkhref, "target" : "_blank"}).text( "  " + medctr);
	var imgNode = $("<img/>").attr({"src" : source});
	aNode2.append(imgNode);
	divNode2.append(aNode2);
	divNode3.append(aNode3);
	divNode1.append(divNode2, divNode3);
	$(columnsNode).append(divNode1);
}
