function fetchDelNavString() {
	  var selectStr = "$select=ID,DeliverableNavString,RptFrequency,PageNameValue";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: fetchDataDelDocs,
			error: errorHandler
		  }
	  );		
}

function fetchDataDelDocs(data) {
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var curYear = new Date().getFullYear();
	  var current_date = new Date();
	  var rpt_frequency = results[0].RptFrequency;
	  var dateFilterStr = "";
	  if(rpt_frequency == "Weekly"){
		 current_date.setDate(current_date.getDate() - 28);
	     dateFilterStr = (current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear(); 
	  }else if(rpt_frequency == "Monthly" || rpt_frequency == "Quarterly"){
		 dateFilterStr = "12/31/" + (curYear-3);
	  }	  
	  var delnav = results[0].DeliverableNavString;
	  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,MedicalCtrValue";
	  var orderbyStr = "$orderby=ReportDataDate%20desc,MedicalCtrValue";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby; // was $top=1&
	  var page_name_value = results[0].PageNameValue;
	  var loadSelectOrAll ="";
	  if(page_name_value.indexOf("all") == 0 || page_name_value.indexOf("rptdash")  == 0 || medctr == false){
		  loadSelectOrAll = loadAll;
	  }else if(page_name_value.indexOf("medctr") == 0){
	      loadSelectOrAll = loadSelect;
	  }
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadSelectOrAll,
			error: errorHandler
		  }
	  );		
}

function loadSelect(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var mcarray = [];
	for (var i = 0; i < results.length; i++) {
		mcarray[i] = results[i].MedicalCtrValue + ";" + results[i].ID;
	};
	var unique = mcarray.filter(function(itm,i,a){
    	return i==a.indexOf(itm);
	});
	mcarray = unique;
	mcarray.sort();
	for (var i = 0; i < mcarray.length; i++ ){
	  var text = mcarray[i].split(";")[0];
	  var var_id = mcarray[i].split(";")[1];
	  var opt = $("<option>").text(text);
	  var medctr_fetch = "";
	  if (medctr != false){
	  	 medctr_fetch = medctr.replace("%20", " ");
	  }
	  if (text == medctr_fetch || i == 0) { opt.attr("selected", "selected"); }
	  opt.val(var_id);
	  $("#q_listMedCtr").append(opt);
	}
	if (medctr != false){
	  	getDataDel_loadSelect(); 
		$('#q_listMedCtr').change(function() { getDataDel_loadSelect(); } );  
	}
}

function getDataDel_loadSelect(){
	  var selectStr = "$select=ID,DeliverableNavString,RptFrequency,PageNameValue";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: getDataDel_loadSelectDone,
			error: errorHandler
		  }
	  );		
}

function getDataDel_loadSelectDone(data) { 
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var curYear = new Date().getFullYear();
	  var current_date = new Date();
	  var rpt_frequency = results[0].RptFrequency; 
	  var dateFilterStr = "";
	  if(rpt_frequency == "Weekly"){
		 current_date.setDate(current_date.getDate() - 28);
	     dateFilterStr = (current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear(); 
	  }else if(rpt_frequency == "Monthly" || rpt_frequency == "Quarterly"){
		 dateFilterStr = "12/31/" + (curYear-3);
	  }
	  var delnav = results[0].DeliverableNavString;
	  var selectedmedctr = $("#q_listMedCtr option:selected").text();
	  $("#q_medctrhistorytitle").empty();
	  $("#q_medctrhistorytitle").append($("<h2>").text("Historical Reports for " + selectedmedctr));
	  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,MedicalCtrValue";
	  var orderbyStr = "$orderby=ReportDataDate%20asc,MedicalCtrValue";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27%20and%20MedicalCtrValue%20eq%20%27" + selectedmedctr + "%27";	
	  if(medctr != false){
	      filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27%20and%20MedicalCtrValue%20eq%20%27" + medctr + "%27";
	  }
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: function(data){
				var jsonObject = JSON.parse(data.body);
				var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
				var lastYear = "";	
				var curYear = new Date().getFullYear();
				$("#q_listHistDocs").empty();
				if(rpt_frequency == "Weekly"){
				    var idval = "q_row";
					var colNode = $("<div/>").addClass("q_delhistrow");
					colNode.append($("<div>").addClass("q_delhistrow_content").append($("<div/>").addClass("q_colcontainer_4 q_add-margin-bottom q_add-margin-top").attr("id", idval + "results")));
					$("#q_listHistDocs").append(colNode);
				}
				for (var i = 0; i < results.length; i++ ){	
					if(rpt_frequency == "Monthly" || rpt_frequency == "Quarterly"){	
						var newYear = new Date(results[i].ReportDataDate).getFullYear();
						if ((curYear - newYear) < 3) {	
							var idval = "q_row";
							if (lastYear == "" || lastYear != newYear) {
								lastYear = new Date(results[i].ReportDataDate).getFullYear();
								var colNode = $("<div/>").addClass("q_delhistrow");
								colNode.append($("<div>").addClass("q_delhistrow_title").append($("<h3>").text(lastYear)));
								colNode.append($("<div>").addClass("q_delhistrow_content").append($("<div/>").addClass("q_colcontainer_4 q_add-margin-bottom q_add-margin-top").attr("id", idval + lastYear)));
								$("#q_listHistDocs").append(colNode);	
								}
								setHistContentItem("#" + idval + lastYear, results[i].ReportDataDate, results[i].FileRef, results[i].FileLeafRef, rpt_frequency);
							}
						} else 
						if(rpt_frequency == "Weekly"){
							 setHistContentItemWeekly("#" + idval + "results", results[i].ReportDataDate, results[i].FileRef, results[i].FileLeafRef);	
					 }	
				}
			},
			error: errorHandler
		  }
	  );		
}

function setHistContentItem (idval, reportDate, fileRef, filename, frequency) {
	var linkhref = catroot + fileRef;
	var source = getIconFile(filename);
	var curDate = new Date(reportDate);
	var qtrStr = "",label = ""; 
	if(frequency == "Quarterly"){
		qtrStr = Math.floor((curDate.getMonth() + 3) / 3);
		label = "Q" + qtrStr;
	}else if(frequency == "Monthly"){
		qtrStr = curDate.getMonth() + 1;
		Date.prototype.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		Date.prototype.getMonthName = function() {return this.monthNames[this.getMonth()];};
		label = curDate.getMonthName();
	}
	var divNode1 = $("<div>");
	var divNode2 = $("<div>").addClass("q_column_aligntop");
	var aNode2 = $("<a/>").attr({"title" : label, "href" : linkhref, "target" : "_blank"});
	var divNode3 = $("<div>").addClass("q_column_linkwidth");
	var aNode3 = $("<a/>").attr({"title" : label, "href" : linkhref, "target" : "_blank"}).text(label);
	var imgNode = $("<img/>").attr({"src" : source});
	aNode2.append(imgNode);
	divNode2.append(aNode2);
	divNode3.append(aNode3);
	divNode1.append(divNode2, divNode3);
	$(idval).append(divNode1);
}

function setHistContentItemWeekly(idval, reportDate, fileRef, filename) {
	var linkhref = catroot + fileRef;
	var source = getIconFile(filename);
	var report_date = new Date(reportDate);
	var report_date_full = (report_date.getMonth() + 1) + "/" + report_date.getDate() + "/" + report_date.getFullYear();
	var label = "Week of " + report_date_full;
	var divNode1 = $("<div>");
	var divNode2 = $("<div>").addClass("q_column_aligntop");
	var aNode2 = $("<a/>").attr({"title" : label, "href" : linkhref, "target" : "_blank"});
	var divNode3 = $("<div>").addClass("q_column_linkwidth");
	var aNode3 = $("<a/>").attr({"title" : label, "href" : linkhref, "target" : "_blank"}).text(label);
	var imgNode = $("<img/>").attr({"src" : source});
	aNode2.append(imgNode);
	divNode2.append(aNode2);
	divNode3.append(aNode3);
	divNode1.append(divNode2, divNode3);
	$(idval).append(divNode1);
}

function loadAll(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var mcarray = [];
	for (var i = 0; i < results.length; i++) {
		mcarray[i] = results[i].MedicalCtrValue + ";" + results[i].ID;
	};
	var unique = mcarray.filter(function(itm,i,a){
    	return i==a.indexOf(itm);
	});
	mcarray = unique;
	mcarray.sort();
	getDataDel_loadAll(); 
}

function getDataDel_loadAll(){
	  var selectStr = "$select=ID,DeliverableNavString,RptFrequency,PageNameValue";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: getDataDel_loadAllDone,
			error: errorHandler
		  }
	  );		
}

function getDataDel_loadAllDone(data) {  
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var curYear = new Date().getFullYear();
	  var current_date = new Date();
	  var rpt_frequency = results[0].RptFrequency;
	  var dateFilterStr = "";
	  if(rpt_frequency == "Weekly"){
		 current_date.setDate(current_date.getDate() - 28);
	     dateFilterStr = (current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear(); 
	  }else if(rpt_frequency == "Monthly" || rpt_frequency == "Quarterly"){
		 dateFilterStr = "12/31/" + (curYear-3);
	  }
	  var delnav = results[0].DeliverableNavString;
	  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,MedicalCtrValue";
	  var orderbyStr = "$orderby=ReportDataDate%20desc,MedicalCtrValue";
	  var year = rptdt.slice(0,4); month = rptdt.slice(4,6); day = rptdt.slice(6,8);
	  var takeDate = month + "/" + day + "/" + year;
	  var filterDate = "";
	  if(medctr == false){
	  	  filterDate = 	takeDate;
	  } else {
	      filterDate = dateFilterStr;
	  }
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + filterDate + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby; 
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: function(data){
				var jsonObject = JSON.parse(data.body);
				var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
				var lastYear = "";	
				var curYear = new Date().getFullYear();
				$(".q_add-margin-top").remove(); 
				if(rpt_frequency == "Weekly"){
				    var idval = "q_row";
					var colNode = $("<div/>").addClass("q_delhistrow");
					colNode.append($("<div>").addClass("q_delhistrow_content").append($("<div/>").addClass("q_colcontainer_4 q_add-margin-bottom q_add-margin-top").attr("id", idval + "results")));
					$("#q_listHistDocs").append(colNode);
				}
				for (var i = 0; i < results.length; i++ ){	
					if(rpt_frequency == "Monthly" || rpt_frequency == "Quarterly"){	
						var newYear = new Date(results[i].ReportDataDate).getFullYear();
						if ((curYear - newYear) < 3) {	
							var idval = "q_row";
							if (lastYear == "" || lastYear != newYear) {
								lastYear = new Date(results[i].ReportDataDate).getFullYear();
								var colNode = $("<div/>").addClass("q_delhistrow");
								colNode.append($("<div>").addClass("q_delhistrow_title").append($("<h3>").text(lastYear)));
								colNode.append($("<div>").addClass("q_delhistrow_content").append($("<div/>").addClass("q_colcontainer_4 q_add-margin-bottom q_add-margin-top").attr("id", idval + lastYear)));
								$("#q_listHistDocs").append(colNode);	
								}
								setHistContentItem("#" + idval + lastYear, results[i].ReportDataDate, results[i].FileRef, results[i].FileLeafRef, rpt_frequency);
							}
						} else 
						if(rpt_frequency == "Weekly"){
							 setHistContentItemWeekly("#" + idval + "results", results[i].ReportDataDate, results[i].FileRef, results[i].FileLeafRef);	
					 }	
				}
				$("#q_listHistDocs .q_delhistrow:first").css("borderTop" , "none");
			},
			error: errorHandler
		  }
	  );		
}