function fetchDelNavString() {
	  var selectStr = "$select=ID,DeliverableNavString";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: fetchDataDelDocs,
			error: errorHandler
		  }
	  );		
}

function fetchDataDelDocs(data) {
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var delnav = results[0].DeliverableNavString;  
	  var current_date = new Date();
	  current_date.setDate(current_date.getDate() - 28);
	  var dateFilterStr = (current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear();
	  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,MedicalCtrValue";
	  var orderbyStr = "$orderby=ReportDataDate%20desc,MedicalCtrValue";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadMedicalCenterValues,
			error: errorHandler
		  }
	  );		
}

function loadMedicalCenterValues(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	for (var i = 0; i < results.length; i++ ){
	}
	var mcarray = [];
	for (var i = 0; i < results.length; i++) {
		mcarray[i] = results[i].MedicalCtrValue + ";" + results[i].ID;
	};
	var unique = mcarray.filter(function(itm,i,a){
    	return i==a.indexOf(itm);
	});
	mcarray = unique;
	mcarray.sort();
	for (var i = 0; i < mcarray.length; i++ ){
	  var text = mcarray[i].split(";")[0];
	  var var_id = mcarray[i].split(";")[1];
	  var opt = $("<option>").text(text);
	  if (i == 0) { opt.attr("selected", "selected"); }
	  opt.val(var_id);
	  $("#select_report").append(opt);
	}
	getDataDel_HistWeMedCtr(); 
	$('#select_report').change(function() { getDataDel_HistWeMedCtr(); } );  
}

function getDataDel_HistWeMedCtr(){
	  var selectStr = "$select=ID,DeliverableNavString";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: getDataDel_HistDocuments,
			error: errorHandler
		  }
	  );		
}

function getDataDel_HistDocuments(data) {
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var delnav = results[0].DeliverableNavString;
	  var current_date = new Date();
	  current_date.setDate(current_date.getDate() - 28);
	  var dateFilterStr = (current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear();
	  var selectedmedctr = $("#select_report option:selected").text();
	  $("#q_medctrhistorytitle").empty();
	  $("#q_medctrhistorytitle").append($("<h2>").text("Historical Reports for " + selectedmedctr));
	  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,MedicalCtrValue";
	  var orderbyStr = "$orderby=ReportDataDate%20asc,MedicalCtrValue";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27%20and%20MedicalCtrValue%20eq%20%27" + selectedmedctr + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadDataDel_histWe,
			error: errorHandler
		  }
	  );		
}

function loadDataDel_histWe(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var today = new Date();
	var currentWeekNumber = today.getWeek();
	$("#q_rptQtrHist_medctr").empty();
	for (var i = 0; i < results.length; i++ ){
		var week = new Date(results[i].ReportDataDate); 
		var reportWeekNumber = week.getWeek();
		var status_week;
		if(reportWeekNumber + 3 == currentWeekNumber){
			status_week = "current_week"
		} else if(reportWeekNumber + 2 == currentWeekNumber){
			status_week = "last_week"
		} else if(reportWeekNumber + 1 == currentWeekNumber){
			status_week = "two_weeks_ago"
		}
		var newWeek = (week.getMonth() + 1) + "/" + week.getDate() + "/" + week.getFullYear();
		var idval = "q_row";
			var colNode = $("<div/>").addClass("q_delhistrow");
			colNode.append($("<div>").addClass("q_delhistrow_title").append($("<h3>").text(newWeek)));
			colNode.append($("<div>").addClass("q_delhistrow_content").append($("<div/>").addClass("q_colcontainer_4 q_add-margin-bottom q_add-margin-top").attr("id", idval + status_week)));
			$("#q_rptQtrHist_medctr").append(colNode);	
		setHistContentItem("#" + idval + status_week, results[i].ReportDataDate, results[i].FileRef, results[i].FileLeafRef);
	}
}

function setHistContentItem (idval, reportDate, fileRef, filename) {
	var linkhref = catroot + fileRef;
	var source = getIconFile(filename);
	var qtrStr;
	
	
	var today = new Date();
	var currentWeekNumber = today.getWeek();
	var report_date = new Date(reportDate);
	var reportWeekNumber = report_date.getWeek();
	if(reportWeekNumber + 3 == currentWeekNumber){
	    qtrStr = 1
	} else if(reportWeekNumber + 2 == currentWeekNumber){
	    qtrStr = 2
	} else if(reportWeekNumber + 1 == currentWeekNumber){
	    qtrStr = 3
	}
	
	var label = "W" + qtrStr;
	var divNode1 = $("<div>");
	var divNode2 = $("<div>").addClass("q_column_aligntop");
	var aNode2 = $("<a/>").attr({"title" : label, "href" : linkhref, "target" : "_blank"});
	var divNode3 = $("<div>").addClass("q_column_linkwidth");
	var aNode3 = $("<a/>").attr({"title" : label, "href" : linkhref, "target" : "_blank"}).text(label);
	var imgNode = $("<img/>").attr({"src" : source});
	aNode2.append(imgNode);
	divNode2.append(aNode2);
	divNode3.append(aNode3);
	divNode1.append(divNode2, divNode3);
	$(idval).append(divNode1);
}

		var qtrStr;
	
    var report_date = new Date(reportDate);
	var reportFilter = (report_date.getMonth() + 1) + "/" + report_date.getDate() + "/" + report_date.getFullYear();
	
	var today = new Date(), last_week = new Date(), two_weeks_ago = new Date();
	
	var todayFilter = (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear();
	
	last_week.setDate((last_week.getDate() - 7)); 
	var last_weekFilterStr = (last_week.getMonth() + 1) + "/" + last_week.getDate() + "/" + last_week.getFullYear(); // setTime
	
	two_weeks_ago.setDate(two_weeks_ago.getDate() - 14);
	var two_weeks_agoFilterStr = (two_weeks_ago.getMonth() + 1) + "/" + two_weeks_ago.getDate() + "/" + two_weeks_ago.getFullYear();
	
	console.log("reportFilter " + reportFilter);
	console.log("today " + todayFilter);
	console.log("last_week " + last_weekFilterStr);
	console.log("two_weeks_ago " + two_weeks_agoFilterStr);
	
	if(reportFilter == todayFilter){
	   	qtrStr = 3
	} else if(reportFilter == last_weekFilterStr){
	   	qtrStr = 2
	} else if(reportFilter == two_weeks_agoFilterStr){
	    qtrStr = 1
	}
	







	/*var today = new Date(curReportDataDt);
	var currentWeekNumber = today.getWeek();	
	var status_week;
	if(currentWeekNumber == currentWeekNumber){
	   status_week = "Current Week"
	} else if((currentWeekNumber - 1) > 1){
	   status_week = "Last Week"
	} else if((currentWeekNumber - 2) > 1){
	   status_week = "2 Weeks Ago"
	}*/


Date.prototype.getWeek = function() {
	var onejan = new Date(this.getFullYear(),0,1);
	var today = new Date(this.getFullYear(),this.getMonth(),this.getDate());
	var dayOfYear = ((today - onejan + 86400000)/86400000);
	return Math.ceil(dayOfYear/7)
};