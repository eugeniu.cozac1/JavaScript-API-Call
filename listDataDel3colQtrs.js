function getDataDel_3colQtrs() {
	  var selectStr = "$select=ID,DeliverableNavString";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27";		
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items?" + selectStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: getDataDel_Documents,
			error: errorHandler
		  }
	  );		
}

function getDataDel_Documents(data) {
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var curYear = new Date().getFullYear();
	  var dateFilterStr = "12/31/" + (curYear-3);
	  var selectStr = "$select=ID,Title,FileLeafRef,FileRef,ReleaseDate,ReportDataDate";
	  var orderbyStr = "$orderby=ReportDataDate%20desc";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListDataDel_3colQtrs,
			error: errorHandler
		  }
	  );		
}


function loadListDataDel_3colQtrs(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var lastYear = "";	
	var curYear = new Date().getFullYear();
	for (var i = 0; i < results.length; i++ ){
		var newYear = new Date(results[i].ReportDataDate).getFullYear();
		if ((curYear - newYear) < 3) {	
			
			if (lastYear == "" || lastYear != newYear) {
				lastYear = new Date(results[i].ReportDataDate).getFullYear();
				var difference = parseInt(curYear) - parseInt(lastYear);
				switch (difference) {
					case 0:
						idval = "q_listdel_curyr";
						break;
					case 1:
						idval = "q_listdel_prioryr"
						break;
					case 2:
						idval = "q_listdel_2yrprior"
						break;
				}
				setCategoryGrp_dataDoc("#" + idval, lastYear);
				$("#" + idval).append($("<ul>").addClass("q_doclist"));	
			}
			setContentRow_dataDoc("#" + idval, results[i].Title, results[i].ReleaseDate, results[i].FileRef, results[i].FileLeafRef);
		}
	}
}

function setCategoryGrp_dataDoc(idval, yearStr) {
	var h2Node = $("<h2>").addClass("q_doclistbox").text(yearStr);
	$(idval).append(h2Node);
}

function setContentRow_dataDoc (idval, title, releaseDate, fileUrl, filename) {
		var linkhref = fileUrl + "/" + filename;
		var fulldate = new Date(releaseDate);
		var formattedDate = (fulldate.getMonth() + 1) + "/" + fulldate.getDate() + "/" + fulldate.getFullYear();		
		var source = getIconFile(filename);
		var liNode = $("<li/>");
		var pNode1 = $("<p/>");
		var aNode1 = $("<a/>").attr({"title" : title, "href" : linkhref, "target" : "_blank"}).append($("<img/>").attr({"src" : source, "height" : "16px", width: "16px"}));
		var aNode2 = $("<a/>").attr({"title" : title, "href" : linkhref, "target" : "_blank"}).text(title);
		pNode1.append(aNode1, aNode2);
		var pNode2 = $("<p/>");
		pNode2.append($("<em/>").addClass("q_releasedt").text("Released: " + formattedDate));
		liNode.append(pNode1);
		liNode.append(pNode2);
		$(idval + " ul").append(liNode);
}