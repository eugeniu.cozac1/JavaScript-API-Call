// Javascript library for adding Page Measure Versions
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/js/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/js/";
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}
 
jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof getMeasuresOptionList == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			overrideSharepointStyles();
			getWebUserData();
			getMeasuresOptionList();
			var intervalStart = setInterval(setWindowValues, 500);	
			function setWindowValues(){
			 	if(loadMeasureDetailsDone == true){	
						var savebuttontext = "Save";
						var updateType = "post";
						if (curId != false) {	
							$("#q_tbl-listMeasures").addClass("q_hide");
							$("#q_tbl-MeasureName").removeClass("q_hide");	
							loadItemData();
							savebuttontext = "Update";
							updateType = "update";	
						};
						if (mdtlId != false) {	
							$("#q_tbl-listMeasures").addClass("q_hide");
							$("#q_tbl-MeasureName").removeClass("q_hide");
							fetchMeasureName(mdtlId);	
							savebuttontext = "Save";
						};
						$("#q_numerator").spHtmlEditor({version: "onprem" });
						$("#q_denominator").spHtmlEditor({version: "onprem" });
						$("#q_inclusions").spHtmlEditor({version: "onprem" });
						$("#q_exclusions").spHtmlEditor({version: "onprem" });
						$("#q_defEffectivedate, #q_defEnddate").datepicker();
						$('#q_numerator, #q_denominator, #q_inclusions, #q_exclusions').on('click', function() {
							setTimeout(function() {	
								$("li[id='Ribbon.EditingTools.CPEditTab-title'] a")[0].click();
							}, 650);
						});	 
						setTimeout(function() {$("li[id='Ribbon.WebPartPage-title'] a")[0].click();}, 400);
						$("#q_cancelbutton").click(function(){ cancelModalWindow() });
						$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation(updateType)});
						$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
						$("#q_hideinputfields").removeClass("q_hide");
						$(".se-pre-con").fadeOut("slow");
						clearInterval(intervalStart);
				}
			}
		},"sp.ui.dialog.js");
	},"sp.js");
}

function fieldValidation(updateType){	
	// required values are Text Category, Title, and Content Text Block.
	var isValid = true;
	$("#q_errormessage").text("");
	if ($("#q_MeasureName").length < 1 && $("#q_listMeasures option:selected").val() < 1) {	
			var error_node = $('<p>').text('Please select a measure name for this item.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	};
	if ($('#q_defEffectivedate').val() == "") { 
			var error_node = $('<p>').text('Effective Date is required');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 
	var numeratortext = $('#q_numerator').spHtmlEditor("gethtml");
	if (numeratortext.length < 7) {
			var error_node = $('<p>').text('A description of the numerator is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 	 
	var denominatortext = $('#q_denominator').spHtmlEditor("gethtml");
	if (denominatortext.length < 7) {
			var error_node = $('<p>').text('A description of the denominator is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 	 
	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		postItem(updateType);
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}

function postItem(updateType) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Versions'); 
	if (updateType == "post") {
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	} else {
		this.oListItem = oList.getItemById(curId);
	}
	var mversionLookupSingle = new SP.FieldLookupValue();  
	if(mdtlId == false){
		var get_id_mversion = $("#q_listMeasures option:selected").val();
		var measureName = $("#q_listMeasures option:selected").text()
	}else{
   		var get_id_mversion = mdtlId;
		var measureName = $("#q_MeasureName").text();
	}

	mversionLookupSingle.set_lookupId(get_id_mversion);
	this.oListItem.set_item('LookupMeasureDetail', mversionLookupSingle);
	this.oListItem.set_item('MeasureNavString', 'mg' + get_id_mversion);
	this.oListItem.set_item('MeasureNameValue', measureName);
	this.oListItem.set_item('Title', 'Measure Version');
	var defEffDate = new Date($('#q_defEffectivedate').val());
	this.oListItem.set_item('DefinitionEffectiveDate', defEffDate.toISOString());
	var deftemp2 = new Date($('#q_defEnddate').val());
	if (deftemp2 != "Invalid Date") { 
		var defEndDate = new Date($('#q_defEnddate').val());	
		this.oListItem.set_item('DefinitionEndDate', defEndDate.toISOString());
	}
	this.oListItem.set_item('Numerator', $('#q_numerator').spHtmlEditor("gethtml"));
	this.oListItem.set_item('Denominator', $('#q_denominator').spHtmlEditor("gethtml"));
	this.oListItem.set_item('Exclusions', $('#q_exclusions').spHtmlEditor("gethtml"));
	this.oListItem.set_item('Inclusions', $('#q_inclusions').spHtmlEditor("gethtml"));
	if ($("#q_iscurrent").is(":checked")){var q_IsCurrent = 1;} else {var q_IsCurrent = 0;}
	this.oListItem.set_item('MeasureIsCurrent', q_IsCurrent); 
	this.oListItem.set_item('PublishingApproval', "Draft");
	
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
}

function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){  $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function onItemPostQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function loadItemData() {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Versions');
	this.oListItem = oList.getItemById(curId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadItemLoadQuerySucceeded), Function.createDelegate(this, this.onItemLoadQueryFailed)); 
}

function fetchMeasureName(measureId) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Measure Details');
	this.oListItem = oList.getItemById(measureId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadMeasureDetailsQuerySucceeded), Function.createDelegate(this, this.onItemLoadQueryFailed)); 
}

function onLoadMeasureDetailsQuerySucceeded(sender, args){
	$("#q_MeasureName").text(oListItem.get_item("MeasureNameValue"));
}

function onLoadItemLoadQuerySucceeded(sender, args) {
	$('#q_numerator .edit-content').html(oListItem.get_item("Numerator"));
	$('#q_denominator .edit-content').html(oListItem.get_item("Denominator"));
	$('#q_exclusions .edit-content').html(oListItem.get_item("Exclusions"));
	$('#q_inclusions .edit-content').html(oListItem.get_item("Inclusions"));
	var defEffectivedate = new Date(oListItem.get_item("DefinitionEffectiveDate"));
	var formatted_defEffectivedate = (defEffectivedate.getMonth() + 1) + "/" + defEffectivedate.getDate() + "/" + defEffectivedate.getFullYear();
	$("#q_defEffectivedate").val(formatted_defEffectivedate);
	var defEnddate = new Date(oListItem.get_item("DefinitionEndDate"));
	var formatted_defEnddate = (defEnddate.getMonth() + 1) + "/" + defEnddate.getDate() + "/" + defEnddate.getFullYear();
	$("#q_defEnddate").val(formatted_defEnddate);	
	$("#q_MeasureName").text(oListItem.get_item("MeasureNameValue"));
	var get_mgrp = oListItem.get_item("LookupMeasureDetail").get_lookupId();
	$("#q_listMeasures option").each(function(){
		if(parseInt($(this).val()) == get_mgrp){
			$(this).prop("selected", true);
		} 
	});
	var iscurrent = oListItem.get_item("MeasureIsCurrent");
	if (iscurrent == true){
		$("#q_iscurrent").prop("checked", true);
	}
	var measureId = oListItem.get_item("MeasureNavString").split("mg")[1];
	fetchMeasureName(measureId);
}

function onItemLoadQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
}
