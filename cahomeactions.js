var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/js/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/js/";
}	
 
jQuery(document).ready(function ($) {
	$.getScript( QOSscriptbase + "qos_ca_commonscripts.js", function () {console.log('qos_ca_commonscripts loaded');} );
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof changeBranding == 'function' && typeof getPageText_Banner == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		$(".se-pre-con").fadeOut("slow");
		$(".q_spinner").removeClass("q_hide");
		return false;
	}
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog');
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () { console.log("Initiating SP.ClientContext") });
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {	  
			$("#q_grid-33-right").addClass("q_borderleft-wide");
			changeBranding();
			getPageText_Banner();
			defineNavigation();
		},"sp.ui.dialog.js");
	},"sp.js");

}

function defineNavigation () {
	setLinksFiles("Business Deliverable File", "General", "#q_row-actions-bizdocs", "", "new_uploaddocs");
	setLinksFiles("Data Deliverable File", "Report", "#q_row-actions-datadocs", "", "new_uploaddocs");
	setLinksContent("Page Text", "Textual", "content", "#q_row-actions-pagetext", "", "new_pagetext");
	setLinksContent("Support Contacts", "QOS", "Support%20Contacts", "#q_row-actions-contacts", "", "new_contacts");
	setLinksContent("WebLinks", "Web", "links", "#q_row-actions-weblinks", "", "new_links");
	setLinksLists("Data Deliverable Profiles", "Deliverables", "deliverables", "#q_row-actions-deltype", "", "new_deliverables");
	setLinksLists("Measures Profiles", "Measure%20Details", "measuredetails", "#q_row-actions-measures", "", "new_measures");
	setLinksLists("Groupings", "Measure%20Groupings", "mgroupings", "#q_row-actions-mgroupings", "", "new_measuregroupings");
	setLinksPubActions("Publish to Staging", "Draft", "#q_row-actions-staging", pushicon);
	setLinksPubActions("Publish to Production", "Pending", "#q_row-actions-prod", pushicon);
	setLinksPubActions("Unpublish Items", "Unpublish", "#q_row-actions-unpublish", unpubicon);
	setLinksPubActions("To Archive", "Archive", "#q_row-actions-archive", archiveicon);
	setLinksPubActions("Restore from Archive", "Recover", "#q_row-actions-recover", restoreicon);
	setLinksPubActions("Permanently Delete", "Delete", "#q_row-actions-delete", deleteicon);
	setLinksAssocActions("By Data Deliverable Profile", "Deliverables", "#q_row-actions-delassoc", editicon);
	setLinksAssocActions("By Measure Profile", "Measure%20Details", "#q_row-actions-massoc", editicon);
	setLinksAssocActions("By Grouped Measures", "Measure%20Groupings", "#q_row-actions-mgassoc", editicon);
	$(".se-pre-con").fadeOut("slow");
	$(".q_spinner").removeClass("q_hide");
}
