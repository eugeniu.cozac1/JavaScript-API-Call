function loadAssocPageIssues(data) {
  var jsonObject = JSON.parse(data.body);
  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
  if (results.length == 0) { writeNoRecordsMessage()};
  for (var i = 0; i < results.length; i++) {
	var windowTitle = "Edit Issues Metadata";
	var formUrl = caformsUrl + "new_issues.aspx?carea=" + contentarea + "&curid=" + results[i].ID;
	var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";
	var nullstr;
	if (i == 0) {
		setCategoryHeader (nullstr);
		setColumnHeaders();
	}
	setContentItemRow (nullstr, onclickstr, results[i].WebURL.Description, results[i].PublishingApproval, results[i].PublishingLastPostDate, nullstr, nullstr, results[i].WebURL.Description);
  }
}

function setCategoryHeader (category) {
		var classname = category;
		if (classname != null) {
			classname = classname.replace(/\s+/g, '');
			classname = classname.replace("/", '-');
			var idshow = "show-" + classname;
			var idhide = "hide-" + classname;
			var table_node = $("<table>").addClass("grid-100 q_tbl-titlebar");
			var tr_node = $("<tr>");
			var td1_node = $("<td>").attr({"colspan": "3"}).addClass("q_titlebarheader").text(category);
			var td2_node = $("<td>").addClass("q_titlebarbutton");
			var show_node = $("<div>").attr({"id": idshow}).addClass("q_bullet_link q_bulletspritedown q_hide").text("Show All").click(function() { showItems(classname);});
			var hide_node = $("<div>").attr({"id": idhide}).addClass("q_bullet_link q_bulletspriteup q_show").text("Collapse").click(function() {hideItems(classname);});
			td2_node.append(show_node);
			td2_node.append(hide_node);
			tr_node.append(td1_node);
			tr_node.append(td2_node);
			table_node.append(tr_node);
			$("#q_resultslist").append(table_node);
		} else {
			var divider_node = $("<div>").addClass("q_dividerwide q_add-margin-top q_add-margin-bottom");
			$("#q_resultslist").append(divider_node);
		}
}

function setColumnHeaders() {
		var table_node = $("<table>").addClass("grid-100 q_tbl-edititems");
		var tr_node = $("<tr>");
		var td1_node = $("<td>").addClass("q_row-edititems-filler");
		var td2_node = $("<td>").addClass("q_label-item").text("Status");
		var td3_node = $("<td>").addClass("q_label-item").text("Publish to Prod");
		tr_node.append(td1_node);
		tr_node.append(td2_node);
		tr_node.append(td3_node);
		table_node.append(tr_node);
		$("#q_resultslist").append(table_node);
}

function setContentItemRow (category, clickstr, title, status, postdate, content, fileUrl, filename, sortValue) {
		var classname = category;
		if (classname != null) {
			classname = classname.replace(/\s+/g, '');
			classname = classname.replace("/", '-');
			var table_node = $("<table>").addClass("grid-100 q_tbl-edititems " + classname + " q_show").attr("data", sortValue);
		} else {
			var table_node = $("<table>").addClass("grid-100 q_tbl-edititems q_show").attr("data", sortValue);
		}
		var tr1_node = $("<tr>");
		var td1_node = $("<td>").addClass("q_iconcell");
		var a1_node = $("<a>").attr({"href": "javascript:;", "onclick": clickstr});
		var img_node = $("<img>").attr({"src": editicon});
		a1_node.append(img_node);
		td1_node.append(a1_node);
		var td2_node = $("<td>").addClass("q_itemtitle");
		var a2_node = $("<a>").attr({"href": "javascript:;", "onclick": clickstr});
		var span2_node = $("<span>").addClass("q_ca_clipline").text(title);
		a2_node.append(span2_node);
		td2_node.append(a2_node);
		tr1_node.append(td1_node);
		tr1_node.append(td2_node);
		var pubstr = setPubInfo(status, postdate);
		var pubarray = pubstr.split(";");
		var td3_node = $("<td>").addClass("q_itempubdetail").text(pubarray[0]);
		var td4_node = $("<td>").addClass("q_itempubdetail").text(pubarray[1]);
		tr1_node.append(td3_node);
		tr1_node.append(td4_node);
		table_node.append(tr1_node);
		if (fileUrl != null) {
			var tr1a_node = $("<tr>");
			var td1a_blanknode = $("<td>").text(" ");
			var td1a_node = $("<td>").attr({"colspan": "3"}).addClass("q_itemfile q_ca_clipline");
			var span_node = $("<span>").addClass("q_label-item").text("Open File: ");
			var link_node = $("<a>").attr({"href": fileUrl}).text(filename);
			td1a_node.append(span_node);
			td1a_node.append(link_node);
			tr1a_node.append(td1a_blanknode);
			tr1a_node.append(td1a_node);
			table_node.append(tr1a_node);
		}
		if (content != null) {
			var tr2_node = $("<tr>");
			var td4_node = $("<td>").text(" ");
			var td5_node = $("<td>").attr({"colspan": "3"}).addClass("q_itemdetail");
			if (content.search("<") < 0 ) {
				var text_node = $("<p>").text(content);
			} else {
				var text_node = $("<span>").addClass("q_ca_clipline");
				text_node.html(content);
			}
			td5_node.append(text_node);
			tr2_node.append(td4_node);
			tr2_node.append(td5_node);
			table_node.append(tr2_node);
		}
		$("#q_resultslist").append(table_node);
}

function setPubInfo(pubstatus, lastpubdate) {
	switch (pubstatus) {
		case "Draft":
			var contentStr = "In Authoring;";
  			break;
		case "Pending":
			var contentStr = "In Staging;";
  			break;
		case "Publish":
			var contentStr = "In Production;";
  			break;
		default:
			var contentStr = "Unpublished;";
  			break;
	};

	if (lastpubdate != null) { 
		var dateStr = lastpubdate.split("T");
		var dateseg = dateStr[0].split("-");
		var formattedDate = dateseg[1] + "-" + dateseg[2] + "-" + dateseg[0];
		contentStr = contentStr +  formattedDate;
	} else {
		contentStr = contentStr + "Not Available";
	}
	return contentStr;	
}


function showItems(classname) {
	if (classname == null) {
		return;
	}
	var classStr = "." + classname;
	var idshow = "#show-" + classname;
	var idhide = "#hide-" + classname;
	jQuery(classStr).each(function (i, value) {
		if ($(this).attr('class').search("q_hide") > 0) {
			$(this).removeClass("q_hide");
			$(this).addClass("q_show");
			$(idshow).removeClass("q_show");
			$(idshow).addClass("q_hide");
			$(idhide).removeClass("q_hide");
			$(idhide).addClass("q_show");
		}
	});
}

function hideItems(classname) {
	if (classname == null) {
		return;
	}
	var classStr = "." + classname;
	var idshow = "#show-" + classname;
	var idhide = "#hide-" + classname;
	jQuery(classStr).each(function (i, value) {
		if ($(this).attr('class').search("q_show") > 0) {
			$(this).removeClass("q_show");
			$(this).addClass("q_hide");
			$(idshow).removeClass("q_hide");
			$(idshow).addClass("q_show");
			$(idhide).removeClass("q_show");
			$(idhide).addClass("q_hide");
		}
	});
}

