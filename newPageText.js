// Javascript library for adding Page Text Items	
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/jstest/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}

jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof getDeliverablesOptionList == 'function' && typeof getMeasureGroupingsOptionList == 'function' && typeof getItemCategoriesOptionList == 'function' && typeof loadProfileData == 'function') {		
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
 });

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			SP.SOD.executeOrDelayUntilScriptLoaded(function () {
				overrideSharepointStyles();	
				getDeliverablesOptionList();
				getMeasureGroupingsOptionList();
				getItemCategoriesOptionList();
				loadProfileData();
				var intervalStart = setInterval(setWindowValues, 1000);
				function setWindowValues(){
					if(loadDeliverablesOptionsDone == true && loadMeasureGroupingsDone == true && loadProfileDataDone == true && loadItemCategoryListDone == true){
						$("#q_listCategories").on('change', function () {
							var currentId = $(this).val();
							hideSelectByCategory(currentId);
							if(currentId == "2" || currentId == "3"){
								$("#q_tbl-IsMeetingContent").removeClass("q_hide");
							}else{
								$("#q_tbl-IsMeetingContent, #q_tbl-IsMeetingDate").addClass("q_hide");
								$("#q_meetingDate, #q_IsMeetingContent").val('').removeAttr('checked');
							}
						}); 
						$('#q_IsMeetingContent').on('click', function(){
							if ($(this).is(':checked')){
								$("#q_tbl-IsMeetingDate").removeClass("q_hide");
							}else{
								$("#q_tbl-IsMeetingDate").addClass("q_hide");
							}
						});	
						var savebuttontext = "Save";
						var updateType = "post";
						if (curId != false) {	
							loadItemData(curId);
							var catId = $("#q_listCategories option:selected").val();	
							hideSelectByCategory(catId);
							savebuttontext = "Update";
							updateType = "update";	
						};	
						$("#q_taggedtext").spHtmlEditor({version: "onprem" });						
						$('#q_taggedtext').click(function() {setRibbonGroupVisibility_FT();});
						setRibbonGroupVisibility_PT();
						$("#q_meetingDate").datepicker();
						$("#q_cancelbutton").click(function(){ cancelModalWindow() });
						$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation(updateType)});
						$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
						$("#q_hideinputfields").removeClass("q_hide");
						$(".se-pre-con").fadeOut("slow");
						clearInterval(intervalStart);
					}
				}
			},"sp.ui.dialog.js");
	},"sp.js");
}

function hideSelectByCategory(currentId){
	$(categoryflags).each(function(){
		var selectId = $(this)[0];
		if(currentId == $(this)[0]){
			var setdeltype = $(this)[1];
			var setmgrp = $(this)[2];
			if (setdeltype){
				if ($('#q_tbl-listDeliverables').hasClass("q_hide")) { $('#q_tbl-listDeliverables').removeClass("q_hide"); };
			} else {
				$('#q_tbl-listDeliverables').addClass("q_hide");
			};
			if (setmgrp){
				if ($('#q_tbl-listMGroupings').hasClass("q_hide")) {$('#q_tbl-listMGroupings').removeClass("q_hide"); };
			} else {
				$('#q_tbl-listMGroupings').addClass("q_hide");
			};
			return;
		}
	});	
}

function fieldValidation(updateType){
	// required values are Text Category, Title, and Content Text Block.
	var isValid = true;
	$("#q_errormessage").text("");
	if ($("#q_listCategories option:selected").val() == null || $("#q_listCategories option:selected").val() == "") {	
			var error_node = $('<p>').text('A text category must be selected to control placement of this content within site pages.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	};
	if ($('#q_title').val() == null || $('#q_title').val().length == 0) {
			var error_node = $('<p>').text('Title is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 
	var othervar = $('#q_taggedtext').spHtmlEditor("gethtml");
	if (othervar.length < 7) {
			var error_node = $('<p>').text('The Content Text Block is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 
	if($("#q_listCategories").val() == "2" || $("#q_listCategories").val() == "3"){
		if ($('#q_IsMeetingContent').is(':checked')) { 
			if ($('#q_meetingDate').val() == "") { 
					var error_node = $('<p>').text('Meeting Date is required');
					$("#q_errormessage").append(error_node);
					isValid = false;
			}; 	
		}; 
	}; 
	if (isValid == true){
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass("q_hide");
		postItem(updateType);
	} else {
		$("#q_errormessage").removeClass("q_hide");
	}
}

function postItem(updateType){
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Textual Content'); 
	if (updateType == "post") {
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	} else {
		this.oListItem = oList.getItemById(curId);
	}
	var titleStr = $('#q_title').val();	
	var htmlStr = $('#q_taggedtext').spHtmlEditor("gethtml");
	var category_text = $("#q_listCategories option:selected").text();
	var catId = new SP.FieldLookupValue();
	catId.set_lookupId(parseInt($("#q_listCategories option:selected").val()));
	this.oListItem.set_item('Title', titleStr);
	this.oListItem.set_item('CategoryValue', category_text);
	this.oListItem.set_item('LookupItemCategory', catId);
	this.oListItem.set_item('QOSTextContent', htmlStr);
	if (parseInt($("#q_listDeliverables option:selected").val()) > 0 && parseInt($("#q_listDeliverables option:selected").val()) != default_delId ) {
		var deltype_text = $("#q_listDeliverables option:selected").text();
		var deltypeId = new SP.FieldLookupValue();
		deltypeId.set_lookupId(parseInt($("#q_listDeliverables option:selected").val()));
		var delnavstr = "del" + $("#q_listDeliverables option:selected").val();
		this.oListItem.set_item('DeliverableNameValue', deltype_text);
		this.oListItem.set_item('Deliverables', deltypeId);
		this.oListItem.set_item('DeliverableNavString', delnavstr);
	} else {
		var emptyStr = "";
		var deltypeId = new SP.FieldLookupValue();
		deltypeId.set_lookupId(parseInt(default_delId));
		this.oListItem.set_item('DeliverableNameValue', emptyStr);
		this.oListItem.set_item('Deliverables', deltypeId);
		this.oListItem.set_item('DeliverableNavString', emptyStr);
	}
	if (parseInt($("#q_listMGroupings option:selected").val()) > 0 && parseInt($("#q_listMGroupings option:selected").val()) != default_mgId) {
		var mgrp_text = $("#q_listMGroupings option:selected").text();
		var mgrpId = new SP.FieldLookupValue();
		mgrpId.set_lookupId(parseInt($("#q_listMGroupings option:selected").val()));
		var mgnavstr = "mg" + $("#q_listMGroupings option:selected").val();
		this.oListItem.set_item('MeasureGroupValue', mgrp_text); 
		this.oListItem.set_item('LookupMeasureGroupings', mgrpId);
		this.oListItem.set_item('MeasureNavString', mgnavstr);
	} else {
		var emptyStr = "";
		var mgrpId = new SP.FieldLookupValue();
		mgrpId.set_lookupId(parseInt(default_mgId));
		this.oListItem.set_item('MeasureGroupValue', emptyStr); 
		this.oListItem.set_item('LookupMeasureGroupings', mgrpId);
		this.oListItem.set_item('MeasureNavString', emptyStr);
	}
	
	if ($('#q_IsMeetingContent').is(':checked')) { 
		var IsMC = 1;
		var meetingDate = new Date($('#q_meetingDate').val());
		this.oListItem.set_item('MeetingDate', meetingDate.toISOString());	
	}else { 
		var IsMC = 0;
		var meetingDate = new Date('12/31/1969');
		this.oListItem.set_item('MeetingDate',meetingDate.toISOString());
	} 
	this.oListItem.set_item('IsMeetingContent', IsMC);
	if (profiledata[0].length >0 ) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1].length >0 ) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2].length >0 ) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3].length >0 ) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4].length >0 ) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5].length >0 ) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }

	this.oListItem.set_item('PublishingApproval', "Draft"); 
	
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
	onItemPostQuerySucceeded();
} 
	
function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function onItemPostQueryFailed(sender, args) {
    //alert('Request failed. ' + args.get_message() + 
   //     '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}


function loadItemData(curId) {
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Textual Content');
	this.oListItem = oList.getItemById(curId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceeded(sender, args) {
	$('#q_title').val(oListItem.get_item("Title"));
	var htmlstr = oListItem.get_item("QOSTextContent");
	$('#q_taggedtext').spHtmlEditor({method: "sethtml", html: htmlstr});
	var catId = oListItem.get_item("LookupItemCategory").get_lookupId();
	hideSelectByCategory(catId);
    $("#q_listCategories option").each(function(){
		if($(this).val() == catId){
			$(this).prop("selected",true);
		} 
	});
	if (oListItem.get_item('Deliverables') != null) {
		var deltypeId = oListItem.get_item("Deliverables").get_lookupId();
		$("#q_listDeliverables option").each(function(){
			if($(this).val() == deltypeId){
				$(this).prop("selected",true);
			} 
		});
	}
	if (oListItem.get_item('LookupMeasureGroupings') != null) {
		var mgrpId = oListItem.get_item("LookupMeasureGroupings").get_lookupId();
		$("#q_listMGroupings option").each(function(){
			if($(this).val() == mgrpId){
				$(this).prop("selected",true);
			} 
		});
	}
	if($("#q_listCategories").val() == "2" || $("#q_listCategories").val() == "3"){
		$("#q_tbl-IsMeetingContent").removeClass("q_hide");
		var IsMC = oListItem.get_item("IsMeetingContent");
		if(IsMC == 1){ 
			$("#q_IsMeetingContent").prop('checked', true); 
			$("#q_tbl-IsMeetingDate").removeClass("q_hide");//, #q_tbl-IsMeetingContent
		}
		var meetDate = new Date(oListItem.get_item("MeetingDate"));
		var formatted_meetDate = (meetDate.getMonth() + 1) + "/" + meetDate.getDate() + "/" + meetDate.getFullYear();
		if (formatted_meetDate == '1/1/1900' || formatted_meetDate == '12/31/1969') { formatted_meetDate = ""; }
		$("#q_meetingDate").val(formatted_meetDate);
	}
}