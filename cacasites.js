// Load the required SharePoint libraries
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/jstest/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
}	
 
jQuery(document).ready(function ($) {
	$.getScript( QOSscriptbase + "qos_ca_commonscripts.js", function () {console.log('qos_ca_commonscripts loaded');} );
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof changeBranding == 'function' && typeof getPageText_Banner == 'function' && typeof loadBreadcrumbsplash == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

// Function to prepare and issue the request to get
//  SharePoint data
function execCrossDomainRequest() {
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			loadBreadcrumbsplash();
			changeBranding();
			getPageText_Banner();
			setContentTypeBanner();
			defineNavigation();
	  },"sp.ui.dialog.js");
  	},"sp.js");
}

function defineNavigation() {
	  setLinksPubActions("To Archive", "Archive", "#q_sidebar-archive", archiveicon_mini);
	  setLinksPubActions("Restore from Archive", "Recover", "#q_sidebar-recover", restoreicon_mini);
	  setLinksPubActions("Permanently Delete", "Delete", "#q_sidebar-delete", deleteicon_mini);
	  setLinksAssocActions("By Data Deliverable Profile", "Deliverables", "#q_sidebar-delassoc", editicon_mini);
	  setLinksAssocActions("By Measure Profile", "Measure%20Details", "#q_sidebar-massoc", editicon_mini);
	  setLinksAssocActions("By Grouped Measures", "Measure%20Groupings", "#q_sidebar-mgassoc", editicon_mini);
	  $("#q_hidesidenavbox").removeClass("q_hide");
}

function setContentTypeBanner() {
	  switch (ctype) {
		  case "Issues":
			  var newUrl = caformsUrl + "new_issues.aspx?carea=" + contentarea;
			  loadListTypeBanner("Manage Page Issues", "Add new Page Issues Item", newUrl);
			  var selectStr = "$select=ID,Title,QOSTextContent,CategoryValue,PublishingApproval,PublishingLastPostDate";
			  var orderbyStr = "$orderby=CategoryValue,Title";
			  var itemsUrl = "https://sites.sp.kp.org/pub/qosdevcat/_api/web/lists/getbytitle('Issues')/items";
			  var executor = new SP.RequestExecutor(casiteUrl);
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocPageIssues,
					error: errorHandler
				  }
			  );
			  break;
	  }
	
}