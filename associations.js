// Load the required SharePoint libraries
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/js/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/js/";
}	
 
jQuery(document).ready(function ($) {
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js", loadRequestor);
});

function loadRequestor() {
	var jsInterval = setInterval(waitforJS, 500);
	function waitforJS() {
		if (typeof getPageText_Banner == 'function' && typeof changeBranding == 'function' && typeof defineNavigation == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
 }


// Function to prepare and issue the request to get
//  SharePoint data
function execCrossDomainRequest() {
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
				changeBranding();
				getPageText_Banner();
				defineNavigation();	
				loadBreadcrumbsplash();
	  },"sp.ui.dialog.js");
  	},"sp.js");
}

function defineNavigation() {
	  setLinksFiles("Business Deliverable File", "General", "#q_sidebar-bizdocs", "mini", "new_uploaddocs");
	  setLinksFiles("Data Deliverable File", "Report", "#q_sidebar-datadocs", "mini", "new_uploaddocs");
	  setLinksContent("Page Text", "Textual", "content", "#q_sidebar-pagetext", "mini", "new_pagetext");
	  setLinksContent("Support Contacts", "QOS", "Support%20Contacts", "#q_sidebar-contacts", "mini", "new_contacts");
	  setLinksContent("WebLinks", "Web", "links", "#q_sidebar-weblinks", "mini", "new_links");
	  setLinksLists("Data Deliverable Profiles", "Deliverables", "deliverables", "#q_sidebar-deltype", "mini", "new_deliverables");
	  setLinksLists("Measures Profiles", "Measure%20Details", "measuredetails", "#q_sidebar-measures", "mini", "new_measures");
	  setLinksLists("Groupings", "Measure%20Groupings", "mgroupings", "#q_sidebar-mgroupings", "mini", "new_measuregroupings");
	  setLinksPubActions("Publish to Staging", "Draft", "#q_sidebar-staging", pushicon_mini);
	  setLinksPubActions("Publish to Production", "Pending", "#q_sidebar-prod", pushicon_mini);
	  setLinksPubActions("Unpublish Items", "Unpublish", "#q_sidebar-unpublish", unpubicon_mini);
	  setLinksPubActions("To Archive", "Archive", "#q_sidebar-archive", archiveicon_mini);
	  setLinksPubActions("Restore from Archive", "Recover", "#q_sidebar-recover", restoreicon_mini);
	  setLinksPubActions("Permanently Delete", "Delete", "#q_sidebar-delete", deleteicon_mini);
	  setLinksAssocActions("By Data Deliverable Profile", "Deliverables", "#q_sidebar-delassoc", editicon_mini);
	  setLinksAssocActions("By Measure Profile", "Measure%20Details", "#q_sidebar-massoc", editicon_mini);
	  setLinksAssocActions("By Grouped Measures", "Measure%20Groupings", "#q_sidebar-mgassoc", editicon_mini);
	  $("#q_hidesidenavbox").removeClass("q_hide");
}

