var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/jstest/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
}	
 
jQuery(document).ready(function ($) {
	$.getScript( QOSscriptbase + "qos_ca_commonscripts.js", function () {console.log('qos_ca_commonscripts loaded');} );
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof changeBranding == 'function' && typeof getPageText_Banner == 'function' && typeof getAuthorizeUserDataNew == 'function' && typeof getInitiative == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		$(".se-pre-con").fadeOut("slow");
		$(".q_spinner").removeClass("q_hide");
		return false;
	}
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog');
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () { console.log("Initiating SP.ClientContext") });
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			$("#q_grid-33-right").addClass("q_borderleft-wide");
			changeBranding();
			getPageText_Banner();
			getAuthorizeUserDataNew();
			getAreasOfWork();
			var initiativeInterval = setInterval(waitforGroups, 200);
			function waitforGroups(){
				if(loadUserHasPermission == true){
					clearInterval(initiativeInterval);
					getInitiative();
					$(".listAreasOfWork a").click(function(){
						var aowid = $(this).attr("data-id");
						$("#q_initiativesBlock > div").each(function(){  
							 var loop = $(this).attr("data-id");
							 $(this).addClass("q_hide");
							 if(aowid == loop){
								$(this).removeClass("q_hide");
							 }
						});
			  		});
			  }
			}
		},"sp.ui.dialog.js");
	},"sp.js");
}

function getAreasOfWork() {
	var selectStr = "$select=ID,Title";  
	var orderbyStr = "$orderby=Title";
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Areas%20Of%20Work')/items?" + selectStr + "&" + orderbyStr;
	$.ajax({
		 url: itemsUrl,
		 method: "GET",
		 headers: { "Accept": "application/json; odata=verbose" },
		 cache: false,
		 async: false,
		 success: function(data){
			var results = (data.d.results == null) ? new Array(data.d) : data.d.results;
		    results = data.d.results;
			$(results).each(function(){ 
				  var title = $(this)[0].Title;
				  var data_id = $(this)[0].ID;
				  var q_spriteindent_med = $("<div>").addClass("q_spriteindent-med").css("margin", "5px 0");
				  var q_dividerwide = $("<div>").addClass("q_dividerwide");
				  var q_rt_rowcell = $("<div>").addClass("q_rt_rowcell-2-title");
				  var a = $("<a>").text(title).css("cursor", "pointer").attr("data-id", data_id).addClass("q_weblinksprite_med");
				  q_rt_rowcell.append(a);
				  q_spriteindent_med.append(q_rt_rowcell);
				  $(".listAreasOfWork").append(q_spriteindent_med).css({"borderRight" : "1px solid #bcb6af", "paddingRight" : "40px"});
				  var div = $("<div>").attr("data-id", data_id).addClass("q_hide");
				  $("#q_initiativesBlock").append(div);
			});
		 },
		 error:function () {
			  console.log("Error");
		  }
		}
	);
}

function getInitiative() {
	var selectStr = "$select=ID,LookupAOWId,Title";
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Initiatives')/items?" + selectStr;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: initiativeLoad,
		  error: errorHandler
		}
	);
}

function initiativeLoad(data){
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$(results).each(function(){ 
		var user_found = false;
		var authors = "aow" + $(this)[0].LookupAOWId + "_" + $(this)[0].ID + " Authors";
		var approvers = "aow" + $(this)[0].LookupAOWId + "_" + $(this)[0].ID + " Approvers";
		$(parse_initiatives).each(function(){
			var current_group = $(this);
			if(current_group = authors || current_group == approvers || current_group == "QOS Owners" || current_group == "QOS Support Team"){
				user_found = true;
			  }
		})
		if (user_found == true){
			  var title = $(this)[0].Title;
			  var id = $(this)[0].ID;
			  var div = $("<div>").css({"clear":"both", "margin": "15px 0"});
			  var q_iconcell = $("<div>").addClass("q_iconcell").css("float", "left");
			  var q_itemtitle = $("<div>").addClass("q_itemtitle");
			  var href = carootUrl + "ca/Pages/new_home.aspx?carea=aow" + $(this)[0].LookupAOWId + "_" + $(this)[0].ID;
			  var a1 = $("<a>").attr("href", href);
			  var a2 = $("<a>").attr("href", href);
			  var img = $("<img>").attr("src", "https://sites.sp.kp.org/pub/qosstgpub/SiteCollectionImages/edit-notes_24x24.png");
			  var q_ca_clipline = $("<span>").addClass("q_ca_clipline").text(title);
			  a1.append(q_ca_clipline);
			  q_itemtitle.append(a1);
			  a2.append(img);
			  q_iconcell.append(a2);
			  div.append(q_iconcell, q_itemtitle);
			  $("#q_initiativesBlock div[data-id='" + $(this)[0].LookupAOWId + "']").append(div);
		}
	});	
}
	
function getAuthorizeUserDataNew() {
	var context = new SP.ClientContext.get_current(carootUrl);
    this.website = context.get_web();
    this.currentUser = website.get_currentUser();
    context.load(currentUser);
    context.executeQueryAsync(Function.createDelegate(this, this.onSuccessMethoddetectUserNew), Function.createDelegate(this, this.onFailureMethodNew));
}

function onSuccessMethoddetectUserNew(){
	var global_user = currentUser.get_loginName();
	var selectStr = "$select=Title,LoginName";
	var curNUID = global_user.slice(10);
    var itemsUrl = carootUrl + "_api/web/siteusers(@v)/Groups?@v=%27i%3A0%23.w%7Ccs%5C" + curNUID + "%27";
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success:loadUsers,
		  error: errorHandler
		}
	);
}

function loadUsers(data){
	var user_found = false;
	var result_title = "";
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	for (i = 0; i < results.length; i++) {
		parse_initiatives[i] = results[i].LoginName;
	};
	loadUserHasPermission = true;
}