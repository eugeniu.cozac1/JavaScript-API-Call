// Javascript library for adding Page Issues Items	
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/jstest/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
	console.log(QOSscriptbase);
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	console.log("IsDlg");
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}

jQuery(document).ready(function ($) {
	console.log("ready");		
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
	    console.log("test");		
		var test = QOSscriptbase + "jQuery.spHtmlEditor.js";
			$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
			$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getNamesOptionsList == 'function' ) {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			overrideSharepointStyles();
			getNamesOptionsList();
			var intervalStart = setInterval(setWindowValues, 5000);	
			function setWindowValues(){
				if (loadNameDone == true){	
				alert(loadNameDone);
					var savebuttontext = "Save";
					var updateType = "post";
					if (curId != false) {	
						savebuttontext = "Update";
						updateType = "update";	
					};
					$("#q_modified, #q_dueDate").datepicker();
					$("#q_taggedtext, #q_inclusions").spHtmlEditor({version: "onprem" });						
					$('#q_taggedtext, #q_inclusions').click(function() {setRibbonGroupVisibility_FT();});
					setRibbonGroupVisibility_PT();	
					$("#q_cancelbutton").click(function(){ cancelModalWindow() });
					$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation(updateType)});
					$(".se-pre-con").fadeOut("slow");
					$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
					$("#q_hideinputfields").removeClass("q_hide");
					clearInterval(intervalStart);
				}
			}
		},"sp.ui.dialog.js");
	},"sp.js");
}

function fieldValidation(updateType){	
	var isValid = true;
	$("#q_errormessage").text("");
	if ($('#q_title').val() == null || $('#q_title').val() == "") {
			var error_node = $('<p>').text('Display Text is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	};
	var othervar = $('#q_taggedtext').spHtmlEditor("gethtml");
	if (othervar.length < 7) {
			var error_node = $('<p>').text('A general description of this measure is required. Web audiences will see this description.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	};
	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		postItem(updateType);
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}
			
function postItem(updateType){
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Issues'); 
	if (updateType == "post") {
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	} else {
		this.oListItem = oList.getItemById(curId);
	}
	var title = $('#q_title').val();
	var description = $('#q_taggedtext').spHtmlEditor("gethtml");
	var dueDate = new Date($('#q_dueDate').val());
	var comments = $('#q_inclusions').spHtmlEditor("gethtml");

	this.oListItem.set_item('Title', title);
	this.oListItem.set_item('Comment', description); 
	this.oListItem.set_item('Modified', dueDate.toISOString());
	this.oListItem.set_item('V3Comments', comments); 

	if (profiledata[0] != "" && profiledata[0] != null) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1] != "" && profiledata[1] != null) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2] != "" && profiledata[2] != null) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3] != "" && profiledata[3] != null) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4] != "" && profiledata[4] != null) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5] != "" && profiledata[5] != null) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	this.oListItem.set_item('PublishingApproval', "Draft"); 
	
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
} 

function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function onItemPostQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function loadItemData(curId) {
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Issues');
	this.oListItem = oList.getItemById(curId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceeded(sender, args) {
   /* $("#q_weburl").val(oListItem.get_item('WebURL').get_url());
	$("#q_urltitle").val(oListItem.get_item('WebURL').get_description());
	$("#q_taggedtext").spHtmlEditor({method: "sethtml", html: oListItem.get_item("Comments")});
	if (oListItem.get_item('Deliverables') != null) {
		var deltypeId = oListItem.get_item("Deliverables").get_lookupId();
		$("#q_listDeliverables option").each(function(){
			if($(this).val() == deltypeId){
				$(this).prop("selected",true);
			} 
		});
	}*/
}
