jQuery(document).ready(function ($) {
	var ourInterval = setInterval(loopInterval, 500);
	var jsInterval = setInterval(waitforJS, 500);
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
	function loopInterval(){
		if (siteType != "" || siteType != null){
			$.getScript(QOSscriptbase + "qos_pub_PageBanner.js", function() { console.log('qos_pub_PageBanner loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_Pages.js", function() { console.log('qos_pub_NavLinks_Pages loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_Deliverables.js", function() { console.log('qos_pub_NavLinks_Deliverables loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_WebLinks.js", function() { console.log('qos_pub_NavLinks_WebLinks loaded');});
			$.getScript(QOSscriptbase + "qos_pub_listSupportContacts.js", function() { console.log('qos_pub_listSupportContacts loaded');});
			$.getScript(QOSscriptbase + "qos_pub_HeaderMeetings.js", function() { console.log('qos_pub_HeaderMeetings loaded');});
			$.getScript(QOSscriptbase + "qos_pub_listMeetings.js", function() { console.log('qos_pub_listMeetings loaded');});
			clearInterval(ourInterval);	
		}
	}
	function waitforJS() {
		if (typeof getPageText_Banner == 'function' && typeof setNavPageLinks == 'function' && typeof getNavDeliverableLinks == 'function' && typeof getNavWebLinks == 'function' && typeof getSupportContacts == 'function' && typeof getHeader_Meetings == 'function' && typeof getList_Meetings == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
 });

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	getPageText_Banner();
	var pageBanner = localStorage.getItem("pageBanner");
	if(pageBanner == "true"){
		setNavPageLinks();
		getNavDeliverableLinks();
		getNavWebLinks();
		getSupportContacts();
		getHeader_Meetings();
		getList_Meetings();
		listMeetings();
	}else{
		writeNoRecordsMessage();
	}
}

