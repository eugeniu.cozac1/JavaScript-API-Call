function currentDataDel_Docs () {

	getDataDel_CurDocs();
	
	function getDataDel_CurDocs() {
		var curYear = new Date().getFullYear();
		var dateFilterStr = "12/31/" + (curYear-3);
		var selectStr = "$select=ID,DeliverableNavString,ReportDataDate";
		var orderbyStr = "$orderby=ReportDataDate%20desc";
		var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27";	
		var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?$top=1&" + selectStr + "&" + orderbyStr + "&" + filterby;
		var executor = new SP.RequestExecutor(catUrl);
		executor.executeAsync(
			{
			  url: itemsUrl,
			  method: "GET",
			  headers: { "Accept": "application/json; odata=verbose" },
			  success: getDataDel_Documents,
			  error: errorHandler
			}
		);		
	}
	
	function getDataDel_Documents(data) {
		  var jsonObject = JSON.parse(data.body);
		  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
		  var delnav = results[0].DeliverableNavString;
		  var curDate = new Date(results[0].ReportDataDate);
		  var dateFilterStr = (curDate.getMonth() + 1) + "/01/" + curDate.getFullYear();
		  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,MedicalCtrValue,ServiceAreaValue,FacilityValue";
		  var orderbyStr = "$orderby=ReportDataDate%20desc";
		  if (granularity == "Medical Center") { orderbyStr = orderbyStr + ",MedicalCtrValue"; }
		  if (granularity == "Service Area") { orderbyStr = orderbyStr + ",ServiceAreaValue"; }
		  if (granularity == "Facility") { orderbyStr = orderbyStr + ",FacilityValue"; }
		  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20ge%20%27" + dateFilterStr + "%27";	
		  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
		  var executor = new SP.RequestExecutor(catUrl);
		  executor.executeAsync(
			  {
				url: itemsUrl,
				method: "GET",
				headers: { "Accept": "application/json; odata=verbose" },
				success: loadListDataDel_curMo,
				error: errorHandler
			  }
		  );		
	}
		
	function loadListDataDel_curMo(data) {
		var jsonObject = JSON.parse(data.body);
		var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
		var columnsNode = $("<div/>").addClass("q_colcontainer_4"); 
		if (results.length > 0) {	
			var curReportDataDt = Date(results[0].ReportDataDate);
			for (var i = 0; i < results.length; i++ ){
				if (Date(results[i].ReportDataDate) == curReportDataDt) {
					if (i == 0) { setCurMoHeader(frequency, curReportDataDt, results[i].ReleaseDate); }
						if (granularity == "Medical Center") { var locName = results[i].MedicalCtrValue; }
						if (granularity == "Service Area") { var locName = results[i].ServiceAreaValue; }
						if (granularity == "Facility") { var locName = results[i].FacilityValue; }
						setCurContentItem(locName, results[i].FileRef, results[i].FileLeafRef, columnsNode);
				} 
			}
		} else { columnNode.append($('h4').text('No items found for this time period')); }
		$("#q_curList").append(columnsNode);
	}
}


function setCurMoHeader(frequency, curReportDataDt, curReleaseDt) {
	var relDate = new Date(curReleaseDt);
	var formattedRelDate = (relDate.getMonth() + 1) + "/" + relDate.getDate() + "/" + relDate.getFullYear();
	var curRptDate = new Date(curReportDataDt);
	if (frequency == "Quarterly") {
		var qtrStr = Math.floor((curRptDate.getMonth() + 3) / 3);
		var periodStr = "Quarter " + qtrStr + " " + curRptDate.getFullYear();
	}
	if (frequency == "Monthly") {
		var qtrStr = curRptDate.getMonth() + 1;
		Date.prototype.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		Date.prototype.getMonthName = function() {return this.monthNames[this.getMonth()];};
		var periodStr = curRptDate.getMonthName() + " " + curRptDate.getFullYear();
	}
	if (frequency == "Weekly") {
		var formattedRptDate = (curRptDate.getMonth() + 1) + "/" + curRptDate.getDate() + "/" + curRptDate.getFullYear();
		var periodStr = "Week of " + formattedRptDate;
	}
	var title = "Reports for " + periodStr + " - Released: " + formattedRelDate;
	$('#q_curList').append($("<h2>").text(title));
}


function setCurContentItem (locName, fileRef, filename, columnsNode) {
	var linkhref = catroot + fileRef;
	var source = getIconFile(filename);
	var divNode1 = $("<div>");
	var divNode2 = $("<div>").addClass("q_column_aligntop");
	var aNode2 = $("<a/>").attr({"title" : locName, "href" : linkhref});
	var divNode3 = $("<div>").addClass("q_column_linkwidth");
	var aNode3 = $("<a/>").attr({"title" : locName, "href" : linkhref}).text( "  " + locName);
	var imgNode = $("<img/>").attr({"src" : source});
	aNode2.append(imgNode);
	divNode2.append(aNode2);
	divNode3.append(aNode3);
	divNode1.append(divNode2, divNode3);
	$(columnsNode).append(divNode1);
}