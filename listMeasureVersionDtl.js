function getMeasureVersionDtl() {
	  var filterby = "$filter=MeasureNavString%20eq%20%27" + mdtlnav + "%27%20and%20MeasureIsCurrent%20eq%201";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Measure%20Versions')/items?" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListMeasureVersionDtl,
			error: errorHandler
		  }
	  );		
}

function loadListMeasureVersionDtl(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var nullstr;
	var last_category = "";
	$("#q_measure-version").addClass("q_boxlist");
	var our_results = { Numerator : results[0].Numerator, Denominator : results[0].Denominator, Inclusions : results[0].Inclusions, Exclusions : results[0].Exclusions };
	$.each(our_results, function( title, content ){ 
		var q_rt_rowcell = $("<div>").addClass("q_rt_rowcell-2");
		var q_dividerwide = $("<div>").addClass("q_dividerwide");
		var q_padright = $("<div>").addClass("q_padright grid-20");
		var q_title = $("<div>").addClass("q_rt_rowcell-1-title").append(title);
		var q_grid80 =  $("<div>").addClass("grid-80 q_rt_rowcell-2-body q_padleft").append(content);
		$("#q_measure-version").addClass("q_boxlist");	
		q_rt_rowcell.append(q_dividerwide);
		q_rt_rowcell.append(q_padright);
		q_rt_rowcell.append(q_grid80);
		q_padright.append(q_title);
		$("#q_measure-version").append(q_rt_rowcell)
	});
}