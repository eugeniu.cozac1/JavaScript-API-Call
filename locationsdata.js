var serviceAreas =  [
 ["CVL", "Central Valley"],
 ["DSA", "Diablo"],
 ["EBA", "East Bay"],
 ["FRS", "Fresno"],
 ["GSA", "Greater Southern Alameda"],
 ["NSA", "Napa/Solano"],
 ["NVL", "North Valley"],
 ["RWC", "Redwood City"],
 ["SFO", "San Franciso"],
 ["SJO", "San Jose"],
 ["SRF", "San Rafael"],
 ["SRO", "Santa Rosa"],
 ["SCL", "Santa Clara"],
 ["SSC", "South Sacramento"],
 ["SSF", "South San Francisco"]
]
 
var medcenters = [
 ["DRV", "Antioch"],
 ["FRS", "Fresno"],
 ["OAK", "Oakland"],
 ["RCH", "Richmond"],
 ["ROS", "Roseville"],
 ["SAC", "Sacramento"],
 ["SLN", "San Leandro"],
 ["SCL", "Santa Clara"],
 ["SFO", "San Francisco"],
 ["SJO", "San Jose"],
 ["SRF", "San Rafael"],
 ["SRO", "Santa Rosa"],
 ["SSF", "South San Francisco"],
 ["SSC", "South Sacramento"],
 ["STK", "Stockton"],
 ["VAC", "Vacaville"],
 ["RWC", "Redwood City"],
 ["VAL", "Vallejo"],
 ["WCR", "Walnut Creek"]
]

var facilities = [
 [" ", " "]
]