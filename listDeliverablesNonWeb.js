function getDeliverables_NonWeb() {
	  var orderbyStr = "$orderby=DeliverableNameValue";
	  var filterby = "$filter=DeliveryMethod%20ne%20%27Web%20Document%27%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items?" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListDeliverable_NonWeb,
			error: errorHandler
		  }
	  );		
}

function loadListDeliverable_NonWeb(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var nullstr;
	var last_category = "";
	$("#q_deliverables-nonweb").addClass("q_boxlist");
	if (results.length > 0) {
		$("#q_deliverables-nonweb").append($("<h3>").text("Reporting Distributed via EMail"));
		$("#q_deliverables-nonweb").append($("<div>").addClass("q_dividerwide"));
	}
		
	for (var i = 0; i < results.length; i++ ){	
		var idval = "q_items_delnonweb_" + i;
		if (last_category.indexOf(results[i].DeliveryMethod) < 0) {
			if (last_category != "") {
				var dividerNode = $("<div>").addClass("q_dividerwide"); 
				$("#q_deliverables-nonweb").append(dividerNode);
			}
			var divNode = $("<div>").attr("id", idval).addClass("grid-100");
			$("#q_deliverables-nonweb").append(divNode);
			setCategoryGrp_NonWebDeliverables("#" + idval, results[i].DeliveryMethod);
			last_category = results[i].DeliveryMethod;
		} else {
			var divNode = $("<div>").attr("id", idval).addClass("grid-100");
			$("#q_deliverables-nonweb").append(divNode);
			setCategoryGrp_NonWebDeliverables("#" + idval, nullstr);
		}
		setContentRow_NonWebDeliverables("#" + idval, results[i].ID, results[i].DeliverableNameValue, results[i].QOSTextContent, results[i].RptFrequency, results[i].IsPHI);
	}
}

function setCategoryGrp_NonWebDeliverables(idval, catvalue) {
	switch (catvalue) {
		case "Web Document":
			var catTitle = "Report Download";
			break;
		case "Email":
			var catTitle = "Via Email";
			break;
		case "Web Page":
			var catTitle = "In-page Views";
			break;
		default:
	}
	var cellNode = $("<div>").addClass("grid-20 q_rt_rowcell-1 q_padright");
	var contentNode = $("<div>").addClass("q_rt_rowcell-1-title").text(catTitle);
	cellNode.append(contentNode);
	$(idval).append(cellNode);
}

function setContentRow_NonWebDeliverables (idval, itemId, delname, content, frequency, PHIflag) {
	var linkhref = pagesUrl + "rptemail.aspx?carea=" + contentarea + "&curId=" + itemId;
	var emText = "Produced " + frequency;
	if (PHIflag == true) { emText = emText + "  |  Contains PHI"; }
	var cellNode = $("<div>").addClass("grid-80 q_rt_rowcell-2-body q_add-margin-bottom");
	var spriteNode = $("<div>").addClass("q_spriteindent-med");
	var node1 = $("<div>").addClass("q_rt_rowcell-2-title");
	
	// create deliverable name line and link
	aNode1 = $("<a>").addClass("q_emailsprite_med").attr("href", linkhref).text(delname);
	aNode1.attr("title", "Click to view details and specific version of the " + delname);
	aNode1.attr("target", "_self");
	node1.append(aNode1);

	// create the second line
	var node2 = $("<div>").addClass("q_rt_rowcell-2-body q_spriteindent-lefttext");
	var emnode = $("<em>").text(emText);
	var spanNode1 = $("<span>").addClass("q_clipline").append(content);
	var spanNode2 = $("<span>").addClass("q_readmore");	
	var aNode2 = $("<a>").addClass("q_viewmore").attr("href", linkhref).text("More");
	aNode2.attr("title", "Click to view details");
	var arrowNode = $("<span>").addClass("q_arrowsprite");
	
	aNode2.append(arrowNode);
	spanNode2.append(aNode2);
	node2.append(emnode, spanNode1, spanNode2);
	
	spriteNode.append(node1);
	spriteNode.append(node2);
	cellNode.append(spriteNode);

	$(idval).append(cellNode);
}
