function getBusinessDeliverables() {
	  var selectStr = "$select=ID,Title,QOSTextContent,CategoryValue,FileRef,FileLeafRef";
	  var orderbyStr = "$orderby=CategoryValue,Title";
	  var filterby = "$filter=CategoryValue%20ne%20%27Report%27%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" +selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadList_BusinessDeliverables,
			error: errorHandler
		  }
	  );		
}

function loadList_BusinessDeliverables(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var nullstr;
	var last_category = "";
	for (var i = 0; i < results.length; i++ ){	
		var idval = "q_items_BusinessDeliverables_" + i;
		if (last_category.indexOf(results[i].CategoryValue) < 0) {
			if (last_category != "") {
				var dividerNode = $("<div>").addClass("q_dividerwide"); 
				$("#q_listBusinessDeliverables").append(dividerNode);
			}
			var divNode = $("<div>").attr("id", idval).addClass("grid-100");
			$("#q_listBusinessDeliverables").append(divNode);
			setCategoryGrp_BusinessDeliverables("#" + idval, results[i].CategoryValue);
			last_category = results[i].CategoryValue;
		} else {
			var divNode = $("<div>").attr("id", idval).addClass("grid-100");
			$("#q_listBusinessDeliverables").append(divNode);
			setCategoryGrp_BusinessDeliverables("#" + idval, nullstr);
		}
		setContentRow_BusinessDeliverables("#" + idval, results[i].QOSTextContent, results[i].CategoryValue, results[i].Title, results[i].FileRef, results[i].FileLeafRef);
	}
	
}

function setCategoryGrp_BusinessDeliverables(idval, catvalue) {
	var cellNode = $("<div>").addClass("grid-20 q_rt_rowcell-1 q_padright");
	var contentNode = $("<div>").addClass("q_rt_rowcell-1-title").text(catvalue);
	cellNode.append(contentNode);
	$(idval).append(cellNode);
}

function setContentRow_BusinessDeliverables (idval, content, catvalue, title,  fileUrl, filename) {
	var fileicon = getIconFile(filename);
	var cellNode = $("<div>").addClass("grid-80 q_rt_rowcell-2-body");
	var divNode1 = $("<div>").addClass("q_spriteindent-med");
	var divNode2 = $("<div>").addClass("q_rt_rowcell-2-title");
	var aNode1 = $("<a/>").attr({"href" : fileUrl, "title" : filename, "target" : "_blank"}).text(title + "  "); //"q_weblinksprite_med"
	var imgNode = $("<img/>").attr({"src" : fileicon});
	aNode1.append(imgNode);
	divNode2.append(aNode1);
	var divNode3  = $("<div>").addClass("q_rt_rowcell-2-body q_spriteindent-lefttext").html(content);
	divNode1.append(divNode2);
	divNode1.append(divNode3);
	cellNode.append(divNode1);
	$(idval).append(cellNode);
}
