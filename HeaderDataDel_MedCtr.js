function getHeader_MedCtrSingle() {
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items";
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListMedCtrSingle,
			error: errorHandler
		  }
	  );		
}

function loadListMedCtrSingle(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	$("#q_header-medctr").addClass("q_banner");
    var wrappDiv = $("<div/>");
	var subHead = $("<div/>").addClass("q_subhead");
	var title = $("<h1/>").text("IMAGUC - Imaging Utilization by Quarter");
	var subHeadnoSprite = $("<div/>").addClass("q_subhead q_notopmargin");
	var related = $("<div/>").addClass("q_related");
	var nopaddingtop = $("<h3/>").addClass("q_nopaddingtop q_add-margin-bottom");
	var a_1 = $("<a/>").attr({"title" : " Click to view Measure Definitions", "href" : ""}).text("Measure Definitions");
	var a_2 = $("<a/>").attr({"title" : " Click to view Measure Resources", "href" : ""}).text("Resources");
	$("#q_sidebar-medctr").addClass("q_textbox");
	var divider = $("<div/>").addClass("q_dividerwide");
	subHead.append(title);
	wrappDiv.append(subHead);
	nopaddingtop.append(a_1);
	nopaddingtop.append("&nbsp;&nbsp;|&nbsp;&nbsp;");
	nopaddingtop.append(a_2);
	related.append(nopaddingtop);
	subHeadnoSprite.append(related);
	wrappDiv.append(subHeadnoSprite);
	$("#q_header-medctr").append(wrappDiv);

	var textbox_ul = $("<ul/>").addClass("q_detailsbox");
	var text_liCategoryValue = $("<li/>");
	var text_spanCategoryValue = $("<span/>").text("Report");
	var text_liRptFrequency = $("<li/>");
	var text_spanRptFrequency = $("<span/>").text("Produced Quarterly");
	var text_liDeliveryMethod = $("<li/>");
	var text_spanDeliveryMethod = $("<span/>").text("Delivered via web");

	text_liCategoryValue.append(text_spanCategoryValue);
	textbox_ul.append(text_liCategoryValue);
	text_liRptFrequency.append(text_spanRptFrequency);
	textbox_ul.append(text_liRptFrequency);
	text_liDeliveryMethod.append(text_spanDeliveryMethod);
	textbox_ul.append(text_liDeliveryMethod);
	$("#q_sidebar-medctr").append(textbox_ul);
}

