function getPageText_Overviews() {
	  var selectStr = "$select=ID,Title,QOSTextContent,CategoryValue";
	  var orderbyStr = "$orderby=CategoryValue";
	  var filterby = "$filter=startswith(CategoryValue,%27Overview%27)%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListPageText_Overviews,
			error: errorHandler
		  }
	  );		
}

function loadListPageText_Overviews(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var nullstr;
	var last_category = "";
	$("#q_listOverviews").addClass("q_textbox");
	for (var i = 0; i < results.length; i++ ){	
		var idval = "q_items_overviews_" + i;
		if (last_category.indexOf(results[i].CategoryValue) < 0) {
			if (last_category != "") {
				var dividerNode = $("<div>").addClass("q_dividerwide"); 
				$("#q_listOverviews").append(dividerNode);
			} 
			var divNode = $("<div>").attr("id", idval).addClass("grid-100");
			$("#q_listOverviews").append(divNode);
			setCategoryGrp_Overviews("#" + idval, results[i].CategoryValue);
			last_category = results[i].CategoryValue;
		} else {
			var divNode = $("<div>").attr("id", idval).addClass("grid-100");
			$("#q_listOverviews").append(divNode);
			setCategoryGrp_Overviews("#" + idval, "");
		}
		setContentRow_Overviews("#" + idval, results[i].QOSTextContent, results[i].CategoryValue);
	}
}

function setCategoryGrp_Overviews(idval, catvalue) {
	var overviewtype = "";
	if (catvalue != null) { overviewtype = catvalue.split("Overview - ")[1]; }
	switch (overviewtype) {
		case "General":
			var catTitle = "Overview";
			break;
		case "Reporting":
			var catTitle = "Reporting Overview";
			break;
		case "Measures":
			var catTitle = "Measures Overview";
			break;
		case "Resources":
			var catTitle = "Resources Overview";
			break;
		case "Meetings":
			var catTitle = "Meetings Overview";
			break;
		default:
			var catTitle = "";	
	}
	var cellNode = $("<div>").addClass("grid-20 q_rt_rowcell-1 q_padright");
	var contentNode = $("<div>").addClass("q_rt_rowcell-1-title").text(catTitle);
	cellNode.append(contentNode);
	$(idval).append(cellNode);
}

function setContentRow_Overviews (idval, content, catvalue) {
	var cellNode = $("<div>").addClass("grid-80 q_rt_rowcell-2-body q_padleft");
	cellNode.append(content);
	var overviewtype = catvalue.split("Overview - ")[1];
	switch (overviewtype) {
		case "Reporting":
			var linkhref = pagesUrl + "topicreports.aspx?carea=" + contentarea;
			break;
		case "Measures":
			var linkhref = pagesUrl + "topicmeasuregrps.aspx?carea=" + contentarea;
			break;
		case "Resources":
			var linkhref = pagesUrl + "topicresources.aspx?carea=" + contentarea;
			break;
		case "Meetings":
			var linkhref = pagesUrl + "topicmeetings.aspx?carea=" + contentarea;
			break;
		default:
			var linkhref = "";		
	}

	if (linkhref != "") {
		var spanNode = $("<span>").addClass("q_readmore q_alignright");	
		var aNode = $("<a>").addClass("q_viewmore").attr("href", linkhref).text("More");
		aNode.attr("title", "Click to view details");
		var arrowNode = $("<span>").addClass("q_arrowsprite");
		aNode.append(arrowNode);
		spanNode.append(aNode);
		cellNode.append(spanNode);
	}
	$(idval).append(cellNode);
}
