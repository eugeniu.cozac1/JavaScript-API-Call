function loadAssocPageText(data) {
  var jsonObject = JSON.parse(data.body);
  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
  var last_category = "";
  var nullstr;
  if (results.length == 0) { writeNoRecordsMessage()};
  for (var i = 0; i < results.length; i++) {
	var windowTitle = "Edit Page Text Metadata";
	var formUrl = caformsUrl + "new_pagetext.aspx?carea=" + contentarea + "&curid=" + results[i].ID;
	var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";
	if (last_category.search(results[i].CategoryValue) < 0) {
		setCategoryHeader (results[i].CategoryValue);
		setColumnHeaders();
		last_category = results[i].CategoryValue;
	}
	setContentItemRow (last_category, onclickstr, results[i].Title, results[i].PublishingApproval, results[i].PublishingLastPostDate, nullstr, nullstr, results[i].Title);
  }

}

function loadAssocContacts(data) {
  var jsonObject = JSON.parse(data.body);
  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
  var last_category = "";
  var nullstr;
  if (results.length == 0) { writeNoRecordsMessage()};
  for (var i = 0; i < results.length; i++) {
	var windowTitle = "Edit Support Contact Metadata";
	var formUrl = caformsUrl + "new_contacts.aspx?carea=" + contentarea + "&curid=" + results[i].ID;
	var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";
	var nameStr = results[i].ContactName.Title;
	if (nameStr == null) {
		nameStr = results[i].GroupEmailName;
	}
	if (results[i].DeliverableNameValue != null) {
			var content = results[i].DeliverableNameValue;
	} else {
			var content;
	}
		// test for change in category value to allow grouping.		
	if (last_category.search(results[i].ContactType) < 0) {
		setCategoryHeader (results[i].ContactType);
		setColumnHeaders();
		last_category = results[i].ContactType;
	}
	setContentItemRow (last_category, onclickstr, nameStr, results[i].PublishingApproval, results[i].PublishingLastPostDate, content, nullstr, nameStr);

  }
}

function loadAssocWebLinks(data) {
  var jsonObject = JSON.parse(data.body);
  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
  if (results.length == 0) { writeNoRecordsMessage()};
  for (var i = 0; i < results.length; i++) {
	var windowTitle = "Edit Web Link Metadata";
	var formUrl = caformsUrl + "new_links.aspx?carea=" + contentarea + "&curid=" + results[i].ID;
	var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";
	var nullstr;
	if (i == 0) {
		setCategoryHeader (nullstr);
		setColumnHeaders();
	}
	setContentItemRow (nullstr, onclickstr, results[i].WebURL.Description, results[i].PublishingApproval, results[i].PublishingLastPostDate, nullstr, nullstr, results[i].WebURL.Description);
  }
}

// Function to handle the success event.
// Prints the data to the page.
function loadAssocGeneralDocs(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var last_category = "";
  	var nullstr;
	if (results.length == 0) { writeNoRecordsMessage()};
	for (var i = 0; i < results.length; i++) {
	  var windowTitle = "Edit Business Deliverable Document Metadata";
	  var formUrl = caformsUrl + "new_documents.aspx?carea=" + contentarea + "&curid=" + results[i].ID + "&ctype=General";
	  var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";		
	  if (last_category.search(results[i].CategoryValue) < 0) {
		  setCategoryHeader (results[i].CategoryValue);
		  setColumnHeaders();
		  last_category = results[i].CategoryValue;
	  }
	  setContentItemRow (last_category, onclickstr, results[i].Title, results[i].PublishingApproval, results[i].PublishingLastPostDate, nullstr, results[i].FileRef, results[i].FileLeafRef, results[i].Title);  
	}
}

function loadAssocReportDocs(data) {
	var jsonObject = JSON.parse(data.body);
  	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var last_category = "";
	var nullstr;
	if (results.length == 0) { writeNoRecordsMessage()};
	for (var i = 0; i < results.length; i++) {
	  var windowTitle = "Edit Data Deliverable Document Metadata";
	  var formUrl = caformsUrl + "new_documents.aspx?carea=" + contentarea + "&curid=" + results[i].ID + "&ctype=Report";
	  var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";			
	  if (last_category.search(results[i].DeliverableNameValue) < 0) {
		  setCategoryHeader (results[i].DeliverableNameValue);
		  setColumnHeaders();
		  last_category = results[i].DeliverableNameValue;
	  }
	  setContentItemRow (last_category, onclickstr, results[i].Title, results[i].PublishingApproval, results[i].PublishingLastPostDate, nullstr, results[i].FileRef, results[i].FileLeafRef, results[i].Title);
	  }
}


function loadAssocDeliverables(data) {
	var jsonObject = JSON.parse(data.body);
  	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var last_category = "";
	var nullstr;
	if (results.length == 0) { writeNoRecordsMessage()};
	for (var i = 0; i < results.length; i++) {
	  var windowTitle = "Edit Data Deliverable Profile Metadata";
	  var formUrl = caformsUrl + "new_deliverables.aspx?carea=" + contentarea + "&curid=" + results[i].ID;
	  var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";	
	  if (last_category.search(results[i].DeliveryMethod) < 0) {
		  setCategoryHeader (results[i].DeliveryMethod);
		  setColumnHeaders();
		  last_category = results[i].DeliveryMethod;
	  }
	  if (results[i].MeasureGroupListValue != null) {
		  var mgrpUrl = capagesUrl + "casplash.aspx?carea=" + contentarea + "&ctype=Measure%20Groupings&mdtlnav=" + results[1].ID;
		  var content = "<p><a href='" + mgrpUrl + "'>Related Grouped Measures</a>";
	  } else {
		  var content = "";
	  }
	  if (results[i].MeasureDetailListValue != null) {
		  if (content != "") {
			  content = content + " &nbsp;|&nbsp; ";
		  } else {
			  content = "<p>";
		  }		
		  var mdtlUrl = capagesUrl + "casplash.aspx?carea=" + contentarea + "&ctype=Measure%20Details&delnav=" + results[i].ID + "&addtitle=" + encodeURIComponent(results[i].DeliverableNameValue);
		  content = content + "<a href='" + mdtlUrl + "'>Related Measures</a></p>";
	  }
	  setContentItemRow (last_category, onclickstr, results[i].DeliverableNameValue, results[i].PublishingApproval, results[i].PublishingLastPostDate, content, nullstr, encodeURIComponent(results[i].DeliverableNameValue));
	}
}


function loadAssocMeasureGroupings(data) {
	var jsonObject = JSON.parse(data.body);
  	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var	resultsfilter = results.filter(function (element) { return element.MeasureGroupValue.indexOf("default") === -1});		
	if (resultsfilter.length == 0) { writeNoRecordsMessage()};
	for (var i = 0; i < resultsfilter.length; i++) {
	  var windowTitle = "Edit Measure Grouping Metadata";
	  var formUrl = caformsUrl + "new_measuregroupings.aspx?carea=" + contentarea + "&curid=" + resultsfilter[i].ID;
	  var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";	
	  var nullstr;
	  if (i == 0) {
		  setCategoryHeader (nullstr);
		  setColumnHeaders();
	  }
	  var content;
	  if (results[i].MeasureDetailListValue != null) {
			var mdtlUrl = capagesUrl + "casplash.aspx?carea=" + contentarea + "&ctype=Measure%20Details&mdtlnav=" + resultsfilter[i].ID + "&addtitle=" + encodeURIComponent(resultsfilter[i].MeasureGroupValue);
	  		content = "<p><a href='" + mdtlUrl + "'>Measures In Group</a></p>";
	  } 
	  setContentItemRow (nullstr, onclickstr, resultsfilter[i].MeasureGroupValue, resultsfilter[i].PublishingApproval, resultsfilter[i].PublishingLastPostDate, content, nullstr , encodeURIComponent(resultsfilter[i].MeasureGroupValue));
	}
}

// Function to handle the success event.
// Prints the data to the page.
function loadAssocMeasureProfiles(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var m = 0;
	var nullstr;
	var mdtlarray = [];
	
	if (results.length == 0) { writeNoRecordsMessage()};
	for (var i = 0; i < results.length; i++) {
		if (i == 0) {
			setCategoryHeader (nullstr);
			setColumnHeaders();
		}
		var grpliststr = results[i].LookupMeasureDetailId;
		var idArray = (grpliststr.results == null) ? new Array(grpliststr) : grpliststr.results;
		
		m = mdtlarray.length;
		if (idArray.length != 0) {
			for (var j = 0; j < idArray.length; j++) {
			  mdtlarray[m] = idArray[j];
			  m++;
			}
		}
	}

	var unique = mdtlarray.filter(function(itm,i,a){
    	return i==a.indexOf(itm);
	});
	mdtlarray = unique;
	for (var j = 0; j < mdtlarray.length; j++) {	
		var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Details')/items(" + mdtlarray[j] + ")";
		var executor = new SP.RequestExecutor(carootUrl);
		executor.executeAsync(
			{
			  url: itemsUrl,
			  method: "GET",
			  headers: { "Accept": "application/json; odata=verbose" },
			  success: writeAssocOneMeasure,
			  error: errorHandler
			}
		);
  	}
	var sortInterval = setInterval(sortItemsDisplay, 200);
	function sortItemsDisplay () {
		var table_count = 0;		
		$(".q_tbl-edititems.q_show").each(function(){
			table_count++;
		});
		if(table_count == mdtlarray.length){
		  $("#q_resultslist .grid-100.q_show").sort(function(a, b){
			  return ($(b).attr("data")) < ($(a).attr("data")) ? 1 : -1;
		  }).appendTo('#q_resultslist');
		  clearInterval(sortInterval);	
		}
		
	}
}

function writeAssocOneMeasure(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results; 
	var createArray = [];
	$(results).each(function(){
		var _this = $(this);
		createArray.push(_this);
	});	
	var nullstr;
	var windowTitle = "Edit Measure Metadata";
	var formUrl = caformsUrl + "new_measures.aspx?carea=" + contentarea + "&curid=" + results[0].ID;
	var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";	
	var mverUrl = capagesUrl + "casplash.aspx?carea=" + contentarea + "&ctype=Measure%20Versions&mdtlnav=mg" + results[0].ID + "&addtitle=" + encodeURIComponent(results[0].MeasureNameValue);
	var content = "<p><a href='" + mverUrl + "'>Measure Definitions</a></p>"; 
	
	setContentItemRow (nullstr, onclickstr, results[0].MeasureNameValue, results[0].PublishingApproval, results[0].PublishingLastPostDate, content, nullstr, nullstr, encodeURIComponent(results[0].MeasureNameValue));
}

function loadAssocMeasureVersions(data) {
  var jsonObject = JSON.parse(data.body);	
  var results = jsonObject.d.results;
  if (results.length == 0) { writeNoRecordsMessage()};
  for (var i = 0; i < results.length; i++) {
		var windowTitle = "Edit Measure Version Metadata";
		var formUrl = caformsUrl + "new_measureversions.aspx?carea=" + contentarea + "&curid=" + results[i].ID;
		var onclickstr = "javascript: showModal('" + formUrl + "', '" + windowTitle + "'); return false;";	
		var nullstr;
		var dateStr = results[i].DefinitionEffectiveDate.split("T");
		var dateseg = dateStr[0].split("-");
		var formattedDate = dateseg[1] + "-" + dateseg[2] + "-" + dateseg[0];
		var newtitle = "Definition Effective Date: " + formattedDate;
		if (results[i].MeasureIsCurrent == true) {
			newtitle = newtitle + " - current definition";
		}		
		if (i == 0) {
			setCategoryHeader (nullstr);
			setColumnHeaders();
		}
		setContentItemRow (nullstr, onclickstr, newtitle, results[i].PublishingApproval, results[i].PublishingLastPostDate, nullstr, nullstr, nullstr);
  }
}

function setCategoryHeader (category) {
		var classname = category;
		if (classname != null) {
			classname = classname.replace(/\s+/g, '');
			classname = classname.replace("/", '-');
			var idshow = "show-" + classname;
			var idhide = "hide-" + classname;
			var table_node = $("<table>").addClass("grid-100 q_tbl-titlebar");
			var tr_node = $("<tr>");
			var td1_node = $("<td>").attr({"colspan": "3"}).addClass("q_titlebarheader").text(category);
			var td2_node = $("<td>").addClass("q_titlebarbutton");
			var show_node = $("<div>").attr({"id": idshow}).addClass("q_bullet_link q_bulletspritedown q_hide").text("Show All").click(function() { showItems(classname);});
			var hide_node = $("<div>").attr({"id": idhide}).addClass("q_bullet_link q_bulletspriteup q_show").text("Collapse").click(function() {hideItems(classname);});
			td2_node.append(show_node);
			td2_node.append(hide_node);
			tr_node.append(td1_node);
			tr_node.append(td2_node);
			table_node.append(tr_node);
			$("#q_resultslist").append(table_node);
		} else {
			var divider_node = $("<div>").addClass("q_dividerwide q_add-margin-top q_add-margin-bottom");
			$("#q_resultslist").append(divider_node);
		}
}

function setColumnHeaders() {
		var table_node = $("<table>").addClass("grid-100 q_tbl-edititems");
		var tr_node = $("<tr>");
		var td1_node = $("<td>").addClass("q_row-edititems-filler");
		var td2_node = $("<td>").addClass("q_label-item").text("Status");
		var td3_node = $("<td>").addClass("q_label-item").text("Publish to Prod");
		tr_node.append(td1_node);
		tr_node.append(td2_node);
		tr_node.append(td3_node);
		table_node.append(tr_node);
		$("#q_resultslist").append(table_node);
}

function setContentItemRow (category, clickstr, title, status, postdate, content, fileUrl, filename, sortValue) {
		var classname = category;
		if (classname != null) {
			classname = classname.replace(/\s+/g, '');
			classname = classname.replace("/", '-');
			var table_node = $("<table>").addClass("grid-100 q_tbl-edititems " + classname + " q_show").attr("data", sortValue);
		} else {
			var table_node = $("<table>").addClass("grid-100 q_tbl-edititems q_show").attr("data", sortValue);
		}
		var tr1_node = $("<tr>");
		var td1_node = $("<td>").addClass("q_iconcell");
		var a1_node = $("<a>").attr({"href": "javascript:;", "onclick": clickstr});
		var img_node = $("<img>").attr({"src": editicon});
		a1_node.append(img_node);
		td1_node.append(a1_node);
		var td2_node = $("<td>").addClass("q_itemtitle");
		var a2_node = $("<a>").attr({"href": "javascript:;", "onclick": clickstr});
		var span2_node = $("<span>").addClass("q_ca_clipline").text(title);
		a2_node.append(span2_node);
		td2_node.append(a2_node);
		tr1_node.append(td1_node);
		tr1_node.append(td2_node);
		var pubstr = setPubInfo(status, postdate);
		var pubarray = pubstr.split(";");
		var td3_node = $("<td>").addClass("q_itempubdetail").text(pubarray[0]);
		var td4_node = $("<td>").addClass("q_itempubdetail").text(pubarray[1]);
		tr1_node.append(td3_node);
		tr1_node.append(td4_node);
		table_node.append(tr1_node);
		if (fileUrl != null) {
			var tr1a_node = $("<tr>");
			var td1a_blanknode = $("<td>").text(" ");
			var td1a_node = $("<td>").attr({"colspan": "3"}).addClass("q_itemfile q_ca_clipline");
			var span_node = $("<span>").addClass("q_label-item").text("Open File: ");
			var link_node = $("<a>").attr({"href": fileUrl}).text(filename);
			td1a_node.append(span_node);
			td1a_node.append(link_node);
			tr1a_node.append(td1a_blanknode);
			tr1a_node.append(td1a_node);
			table_node.append(tr1a_node);
		}
		if (content != null) {
			var tr2_node = $("<tr>");
			var td4_node = $("<td>").text(" ");
			var td5_node = $("<td>").attr({"colspan": "3"}).addClass("q_itemdetail");
			if (content.search("<") < 0 ) {
				var text_node = $("<p>").text(content);
			} else {
				var text_node = $("<span>").addClass("q_ca_clipline");
				text_node.html(content);
			}
			td5_node.append(text_node);
			tr2_node.append(td4_node);
			tr2_node.append(td5_node);
			table_node.append(tr2_node);
		}
		$("#q_resultslist").append(table_node);
}

function setPubInfo(pubstatus, lastpubdate) {
	switch (pubstatus) {
		case "Draft":
			var contentStr = "In Authoring;";
  			break;
		case "Pending":
			var contentStr = "In Staging;";
  			break;
		case "Publish":
			var contentStr = "In Production;";
  			break;
		default:
			var contentStr = "Unpublished;";
  			break;
	};

	if (lastpubdate != null) { 
		var dateStr = lastpubdate.split("T");
		var dateseg = dateStr[0].split("-");
		var formattedDate = dateseg[1] + "-" + dateseg[2] + "-" + dateseg[0];
		contentStr = contentStr +  formattedDate;
	} else {
		contentStr = contentStr + "Not Available";
	}
	return contentStr;	
}


function showItems(classname) {
	if (classname == null) {
		return;
	}
	var classStr = "." + classname;
	var idshow = "#show-" + classname;
	var idhide = "#hide-" + classname;
	jQuery(classStr).each(function (i, value) {
		if ($(this).attr('class').search("q_hide") > 0) {
			$(this).removeClass("q_hide");
			$(this).addClass("q_show");
			$(idshow).removeClass("q_show");
			$(idshow).addClass("q_hide");
			$(idhide).removeClass("q_hide");
			$(idhide).addClass("q_show");
		}
	});
}

function hideItems(classname) {
	if (classname == null) {
		return;
	}
	var classStr = "." + classname;
	var idshow = "#show-" + classname;
	var idhide = "#hide-" + classname;
	jQuery(classStr).each(function (i, value) {
		if ($(this).attr('class').search("q_show") > 0) {
			$(this).removeClass("q_show");
			$(this).addClass("q_hide");
			$(idshow).removeClass("q_hide");
			$(idshow).addClass("q_show");
			$(idhide).removeClass("q_show");
			$(idhide).addClass("q_hide");
		}
	});
}

