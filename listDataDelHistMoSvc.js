function fetchDelNavString() {
	  var selectStr = "$select=ID,DeliverableNavString";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: fetchDataDelDocs,
			error: errorHandler
		  }
	  );		
}

function fetchDataDelDocs(data) {
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var delnav = results[0].DeliverableNavString;
	  var curYear = new Date().getFullYear();
	  var dateFilterStr = "12/31/" + (curYear-3);
	  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,ServiceAreaValue";
	  var orderbyStr = "$orderby=ReportDataDate%20desc,ServiceAreaValue";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadMedicalCenterValues,
			error: errorHandler
		  }
	  );		
}

function loadMedicalCenterValues(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	for (var i = 0; i < results.length; i++ ){
	}
	var mcarray = [];
	for (var i = 0; i < results.length; i++) {
		mcarray[i] = results[i].ServiceAreaValue + ";" + results[i].ID;
	};
	var unique = mcarray.filter(function(itm,i,a){
    	return i==a.indexOf(itm);
	});
	mcarray = unique;
	mcarray.sort();
	for (var i = 0; i < mcarray.length; i++ ){
	  var text = mcarray[i].split(";")[0];
	  var var_id = mcarray[i].split(";")[1];
	  var opt = $("<option>").text(text);
	  if (i == 0) { opt.attr("selected", "selected"); }
	  opt.val(var_id);
	  $("#select_report").append(opt);
	}
	getDataDel_HistMoMedCtr(); 
	$('#select_report').change(function() { getDataDel_HistMoMedCtr(); } );  
}

function getDataDel_HistMoSvc(){
	  var selectStr = "$select=ID,DeliverableNavString";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: getDataDel_HistDocuments,
			error: errorHandler
		  }
	  );		
}

function getDataDel_HistDocuments(data) {
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var delnav = results[0].DeliverableNavString;
	  var curYear = new Date().getFullYear();
	  var dateFilterStr = "12/31/" + (curYear-3);
	  var selectedmedctr = $("#select_report option:selected").text();
	  $("#q_medctrhistorytitle").empty();
	  $("#q_medctrhistorytitle").append($("<h2>").text("Historical Reports for " + selectedmedctr));
	  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,ServiceAreaValue";
	  var orderbyStr = "$orderby=ReportDataDate%20asc,ServiceAreaValue";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27%20and%20ServiceAreaValue%20eq%20%27" + selectedmedctr + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadDataDel_histMo,
			error: errorHandler
		  }
	  );		
}


function loadDataDel_histMo(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var lastYear = "";	
	var curYear = new Date().getFullYear();
	$("#q_rptQtrHist_medctr").empty();
	for (var i = 0; i < results.length; i++ ){
		var newYear = new Date(results[i].ReportDataDate).getFullYear();			
		if ((curYear - newYear) < 3) {	
			var idval = "q_row";
			if (lastYear == "" || lastYear != newYear) {
				lastYear = new Date(results[i].ReportDataDate).getFullYear();
				var colNode = $("<div/>").addClass("q_delhistrow");
				colNode.append($("<div>").addClass("q_delhistrow_title").append($("<h3>").text(lastYear)));
				colNode.append($("<div>").addClass("q_delhistrow_content").append($("<div/>").addClass("q_colcontainer_4 q_add-margin-bottom q_add-margin-top").attr("id", idval + lastYear)));
				$("#q_rptQtrHist_medctr").append(colNode);	
			}
			setHistContentItem("#" + idval + lastYear, results[i].ReportDataDate, results[i].FileRef, results[i].FileLeafRef);	
		}
	}
}


function setHistContentItem (idval, reportDate, fileRef, filename) {
	var linkhref = catroot + fileRef;
	var source = getIconFile(filename);
	var curDate = new Date(reportDate);
	var qtrStr = curDate.getMonth() + 1;
	Date.prototype.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	Date.prototype.getMonthName = function() {return this.monthNames[this.getMonth()];};
	var label = curDate.getMonthName();
	var divNode1 = $("<div>");
	var divNode2 = $("<div>").addClass("q_column_aligntop");
	var aNode2 = $("<a/>").attr({"title" : label, "href" : linkhref, "target" : "_blank"});
	var divNode3 = $("<div>").addClass("q_column_linkwidth");
	var aNode3 = $("<a/>").attr({"title" : label, "href" : linkhref, "target" : "_blank"}).text(label);
	var imgNode = $("<img/>").attr({"src" : source});
	aNode2.append(imgNode);
	divNode2.append(aNode2);
	divNode3.append(aNode3);
	divNode1.append(divNode2, divNode3);
	$(idval).append(divNode1);
}

