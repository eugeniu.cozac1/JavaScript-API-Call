var filterParams;
var verArray = [];
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/js/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/js/";
}	

jQuery(document).ready(function ($) {
	$.getScript(QOSscriptbase + "qos_ca_commonscripts.js", function() { console.log('qos_ca_commonscripts loaded');});
	var jsInterval = setInterval(waitforJS, 500);
	function waitforJS() {
		if (typeof getPageText_Banner == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});
	 
function execCrossDomainRequest() {
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			switch (status.toLowerCase()) {
			  case "draft":
				  filterParams = "PublishingApproval%20eq%20%27Draft%27%20or%20PublishingApproval%20eq%20%27Unpublish%27";
				  var hdrtext = "<h1>Select Items to Publish to the Staging Web Site</h1>";
				  var savebuttontext = "Publish Checked Items to Staging";
				  var newstatus = "Pending";
				  break;
			  case "pending":
				  filterParams = "PublishingApproval%20eq%20%27" + status + "%27";
				  var hdrtext = "<h1>Select Items to Publish to the Production Web Site</h1>";
				  var savebuttontext = "Publish Checked Items to Production";
				  var newstatus = "Publish";
				  break;
			  case "unpublish":
				  filterParams = "PublishingApproval%20eq%20%27Publish%27%20or%20PublishingApproval%20eq%20%27Pending%27";
				  var hdrtext = "<h1>Select Items to Unpublish from Staging and Production sites</h1>";
				  var savebuttontext = "Unpublish Checked Items";
				  var newstatus = "Unpublish";
				  break;
			  case "archive":
				  filterParams = "PublishingApproval%20ne%20%27Archive%27";
				  var hdrtext = "<h1>Select Items to Archive from all views</h1>";
				  var savebuttontext = "Archive Checked Items";
				  var newstatus = "Archive";
				  break;
			  case "recover":
				  filterParams = "PublishingApproval%20eq%20%27Archive%27";
				  var hdrtext = "<h1>Select Items to Recover from Archive</h1>";
				  var savebuttontext = "Recover Checked Items from Archive";
				  var newstatus = "Recover";
				  break;
			  case "delete":
				  filterParams = "PublishingApproval%20eq%20%27Archive%27";
				  var hdrtext = "<h1>Select Items to Permanently Delete</h1>";
				  var savebuttontext = "Permanently Delete Selected Items";
				  var newstatus = "Delete";
				  break;
			};
			$('#q_statuschangelistheader').html(hdrtext);
			changeBranding();
			getPageText_Banner();
			loadTypeBannerBar();	
			setLinksFiles("Business Deliverable File", "General", "#q_sidebar-bizdocs", "mini", "new_uploaddocs");
			setLinksFiles("Data Deliverable File", "Report", "#q_sidebar-datadocs", "mini", "new_uploaddocs");
			setLinksContent("Page Text", "Textual", "content", "#q_sidebar-pagetext", "mini", "new_pagetext");
			setLinksContent("Support Contacts", "QOS", "Support%20Contacts", "#q_sidebar-contacts", "mini", "new_contacts");
			setLinksContent("WebLinks", "Web", "links", "#q_sidebar-weblinks", "mini", "new_links");
			setLinksLists("Data Deliverable Profiles", "Deliverables", "deliverables", "#q_sidebar-deltype", "mini", "new_deliverables");
			setLinksLists("Measures Profiles", "Measure%20Details", "measuredetails", "#q_sidebar-measures", "mini", "new_measures");
			setLinksLists("Groupings", "Measure%20Groupings", "mgroupings", "#q_sidebar-mgroupings", "mini", "new_measuregroupings");
			setLinksPubActions("Publish to Staging", "Draft", "#q_sidebar-staging", pushicon_mini);
			setLinksPubActions("Publish to Production", "Pending", "#q_sidebar-prod", pushicon_mini);
			setLinksPubActions("Unpublish Items", "Unpublish", "#q_sidebar-unpublish", unpubicon_mini);
			setLinksPubActions("To Archive", "Archive", "#q_sidebar-archive", archiveicon_mini);
			setLinksPubActions("Restore from Archive", "Recover", "#q_sidebar-recover", restoreicon_mini);
			setLinksPubActions("Permanently Delete", "Delete", "#q_sidebar-delete", deleteicon_mini);
			setLinksAssocActions("By Data Deliverable Profile", "Deliverables", "#q_sidebar-delassoc", editicon_mini);
			setLinksAssocActions("By Measure Profile", "Measure%20Details", "#q_sidebar-massoc", editicon_mini);
			setLinksAssocActions("By Grouped Measures", "Measure%20Groupings", "#q_sidebar-mgassoc", editicon_mini);
			getTypeBusinessDocs();
			getTypeDataDocs();
			getTypeMeasureGroupings();
			getTypeDeliverables();
			getTypePageText();
			getTypeContacts();
			getTypeWebLinks();
			$("#q_hidesidenavbox").removeClass("q_hide");
	 
			var cancel_ = $("<input>").attr({"type": "button", "value": "Reset Selection"}).click(function(){clearCheckedBoxes();});
			var save_ = $("<input>").attr({"type": "button", "value": savebuttontext}).click(function(){
					$("#q_spinner").removeClass("q_hide");
					var returnval = updateItems(newstatus);
					if (returnval) {
						setTimeout(function() {
							$("#q_publist_hideall").addClass("q_hide");
							$("#q_messagebox_updated").removeClass("q_hide");
							$(".se-pre-con").fadeOut("slow");
							$("#q_updatemsgOK").click(function(){window.location.reload(true);})
						}, 3000);
					} else {
						alert("Error in web services call. Please contact QOS Web (qos.web@kp.org).");
						$(".se-pre-con").fadeOut("slow");
					}	
				});
			cancel_.appendTo("#q_buttonactions");
			save_.appendTo("#q_buttonactions"); 

	},"sp.js");	
};

function getTypePageText () {  
	  var selectStr = "$select=ID,Title,CategoryValue,PublishingApproval";
	  var orderbyStr = "$orderby=Title"
	  var filterby = "$filter=CategoryValue%20ne%20%27Page%20Banner%27%20and%20(" + filterParams + ")%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";
	  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;	
	  var executor = new SP.RequestExecutor(casiteUrl);
	  executor.executeAsync(
			{
			  url: itemsUrl,
			  method: "GET",
			  headers: { "Accept": "application/json; odata=verbose" },
			  success: displayPageText,
			  error: errorHandler
			}
	   );
};

function getTypeContacts (){
	  var selectStr = "$select=ID,GroupEmailName,ContactType,DeliverableNameValue,PublishingApproval,ContactName/Title&$expand=ContactName/Title";
	  var orderbyStr = "$orderby=ContactType%20desc,ContactName/Title,DeliverableNameValue";
	  var filterby = "$filter=(" + filterParams + ")%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";
	  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Support%20Contacts')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(casiteUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: displayContacts,
			error: errorHandler
		  }
	  );
};

function getTypeWebLinks () {
	  var selectStr = "$select=ID,WebURL,DeliverableNameValue,PublishingApproval";
	  var orderbyStr = "$orderby=WebURL";
	  var filterby = "$filter=(" + filterParams + ")%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";
	  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Web%20Links')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(casiteUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: displayWebLinks,
			error: errorHandler
		  }
	  );
};

function getTypeBusinessDocs () {
	  var selectStr = "$select=ID,Title,CategoryValue,PublishingApproval";
	  var orderbyStr = "$orderby=Title";	
	  var filterby = "$filter=CategoryValue%20ne%20%27Report%27%20and%20(" + filterParams + ")%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";  
	  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr  + "&" + filterby;
	  var executor = new SP.RequestExecutor(casiteUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: displayBusinessDocs,
			error: errorHandler
		  }
	  );
};

function getTypeDataDocs () {
	  var selectStr = "$select=ID,Title,DeliverableNameValue,PublishingApproval";
	  var orderbyStr = "$orderby=DeliverableNameValue,Title";
	  var filterby = "$filter=CategoryValue%20eq%20%27Report%27%20and%20(" + filterParams + ")%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	  
	  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(casiteUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: displayDataDocs,
			error: errorHandler
		  }
	  );
};

function getTypeDeliverables () {
	  var selectStr = "$select=ID,DeliverableNameValue,DeliveryMethod,MeasureDetailListValue,MeasureDetailIDListValue,PublishingApproval";
	  var orderbyStr = "$orderby=DeliverableNameValue";
	  var filterby = "$filter=(" + filterParams + ")%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	  
	  var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Deliverables')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(carootUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: displayDeliverables,
			error: errorHandler
		  }
	  );
};

function getTypeMeasureGroupings () {
	  var selectStr = "$select=ID,MeasureGroupValue,LookupMeasureDetailId,PublishingApproval";
	  var orderbyStr = "$orderby=MeasureGroupValue";
	  var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	  
	  var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groupings')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(carootUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: displayMeasureGroupings,
			error: errorHandler
		  }
	  );
};

function getTypeMeasures (mdtlids) {
	for (var i = 0; i < mdtlids.length; i++) {
	// To get the list of measures, we use the Deliverable for a content area as the source
	  var selectStr = "$select=ID,MeasureNameValue,PublishingApproval";
	  var filterby = "$filter=ID%20eq%20" + mdtlids[i];	  
	  var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Details')/items?" + selectStr + "&" + filterby;
	  $.ajax({
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  cache: false,
		  async: false,
		  success: function(data) { displayMeasures(data);},
		  error: function (data) {
			  console.log("Error");
		  }
	  });	
	}
	getTypeMeasureVersions(verArray);

};
function getTypeMeasureVersions (verArray) {
	for (var i = 0; i < verArray.length; i++) { 
		var selectStr = "$select=ID,MeasureNavString,MeasureVersionValue,MeasureNameValue,DefinitionEffectiveDate,PublishingApproval";
		var orderbyStr = "$orderby=MeasureNameValue,DefinitionEffectiveDate%20desc"
		var filterby = "$filter=MeasureNavString%20eq%20%27mg" + verArray[i] + "%27%20and%20(" + filterParams + ")";	  
		var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Versions')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
		var executor = new SP.RequestExecutor(carootUrl);
		executor.executeAsync(
			{
			  url: itemsUrl,
			  method: "GET",
			  headers: { "Accept": "application/json; odata=verbose" },
			  success: displayMeasureVersions,
			  error: errorHandler
			}
		);
	}
};


function displayPageText(data){
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if (results.length > 0) {
		$("#q_pagetextstatus").parent(".q_pubitemslist").removeClass("q_hide");
	}
	for (var i = 0; i < results.length; i++) {
		var idStr = results[i].ID + "-Textual Content";
		var itemtitle = results[i].CategoryValue +" - " + results[i].Title;
		var newcontent = createItemRow(idStr, itemtitle, "q_pagetextstatus");
	};
	return true;
} 

function displayContacts(data){
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if (results.length > 0) {
		$("#q_contactstatus").parent(".q_pubitemslist").removeClass("q_hide");
	}
	for (var i = 0; i < results.length; i++) {
		var idStr = results[i].ID + "-Support Contacts";
		var nameStr = results[i].ContactName.Title;
		if (nameStr == null) {
			nameStr = results[i].GroupEmailName;
		}
		var delname = results[i].DeliverableNameValue;
		if (delname != null) {
			delname = " - " + delname;
		} else {
			delname = "";
		}
		var itemtitle = nameStr + " - " + results[i].ContactType + delname;
		var newcontent = createItemRow(idStr, itemtitle, "q_contactstatus");
	};
	return true;
} 

function displayWebLinks(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if (results.length > 0) {
		$("#q_weblinkstatus").parent(".q_pubitemslist").removeClass("q_hide");
	}
	for (var i = 0; i < results.length; i++) {
		var idStr = results[i].ID + "-Web Links";
		var itemtitle = results[i].WebURL.Description;
		var newcontent = createItemRow(idStr, itemtitle, "q_weblinkstatus");
	};
	return true;
} 

function displayBusinessDocs(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if (results.length > 0) {
		$("#q_bizdocstatus").parent(".q_pubitemslist").removeClass("q_hide");
	}
	for (var i = 0; i < results.length; i++) {
		var idStr = results[i].ID + "-Documents";
		var itemtitle = results[i].CategoryValue +" - " + results[i].Title;
		var newcontent = createItemRow(idStr, itemtitle, "q_bizdocstatus");
	};
	return true;
} 

function displayDataDocs(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if (results.length > 0) {
		$("#q_datadocstatus").parent(".q_pubitemslist").removeClass("q_hide");
	}
	for (var i = 0; i < results.length; i++) {
		var idStr = results[i].ID + "-Documents";
		var itemtitle = results[i].DeliverableNameValue +" - " + results[i].Title;
		var newcontent = createItemRow(idStr, itemtitle, "q_datadocstatus");
	};
	return true;
} 

function displayDeliverables(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if (results.length > 0) {
		$("#q_deltypestatus").parent(".q_pubitemslist").removeClass("q_hide");
	}
	for (var i = 0; i < results.length; i++) {
		var idStr = results[i].ID + "-Deliverables";
		var itemtitle = results[i].DeliverableNameValue;
		var newcontent = createItemRow(idStr, itemtitle, "q_deltypestatus");
	};
	return true;
} 

function displayMeasureGroupings(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if ($('#q_mgroupingstatus > tbody > tr').length > 0) {
		$("#q_mgroupingstatus").parent(".q_pubitemslist").removeClass("q_hide").addClass("q_show");
	}
	var mdtlarray = [];
	for (var i = 0; i < results.length; i++) {
		var writeflag = false;
		var mgstatus = results[i].PublishingApproval.toLowerCase();
		switch (status.toLowerCase()) {
			case 'draft':
				if (mgstatus == 'draft' || mgstatus == 'unpublish') { writeflag = true;}
				break;
			case 'pending':
				if (mgstatus == 'pending') { writeflag = true;}
				break;
			case 'unpublish':
				if (mgstatus == 'publish' || mgstatus == 'pending') { writeflag = true;}
				break;
			case 'archive':
				if (mgstatus != 'archive') { writeflag = true;}
				break;
			case 'recover':
				if (mgstatus == 'archive') { writeflag = true;}
				break;
			case 'delete':
				if (mgstatus == 'archive') { writeflag = true;}
				break;
		}
		if (writeflag == true) {
			var idStr = results[i].ID + "-Measure Groupings";
			var itemtitle = results[i].MeasureGroupValue;
			if (itemtitle.indexOf("_del") > 0) {
				itemtitle = "Default group for Deliverable ID: " + itemtitle.split("default_del")[1];
			}
			if (itemtitle != "default") { 
				var newcontent = createItemRow(idStr, itemtitle, "q_mgroupingstatus");
				$("#q_mgroupingstatus").parent(".q_pubitemslist").removeClass("q_hide");
			}
		}
		var grpliststr = results[i].LookupMeasureDetailId;
		var idArray = (grpliststr.results == null) ? new Array(grpliststr) : grpliststr.results;
		
		m = mdtlarray.length;
		if (idArray.length != 0) {
			for (var j = 0; j < idArray.length; j++) {
			  mdtlarray[m] = idArray[j];
			  m++;
			}
		}
	};
	var unique = mdtlarray.filter(function(itm,i,a){
    	return i==a.indexOf(itm);
	});
	mdtlarray = unique;
	getTypeMeasures(mdtlarray);
}


function displayMeasures(data){
	var newmsg = JSON.stringify(data); 
	var results = (data.d.results == null) ? new Array(data.d) : data.d.results;
	for (var i = 0; i < results.length; i++) {
		var pubstatus = getQueryStringParameter("status");
		var writeflag = false;
		var mgstatus = results[i].PublishingApproval.toLowerCase();
		var veridx = verArray.length;
		var addToVerArray = true;
		for (var j = 0; j < verArray.length; j++) { if (verArray[j] == results[i].ID ) { addToVerArray = false; } }
		if (addToVerArray == true) { verArray.push(results[i].ID); }
		switch (status.toLowerCase()) {
			case 'draft':
				if (mgstatus == 'draft' || mgstatus == 'unpublish') { writeflag = true;}
				break;
			case 'pending':
				if (mgstatus == 'pending') { writeflag = true;}
				break;
			case 'unpublish':
				if (mgstatus == 'publish' || mgstatus == 'pending') { writeflag = true;}
				break;
			case 'archive':
				if (mgstatus != 'archive') { writeflag = true;}
				break;
			case 'recover':
				if (mgstatus == 'archive') { writeflag = true;}
				break;
			case 'delete':
				if (mgstatus == 'archive') { writeflag = true;}
				break;
			default:
		}
		if (writeflag == true) {
			var idStr = results[i].ID + "-Measure Details";
			var itemtitle = results[i].MeasureNameValue;
			createItemRow(idStr, itemtitle, "q_measurestatus");
			$("#q_measurestatus").parent(".q_pubitemslist").removeClass("q_hide");
		}	
	};
} 

function displayMeasureVersions(data){
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results; 
	for (var i = 0; i < results.length; i++) {
		var idStr = results[i].ID + "-Measure Versions";
		var defdate = new Date(results[i].DefinitionEffectiveDate.split("T")[0]);
		var formatted_defdate = (defdate.getMonth() + 1) + "/" + defdate.getDate() + "/" + defdate.getFullYear();
		var itemtitle = "Effective: " + formatted_defdate + "  " + results[i].MeasureNameValue;
		createItemRow(idStr, itemtitle, "q_mversionstatus");
		$("#q_mversionstatus").parent(".q_pubitemslist").removeClass("q_hide");
	};
} 
		
function createItemRow (idStr, title, appendID) {	
	var row_node = $("<tr>");
	var cell1_node = $("<td>").attr({"class": "q_cell-1"});
	var input_node = $("<input>").attr({"type": "checkbox", "class": "q_checkbox", "name": idStr, "id": idStr});
	var cell2_node = $("<td>").attr({"class": "q_cell-2"}).text(title);
	input_node.appendTo(cell1_node);
	cell1_node.appendTo(row_node);
	cell2_node.appendTo(row_node);
	$("#" + appendID).append(row_node);
	return true;

}

function clearCheckedBoxes () {
	$(".q_checkbox").each(function() { $(this).prop("checked", false);});
}

function updateItems(newstatus) {
	var ca_listname;
	var results = document.getElementsByClassName("q_checkbox");
	$(results).each( function() {		
		var idStr = $(this).attr("id");
		var idval = idStr.split("-")[0];
		var contenttype = idStr.split("-")[1];
		switch (contenttype) {
			case "Textual Content":
				ca_listname = "TextualContent";
				break;
			case "Support Contacts":
				ca_listname = "SupportContacts";
				break;
			case "Web Links":
				ca_listname = "WebLinks";
				break;
			case "Documents":
				ca_listname = "Documents";
				break;
			case "Deliverables":
				ca_listname = "Deliverables";
				break;
			case "Measure Details":
				ca_listname = "MeasureDetails";
				break;	
			case "Measure Versions":
				ca_listname = "MeasureVersions";
				break;	
			case "Measure Groupings":
				ca_listname = "MeasureGroupings";
				break;	
			default:
				break;		
		};
		if ($(this).prop('checked') == true) {
			if (siteType.indexOf("stg") > 0) {
				var websvcUrl = "https://services1-qos.appl.kp.org/SharePointData/Relay/" + ca_listname + "/svcncqoswb1/" + idval;
			} else {
				var websvcUrl = "https://services1-qos.appl.kp.org/SharePointDataDev/Relay/" + ca_listname + "/svcncqoswb1/" + idval;	
			}
			if (ca_listname == "Deliverables" || ca_listname == "MeasureDetails" || ca_listname == "MeasureVersions" || ca_listname == "MeasureGroupings") {
				websvcUrl = websvcUrl + "/" + newstatus;
			} else {
				websvcUrl = websvcUrl + "/" + contentarea + "/" + newstatus;
			}
			$.ajax({
				url: websvcUrl,
				method: "GET",
				headers: { "Accept": "application/json; odata=verbose" },
				success: function() {
				},
				error: function () {
					alert("Error in calling web service");
				}
			});	
		}
	});
	return true;
}