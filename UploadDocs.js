// Check for FileReader API (HTML5) support.
if (!window.FileReader) {
	alert('This browser does not support the FileReader API. Please download IE 11 or Chrome before proceeding.');
	var content = "<h4>Uploading and replacing files requires IE 11 or Chrome.</h4>" +
	"<p>If you do not currently have IE 11 or Chrome (the current KP standard), please enter a Track-IT ticket to get them installed.</p>";
	$('#q_errormessage').addClass("q_add-margin-top").html(content);
} else { 
	var parentsite = document.URL.split("/")[4];
	if (parentsite.indexOf("stg") > 0) {
		var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/jstest/";
	} else {
		var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
	}	
	if (document.URL.indexOf("IsDlg") < 0 ){
		$(".ms-breadcrumb-top").each( function() {
			$(this).addClass("q_ca_navbar_backgroundcolor");
		});
		$("#q_spinner").addClass("q_hide");
	} else {
		$("#q_spinner").removeClass("q_hide");
	}
}

jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
		$.getScript( QOSscriptbase + "qos_ca_locationsdata.js",function () {console.log('qos_ca_locationsdata loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof loadProfileData == 'function' && typeof getMeasureGroupingsOptionList == 'function' && typeof getDocCategoriesOptionList == 'function' && typeof getDeliverablesOptionList == 'function' && typeof getSubregionsOptionList == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}	
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			overrideSharepointStyles();
			if (ctype == "General") {
				getMeasureGroupingsOptionList();
				getDocCategoriesOptionList(true);
			} else {	
				getDeliverablesOptionList();
				//getSubregionsOptionList();
				loadServiceAreaOptions();
				loadMedicalCenterOptions();
				$("#q_listDeliverables").change(function() {
				   var id = parseInt($(this).val());
				   loadItemDeliverableId(id);
				});	
			}
			loadProfileData();
			var intervalStart = setInterval(setWindowValues, 500);	
			function setWindowValues(){
				if(loadProfileDataDone == true){
					$('#q_IsMeetingContent').on('click', function(){
						if ($(this).is(':checked')){
							$("#q_tbl-IsMeetingDate").removeClass("q_hide");
						}else{
							$("#q_tbl-IsMeetingDate").addClass("q_hide");
						}
					});
					if (ctype == "General") {
						$("#q_meetingDate").datepicker();				
						$("#q_taggedtext").spHtmlEditor({version: "onprem" });							
						$('#q_taggedtext').click(function() {setRibbonGroupVisibility_FT();});
					} else { $("#q_rptdatadate, #q_rptreleasedate").datepicker();}
					if (curId != false) {		
						var savebuttontext = "Update File Metadata";
						getFileById(curId);
						var updateType = "update";
					} else {
						var updateType = "post";
						var savebuttontext = "Upload/Replace File";
						$("#q_filebrowse").change( function () {
							var filename = $(this).val();
							findFile( filename );
							});
						$("#q_tbl-filebrowse").removeClass("q_hide");
					}
					$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
					setRibbonGroupVisibility_PT();
					$("#q_cancelbutton").bind("click", cancelModalWindow);	
					$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation()});
					$(".se-pre-con").fadeOut("slow");
					clearInterval(intervalStart);
				}
			}
		},'sp.ui.dialog.js');
	},'sp.js');		
}

function fieldValidation(){	
	// required values are Text Category, Title, and Content Text Block.
	var isValid = true;
	$("#q_errormessage").text("");
	if ($('#q_title').val() == null || $('#q_title').val() == "") {
		var error_node = $('<p>').text('Title is required.');
		$("#q_errormessage").append(error_node);
		isValid = false;
	}	
	if ($('#q_filename').val() == null || $('#q_filename').val() == "") {
		var error_node = $('<p>').text('File Name is required.');
		$("#q_errormessage").append(error_node);
		isValid = false;
	}
	if (ctype == "Report") {
		if ($('#q_rptdatadate').val() == null || $('#q_rptdatadate').val() == "") {
			var error_node = $('<p>').text('Report Data Date is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
		}
		if ($('#q_rptreleasedate').val() == null || $('#q_rptreleasedate').val() == "") {
			var error_node = $('<p>').text('Report Release Date is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
		}	
		if (parseInt($("#q_listDeliverables option:selected").val()) < 0 || parseInt($("#q_listDeliverables option:selected").val()) == default_delId ) {
			var error_node = $('<p>').text('A data deliverable profile must be selected.');
			$("#q_errormessage").append(error_node);
			isValid = false;
		}
		if (parseInt($('#q_secgrpid').val()) < 10001 || parseInt($('#q_secgrpid').val()) > 99999) {
			var error_node = $('<p>').text('The Security Group ID must be within the range of 10001 to 99999.');
			$("#q_errormessage").append(error_node);
			isValid = false;
		}
	}else{
		if($("#q_listCategories").val() == "11" || $("#q_listCategories").val() == "12" || $("#q_listCategories").val() == "13"){
		  if ($('#q_IsMeetingContent').is(':checked')) { 
			if ($('#q_meetingDate').val() == "") { 
				var error_node = $('<p>').text('Meeting Date is required');
				$("#q_errormessage").append(error_node);
				isValid = false;
			}; 	
		  }; 		
		}
	}

	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		uploadFile();
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}

function showFields() {
	  switch (ctype) {
		  case "Report":
			  $("#q_tbl-listDeliverables").removeClass("q_hide");
			  $("#q_tbl-title").removeClass("q_hide");
			  $("#q_tbl-filename").removeClass("q_hide");
			  $("#q_tbl-isPHI").removeClass("q_hide");
			  $("#q_tbl-secgrpid").removeClass("q_hide");				  
			  $("#q_tbl-rptdatadate").removeClass("q_hide");
			  $("#q_tbl-rptreleasedate").removeClass("q_hide");
			  $("#q_actionbuttons").removeClass("q_hide");
			  var unhideflag = false;
			  /*$("#q_listSubregions option").each(function() {
				  if ($(this).val() != "") { unhideflag = true;}
			  });
			  if (unhideflag == true) {
					$("#q_tbl-listSubregions").removeClass("q_hide");
			  }; */
			  break;
		  case "General":
			  $("#q_tbl-listCategories").removeClass("q_hide");
			  $("#q_tbl-title").removeClass("q_hide");
			  $("#q_tbl-filename").removeClass("q_hide");
			  $("#q_tbl-taggedtext").removeClass("q_hide");
			  $("#q_tbl-listMgroupings").removeClass("q_hide");
			  $("#q_actionbuttons").removeClass("q_hide");
			  if($("#q_listCategories").val() == "12"){
					$("#q_tbl-IsMeetingContent").removeClass("q_hide");
			  }
			  $("#q_listCategories").on('change', function () {
					var currentId = $(this).val();
					if(currentId == "12" || currentId == "13" || currentId == "11"){
						$("#q_tbl-IsMeetingContent").removeClass("q_hide");
					}else{
						$("#q_tbl-IsMeetingContent, #q_tbl-IsMeetingDate").addClass("q_hide");
					}
			  });
			  break;
		  default:	  
	  };
}

function findFile( filename ){
	var fullUrl = filename.replace(/\\/g, ";");
	var n = fullUrl.lastIndexOf(";");
	n = n + 1;
	var urlseg = fullUrl.substring(n);
	if (urlseg != "") {
		getFileByName(urlseg);
		var fileext = "." + urlseg.split('.').pop();
		var filename = urlseg.split(fileext)[0];
		$("#q_filename").val(filename);
		$("#q_fileextension").text(fileext);
	}
}

function getFileByName(filename) {
	if (siteType == "qosstgpub" || siteType == 'qosstgcat') {
  		var serverRelUrl = "pub/qosstgcat/" + contentarea;
	} else {	
  		var serverRelUrl = "pub/qosdevcat/" + contentarea;
	}
	var serverRelUrl = serverRelUrl + "/docs/" + encodeURIComponent(filename); 	
	var itemsUrl = casiteUrl + "/_api/web/getFileByServerRelativeUrl('/" + serverRelUrl + "')/ListItemAllFields";
	var executor = new SP.RequestExecutor(casiteUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadByFilename,
		  error: showFields
		}
	);
}

function getFileById(fetchId) {
	var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Documents')/items(" + fetchId + ")/FieldValuesAsText";
	var executor = new SP.RequestExecutor(casiteUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadByItem,
		  error: errorHandler
		}
	);
}

function loadByFilename (data) {
	var jsonObject = JSON.parse(data.body);
  	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if (results[0].ID > 0) { 
		getFileById(results[0].ID);
	};	
	showFields();

}

function loadByItem (data) {
  	var jsonObject = JSON.parse(data.body);
  	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var record_found = false;
	var deltypeId, subregId, catValId, mgrpId;
	var selectStr = "$select=DeliverablesId,LookupSubregionsId,LookupDocCategoryId,LookupMeasureGroupingsId";
	var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Documents')/items(" + results[0].ID + ")?" + selectStr;
	$.ajax({
		url: itemsUrl,
		method: "GET",
		headers: { "Accept": "application/json; odata=verbose" },
		cache: false,
		async: false,
		success: function(data){
			var docitem = (data.d.results == null) ? new Array(data.d) : data.d.results;
			deltypeId = docitem[0].DeliverablesId;
			subregId = docitem[0].LookupSubregionsId;
			catValId = docitem[0].LookupDocCategoryId;
			mgrpId = docitem[0].LookupMeasureGroupId;	
			record_found = true;
		},
		error:  function() {
		  record_found = false;
	   	}
	   }
	);
	if(record_found == false){return};
	var col_title = results[0].Title;
	var col_secgrpid = results[0].SecurityGrpID;
	var fullfilename = results[0].FileLeafRef;
	$('#q_title').val(col_title);
	
	var fileext = "." + fullfilename.split('.').pop();
	var filename = fullfilename.split(fileext)[0];
	$("#q_fileextension").text(fileext);
	$('#q_filename').val(filename);
	$('#q_secgrpid').val(col_secgrpid);
	if (ctype == "Report") {
		$("#q_listDeliverables option").each(function(){
			if($(this).val() == deltypeId){
				$(this).attr("selected","selected");
			   } 
			});
		$("#q_listSubregions option").each(function(){
			if($(this).val() == subregId){
				$(this).attr("selected","selected");
			   } 
			});
		if (results[0].isPHI != 'undefined') {
			$('#q_isPHI').prop('checked', results[0].isPHI);
		}
		var releaseDate = new Date(results[0].ReleaseDate);
		var formatted_releaseDate = (releaseDate.getMonth() + 1) + "/" + releaseDate.getDate() + "/" + releaseDate.getFullYear();
		$('#q_rptreleasedate').val(formatted_releaseDate);
		var rptdataDate = new Date(results[0].ReportDataDate);
		var formatted_rptdataDate = (rptdataDate.getMonth() + 1) + "/" + rptdataDate.getDate() + "/" + rptdataDate.getFullYear();
		if (formatted_rptdataDate == '1/1/1900' || formatted_rptdataDate == '12/31/1969') { formatted_rptdataDate = ""; }
		$("#q_rptdatadate").val(formatted_rptdataDate);
		$('#q_medicalcenter').val(results[0].MedicalCtrValue);	
		var id = $("#q_listDeliverables option:selected").val();
		loadItemDeliverableId(parseInt(id));
		$("#q_listServiceArea option").each(function(){
			if($(this).text() == results[0].ServiceAreaValue){
				$(this).attr("selected","selected");
			} 
		});
		$("#q_medicalcenter option").each(function(){
			if($(this).text() == results[0].MedicalCtrValue){
				$(this).attr("selected","selected");
			} 
		});
	} else {
		$('#q_taggedtext').spHtmlEditor({method: "sethtml", html: results[0].QOSTextContent});
		$("#q_listCategories option").each(function(){
			if($(this).val() == catValId){
				$(this).attr("selected","selected");
			   } 
		});
		$("#q_listMGroupings option").each(function(){
			if($(this).val() == mgrpId){
				$(this).attr("selected","selected");
			   } 
		});
		if($("#q_listCategories").val() == "11" || $("#q_listCategories").val() == "12" || $("#q_listCategories").val() == "13"){
			var IsMC = oListItem.get_item("IsMeetingContent");
			if(IsMC == 1){ 
				$("#q_IsMeetingContent").prop('checked', true); 
				$("#q_tbl-IsMeetingDate").removeClass("q_hide");
			}
			var meetDate = new Date(oListItem.get_item("MeetingDate"));
			var formatted_meetDate = (meetDate.getMonth() + 1) + "/" + meetDate.getDate() + "/" + meetDate.getFullYear();
			if (formatted_meetDate == '1/1/1900' || formatted_meetDate == '12/31/1969') { formatted_meetDate = ""; }
			$("#q_meetingDate").val(formatted_meetDate);
		}
	};	
	showFields();
}


// Upload the file.
// You can upload files up to 2 GB with the REST API.
function uploadFile() {
	// Define the folder path for this example.
	var serverRelativeUrlToFolder = 'docs';
	var fileInput = $('#q_filebrowse');
	var serverUrl = casiteUrl;

	// Initiate method calls using jQuery promises.
	// Get the local file as an array buffer.
	var getFile = getFileBuffer();
	getFile.done(function (arrayBuffer) {

		// Add the file to the SharePoint folder.
		var addFile = addFileToFolder(arrayBuffer);
		addFile.done(function (file, status, xhr) {

			// Get the list item that corresponds to the uploaded file.
			//var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri); // Post item function postItem()
			var url_item = file.d.ListItemAllFields.__deferred.uri;
			var executor = new SP.RequestExecutor(casiteUrl);
			executor.executeAsync(
				{
				 url: url_item,
				 method: "GET",
				 headers: { "Accept": "application/json; odata=verbose" },
				 success: postItem,
				 error: errorHandler
				}
			);
		});
		addFile.fail(onError);
	});
	getFile.fail(onError);

	// Get the local file as an array buffer.
	function getFileBuffer() {
		var deferred = jQuery.Deferred();
		var reader = new FileReader();
		reader.onloadend = function (e) {
			deferred.resolve(e.target.result);
		}
		reader.onerror = function (e) {
			deferred.reject(e.target.error);
		}
		reader.readAsArrayBuffer(fileInput[0].files[0]);
		return deferred.promise();
	}

	// Add the file to the file collection in the Shared Documents folder.
	function addFileToFolder(arrayBuffer) {

		// Get the file name from the file input control on the page.
		var parts = fileInput[0].value.split('\\');
		var fileName = parts[parts.length - 1];
		
		// Construct the endpoint.
		// contentare
		var fileCollectionEndpoint = String.format(
				"{0}/" + "_api/web/getfolderbyserverrelativeurl('{1}')/files" + 
				"/add(overwrite=true, url='{2}')",
				serverUrl, serverRelativeUrlToFolder, fileName);
				
		//console.log(fileCollectionEndpoint);
		// Send the request and return the response.
		// This call returns the SharePoint file.
		return jQuery.ajax({
			url: fileCollectionEndpoint,
			type: "POST",
			data: arrayBuffer,
			processData: false,
			headers: {
				"accept": "application/json;odata=verbose",
				"X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
				"content-length": arrayBuffer.byteLength
			}
		});
	}
}

function postItem(data){
	var jsonObject = JSON.parse(data.body);
  	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Documents'); 
	this.oListItem = oList.getItemById(results[0].Id);
	
	var secgrpid = profiledata[10];
	var filename = $("#q_filename").val() + $('#q_fileextension').text();
	
	this.oListItem.set_item('Title', $('#q_title').val());
	this.oListItem.set_item('FileLeafRef', filename);
	
	if(ctype == "General"){
		this.oListItem.set_item('ContentTypeId', ct_bizdoc);
		this.oListItem.set_item('QOSTextContent', $('#q_taggedtext').spHtmlEditor("gethtml"));
		this.oListItem.set_item('SecurityGrpID', parseInt(secgrpid));
		if ($("#q_listCategories option:selected").val() > 0) {
			var catvalue = $("#q_listCategories option:selected").text();
			var catId = new SP.FieldLookupValue();
			catId.set_lookupId(parseInt($("#q_listCategories option:selected").val()));
			this.oListItem.set_item('CategoryValue', catvalue);
			this.oListItem.set_item('LookupDocCategory', catId);
			}
		if (parseInt($("#q_listMGroupings option:selected").val()) > 0 && parseInt($("#q_listMGroupings option:selected").val()) != default_mgId) {
			var mgrp_text = $("#q_listMGroupings option:selected").text();
			var mgrpId = new SP.FieldLookupValue();
			mgrpId.set_lookupId(parseInt($("#q_listMGroupings option:selected").val()));
			var mgnavstr = "mg" + $("#q_listMGroupings option:selected").val();
			this.oListItem.set_item('MeasureGroupValue', mgrp_text); 
			this.oListItem.set_item('LookupMeasureGroupings', mgrpId);
			this.oListItem.set_item('MeasureNavString', mgnavstr);
		} else {
			var emptyStr = "";
			var mgrpId = new SP.FieldLookupValue();
			mgrpId.set_lookupId(parseInt(default_mgId));
			this.oListItem.set_item('MeasureGroupValue', emptyStr); 
			this.oListItem.set_item('LookupMeasureGroupings', mgrpId);
			this.oListItem.set_item('MeasureNavString', emptyStr);
		}
		if ($('#q_IsMeetingContent').is(':checked')) { 
			var IsMC = 1;
			var meetingDate = new Date($('#q_meetingDate').val());
			this.oListItem.set_item('MeetingDate', meetingDate.toISOString());	
		}else { 
			var IsMC = 0;
		} 
		this.oListItem.set_item('IsMeetingContent', IsMC);
	} else {
		// these fields are set for reports only
		this.oListItem.set_item('ContentTypeId', ct_report);
		if ($("#q_PHI").is(":checked")){q_PHI = 1;} else {q_PHI = 0;}
		this.oListItem.set_item('IsPHI', q_PHI);
		if (parseInt($('#q_secgrpid').val()) > 0) { secgrpid = $('#q_secgrpid').val(); }
		this.oListItem.set_item('SecurityGrpID', parseInt(secgrpid));
		var catId = new SP.FieldLookupValue();
		catId.set_lookupId(parseInt("1"));
		this.oListItem.set_item('LookupDocCategory', catId);
		this.oListItem.set_item('CategoryValue', 'Report');
		if (parseInt($("#q_listDeliverables option:selected").val()) > 0 && parseInt($("#q_listDeliverables option:selected").val()) != default_delId ) {
			var deltype_text = $("#q_listDeliverables option:selected").text();
			var deltypeId = new SP.FieldLookupValue();
			deltypeId.set_lookupId(parseInt($("#q_listDeliverables option:selected").val()));
			var delnavstr = "del" + $("#q_listDeliverables option:selected").val();
			this.oListItem.set_item('DeliverableNameValue', deltype_text);
			this.oListItem.set_item('Deliverables', deltypeId);
			this.oListItem.set_item('DeliverableNavString', delnavstr);
		} else {
			var emptyStr = "";
			var deltypeId = new SP.FieldLookupValue();
			deltypeId.set_lookupId(parseInt(default_delId));
			this.oListItem.set_item('DeliverableNameValue', emptyStr);
			this.oListItem.set_item('Deliverables', deltypeId);
			this.oListItem.set_item('DeliverableNavString', emptyStr);
		}
		/*if ( parseInt($("#q_listSubregions option:selected").val()) > 0 ){
			var subregId = new SP.FieldLookupValue();
			subregId.set_lookupId(parseInt($("#q_listSubregions option:selected").val()));
			this.oListItem.set_item('LookupSubregions', subregId);
			this.oListItem.set_item('SubregionValue', $("#q_listSubregions option:selected").text());
		} else {
			var emptyStr = "";var subregId = new SP.FieldLookupValue();
			subregId.set_lookupId(parseInt("6"));
			this.oListItem.set_item('LookupSubregions', subregId);
			this.oListItem.set_item('SubregionValue', emptyStr);
		}*/
		var releaseDate = new Date($('#q_rptreleasedate').val());
		this.oListItem.set_item('ReleaseDate', releaseDate.toISOString());
		var rptdataDate = new Date($('#q_rptdatadate').val());
		this.oListItem.set_item('ReportDataDate', rptdataDate.toISOString());
		this.oListItem.set_item('ServiceAreaValue', " ");
		this.oListItem.set_item('MedicalCtrValue', " ");
		if ($("#q_tbl-listServiceArea").hasClass("q_hide") == false) { this.oListItem.set_item('ServiceAreaValue', $("#q_listServiceArea option:selected").text());}
		if ($("#q_tbl-listServiceArea").hasClass("q_hide") == false) { this.oListItem.set_item('MedicalCtrValue', $("#q_listMedicalCtr option:selected").text());}
	}
	
	if (profiledata[0].length >0 ) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1].length >0 ) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2].length >0 ) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3].length >0 ) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4].length >0 ) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5].length >0 ) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	this.oListItem.set_item('PublishingApproval', "Draft"); 
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}; 

function onItemPostQuerySucceeded(sender, args) {
	/*console.log('query success');
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	*/
}

function onItemPostQueryFailed(sender, args) {
	console.log('query fail');
    if (document.URL.indexOf("IsDlg") < 0 ){ 
		$(".se-pre-con").fadeOut("slow");
		alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
	}
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function loadItemDeliverableId(id) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Deliverables');
	this.oListItem = oList.getItemById(id);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadDelIdQuerySucceeded), Function.createDelegate(this, this.onLoadDelIdQueryFailed)); 
}

function onLoadDelIdQuerySucceeded(sender, args) {
	var fileGranularity = oListItem.get_item('FileGranularity');
	if(fileGranularity == "Medical Center"){
	 	$("#q_tbl-listMedicalCtr").removeClass("q_hide");
	}else {
	    $("#q_tbl-listMedicalCtr").addClass("q_hide");
	}
	if(fileGranularity == "Service Area"){
	 	$("#q_tbl-listServiceArea").removeClass("q_hide");
	}else {
	    $("#q_tbl-listServiceArea").addClass("q_hide");
	}			 
}

function onLoadDelIdQueryFailed(sender, args) {
    if (document.URL.indexOf("IsDlg") < 0 ){ 
		alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
	}
}

function onError(error) {
	alert(error.responseText);
}
