jQuery(document).ready(function ($) {
	var ourInterval = setInterval(loopInterval, 500);
	var jsInterval = setInterval(waitforJS, 500);
	function loopInterval(){
		if (siteType != "" || siteType != null){
			$.getScript(QOSscriptbase + "qos_pub_PageBanner.js", function() { console.log('qos_pub_PageBanner loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_Pages.js", function() { console.log('qos_pub_NavLinks_Pages loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_Deliverables.js", function() { console.log('qos_pub_NavLinks_Deliverables loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_WebLinks.js", function() { console.log('qos_pub_NavLinks_WebLinks loaded');});
			$.getScript(QOSscriptbase + "qos_pub_listSupportContacts.js", function() { console.log('qos_pub_listSupportContacts loaded');});
			$.getScript(QOSscriptbase + "qos_pub_HeaderDataDeliverable.js", function() { console.log('qos_pub_HeaderDataDeliverable loaded');});
			$.getScript(QOSscriptbase + "qos_pub_listDataDel_CurMoSvc.js", function() { console.log('qos_pub_listDataDel_CurMoSvc loaded');});
			$.getScript(QOSscriptbase + "qos_pub_listDataDel_HistMoSvc.js", function() { console.log('qos_pub_listDataDel_HistMoSvc loaded');});
			clearInterval(ourInterval);	
		}
	}
	function waitforJS() {
		if (typeof getPageText_Banner == 'function' && typeof setNavPageLinks == 'function' && typeof getNavDeliverableLinks == 'function' && typeof getNavWebLinks == 'function' && typeof getSupportContacts == 'function' && typeof getHeader_DataDel == 'function' && typeof getDataDel_CurMoSvc == 'function' && typeof fetchDelNavString == 'function'
		 ) {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
 });

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	getPageText_Banner();
	var waitInterval = setInterval(writePageContent, 200);
	function writePageContent() {
		if (loadPageText_BannerDone == true) {
			if(restrictedContent == false){
				setNavPageLinks();
				getNavDeliverableLinks();
				getNavWebLinks();
				getSupportContacts();
				getHeader_DataDel();
				getDataDel_CurMoSvc();
				fetchDelNavString();
			}
			clearInterval(waitInterval);
		}
	}
}
