function getHeader_Deliverables() {
	  var selectStr = "$select=ID,Title,QOSTextContent,CategoryValue,InitiativeValue,ProgramValue,SubprogramValue";
	  var orderbyStr = "$orderby=CategoryValue";
	  var filterby = "$filter=substringof(%27Reporting%27,CategoryValue)%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadHeader_Deliverables,
			error: errorHandler
		  }
	  );		
}

function loadHeader_Deliverables(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var nullstr;
	var last_category = "";
	$("#q_header-deliverables").addClass("q_banner");
	for (var i = 0; i < results.length; i++ ){
		var headertitle = "Reporting for ";
		if (results[i].InitiativeValue != "" && results[i].InitiativeValue != null) {
			headertitle = headertitle + results[i].InitiativeValue;
		} else if (results[i].SubprogramsValue != "" && results[i].SubprogramValue != null) {
			headertitle = headertitle + results[i].SubprogramValue;
		} else {
			headertitle = headertitle + results[i].ProgramValue;
		}
				
		var node1 = $("<div>").addClass("q_subhead");
		var node2 = $("<div>").addClass("q_nosprite").append($("<h1>").text(headertitle));
		var node3 = $("<div>").addClass("grid-100 q_rt_rowcell-2-body").append(results[i].QOSTextContent);
		node1.append(node2,node3);
		$("#q_header-deliverables").append(node1);
	}
}
