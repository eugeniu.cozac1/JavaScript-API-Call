// Load the required SharePoint libraries
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/jstest/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
}	
 
jQuery(document).ready(function ($) {
	$.getScript( QOSscriptbase + "qos_ca_commonscripts.js", function () {console.log('qos_ca_commonscripts loaded');} );
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof changeBranding == 'function' && typeof getPageText_Banner == 'function' && typeof loadBreadcrumbsplash == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

// Function to prepare and issue the request to get
//  SharePoint data
function execCrossDomainRequest() {
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			loadBreadcrumbsplash();
			changeBranding();
			getPageText_Banner();
			setContentTypeBanner();
			defineNavigation();
	  },"sp.ui.dialog.js");
  	},"sp.js");
}

function defineNavigation() {
	  setLinksFiles("Business Deliverable File", "General", "#q_sidebar-bizdocs", "mini", "new_uploaddocs");
	  setLinksFiles("Data Deliverable File", "Report", "#q_sidebar-datadocs", "mini", "new_uploaddocs");
	  setLinksContent("Page Text", "Textual", "content", "#q_sidebar-pagetext", "mini", "new_pagetext");
	  setLinksContent("Support Contacts", "QOS", "Support%20Contacts", "#q_sidebar-contacts", "mini", "new_contacts");
	  setLinksContent("WebLinks", "Web", "links", "#q_sidebar-weblinks", "mini", "new_links");
	  setLinksLists("Data Deliverable Profiles", "Deliverables", "deliverables", "#q_sidebar-deltype", "mini", "new_deliverables");
	  setLinksLists("Measures Profiles", "Measure%20Details", "measuredetails", "#q_sidebar-measures", "mini", "new_measures");
	  setLinksLists("Groupings", "Measure%20Groupings", "mgroupings", "#q_sidebar-mgroupings", "mini", "new_measuregroupings");
	  setLinksPubActions("Publish to Staging", "Draft", "#q_sidebar-staging", pushicon_mini);
	  setLinksPubActions("Publish to Production", "Pending", "#q_sidebar-prod", pushicon_mini);
	  setLinksPubActions("Unpublish Items", "Unpublish", "#q_sidebar-unpublish", unpubicon_mini);
	  setLinksPubActions("To Archive", "Archive", "#q_sidebar-archive", archiveicon_mini);
	  setLinksPubActions("Restore from Archive", "Recover", "#q_sidebar-recover", restoreicon_mini);
	  setLinksPubActions("Permanently Delete", "Delete", "#q_sidebar-delete", deleteicon_mini);
	  setLinksAssocActions("By Data Deliverable Profile", "Deliverables", "#q_sidebar-delassoc", editicon_mini);
	  setLinksAssocActions("By Measure Profile", "Measure%20Details", "#q_sidebar-massoc", editicon_mini);
	  setLinksAssocActions("By Grouped Measures", "Measure%20Groupings", "#q_sidebar-mgassoc", editicon_mini);
	  $("#q_hidesidenavbox").removeClass("q_hide");
	
}

function setContentTypeBanner() {
	  switch (ctype) {
		  case "Textual":
			  var newUrl = caformsUrl + "new_pagetext.aspx?carea=" + contentarea;
			  loadListTypeBanner("Manage Page Text", "Add new Page Text Item", newUrl);
			  var selectStr = "$select=ID,Title,QOSTextContent,CategoryValue,PublishingApproval,PublishingLastPostDate";
			  var orderbyStr = "$orderby=CategoryValue,Title";
			  var filterby = "$filter=CategoryValue%20ne%20%27Page%20Banner%27%20and%20PublishingApproval%20ne%20%27Archive%27";
			  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
			  var executor = new SP.RequestExecutor(casiteUrl);
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocPageText,
					error: errorHandler
				  }
			  );
			  break;
		  case "QOS":
			  var newUrl = caformsUrl + "new_contacts.aspx?carea=" + contentarea;
			  loadListTypeBanner("Manage Support Contacts", "Add new Support Contact Item", newUrl);
			  var selectStr = "$select=ID,GroupEmailName,ContactType,DeliverableNameValue,PublishingApproval,PublishingLastPostDate,ContactName/ID,ContactName/Title&$expand=ContactName/ID,ContactName/Title";
			  var orderbyStr = "$orderby=ContactType%20desc,ContactName/Title,DeliverableNameValue";
			  var filterby = "$filter=PublishingApproval%20ne%20%27Archive%27";
			  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Support%20Contacts')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
			  var executor = new SP.RequestExecutor(casiteUrl);
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocContacts,
					error: errorHandler
				  }
			  );
			  break;
		  case "Web":
			  var newUrl = caformsUrl + "new_links.aspx?carea=" + contentarea;
			  loadListTypeBanner("Manage Web Links", "Add new Web Link Item", newUrl);
			  var selectStr = "$select=ID,WebURL,DeliverableNameValue,PublishingApproval,PublishingLastPostDate";
			  var orderbyStr = "$orderby=WebURL";
			  var filterby = "$filter=PublishingApproval%20ne%20%27Archive%27";
			  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Web%20Links')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
			  var executor = new SP.RequestExecutor(casiteUrl);
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocWebLinks,
					error: errorHandler
				  }
			  );
			  break;
		  case "Report":
			  var newUrl = caformsUrl + "ca_new_uploaddocs.aspx?carea=" + contentarea + "&ctype=Report";
			  loadListTypeBanner("Manage Data Deliverable Files", "Upload Data Deliverable File", newUrl); 
			  var selectStr = "$select=ID,Title,DeliverableNameValue,PublishingApproval,PublishingLastPostDate,FileLeafRef,FileRef";
			  var orderbyStr = "$orderby=DeliverableNameValue,ReportDataDate desc,Title";	
			  var filterby = "$filter=CategoryValue%20eq%20%27Report%27%20and%20PublishingApproval%20ne%20%27Archive%27";  
			  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
			  var executor = new SP.RequestExecutor(casiteUrl);
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocReportDocs,
					error: errorHandler
				  }
			  );
			  break;
		  case "General":
			  var newUrl = caformsUrl + "ca_new_uploaddocs.aspx?carea=" + contentarea + "&ctype=General";
			  loadListTypeBanner("Manage Business Deliverable Files", "Upload Business Deliverable File", newUrl);
			  var selectStr = "$select=ID,Title,CategoryValue,QOSTextContent,PublishingApproval,PublishingLastPostDate,FileLeafRef,FileRef";
			  var orderbyStr = "$orderby=CategoryValue,Title";
			  var filterby = "$filter=CategoryValue%20ne%20%27Report%27%20and%20PublishingApproval%20ne%20%27Archive%27";	  
			  var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
			  var executor = new SP.RequestExecutor(casiteUrl);
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocGeneralDocs,
					error: errorHandler
				  }
			  );
			  break;
		  case "Deliverables":
			  var newUrl = caformsUrl + "new_deliverables.aspx?carea=" + contentarea; 
			  loadListTypeBanner("Manage Data Deliverable Profiles", "Add new Data Deliverable Profile", newUrl);
			  var selectStr = "$select=ID,DeliverableNameValue,DeliveryMethod,MeasureGroupListValue,MeasureGroupIDListValue,MeasureDetailListValue,MeasureDetailIDListValue,PublishingApproval,PublishingLastPostDate";
			  var orderbyStr = "$orderby=DeliveryMethod desc,DeliverableNameValue";
			  var filterby = "$filter=PublishingApproval%20ne%20%27Archive%27%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	  
			  var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Deliverables')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
			  var executor = new SP.RequestExecutor(carootUrl);
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocDeliverables,
					error: errorHandler
				  }
			  );
			  break;
		  case "Measure%20Groupings":
			  var newUrl = caformsUrl + "new_measuregroupings.aspx?carea=" + contentarea; 
			  loadListTypeBanner("Manage Measure Groupings", "Add new Measure Grouping", newUrl);
			  var selectStr = "$select=ID,MeasureGroupValue,MeasureDetailListValue,MeasureDetailIDListValue,PublishingApproval,PublishingLastPostDate";
			  var orderbyStr = "$orderby=MeasureGroupValue";
			  var filterby = "$filter=PublishingApproval%20ne%20%27Archive%27%20and%20AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	  
			  var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groupings')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
			  var executor = new SP.RequestExecutor(carootUrl);	
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocMeasureGroupings,
					error: errorHandler
				  }
			  );
			  break;
		  case "Measure%20Details":
			  var newUrl = caformsUrl + "ca_new_measures.aspx?carea=" + contentarea;
			  // To get the list of measures, we use the Measure Groupings (now with the default associations of all measures)
			  loadListTypeBanner("Manage All Measure Profiles", "111Add new Measure Profile", newUrl);
			  var selectStr = "$select=ID,MeasureGroupValue,LookupMeasureDetailId";
			  var orderbyStr = "$orderby=";
			  var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	  
			  var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groupings')/items?" + selectStr + "&" + filterby;
			  var executor = new SP.RequestExecutor(carootUrl);
			  executor.executeAsync(
				  {
					url: itemsUrl,
					method: "GET",
					headers: { "Accept": "application/json; odata=verbose" },
					success: loadAssocMeasureProfiles,
					error: errorHandler
				  }
			  );
			  break;
		  case "Measure%20Versions":
			  var newUrl = caformsUrl + "new_measureversions.aspx?carea=" + contentarea;
			  if (mdtlnav != false) { newUrl = newUrl + "&mdtlId=" + mdtlnav.split("mg")[1];}
			  if (mdtlnav != null) {
				  var newtitle = "Manage Measure Definitions for " + addtitle; 
				  loadListTypeBanner(newtitle, "Add new Measure Definition", newUrl);
				  var selectStr = "$select=ID,MeasureVersionValue,MeasureNameValue,MeasureNavString,MeasureIsCurrent,DefinitionEffectiveDate,PublishingApproval,PublishingLastPostDate";
				  var orderbyStr = "$orderby=DefinitionEffectiveDate desc,MeasureVersionValue";
				  var filterby = "$filter=MeasureNavString%20eq%20%27" + mdtlnav + "%27%20and%20PublishingApproval%20ne%20%27Archive%27";	  
				  var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Versions')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
				  var executor = new SP.RequestExecutor(carootUrl);
				  executor.executeAsync(
					  {
						url: itemsUrl,
						method: "GET",
						headers: { "Accept": "application/json; odata=verbose" },
						success: loadAssocMeasureVersions,
						error: errorHandler
					  }
				  );
			  } else {
				  var newtitle = "Manage Measure Definitions";
				  LoadListTypeBanner(newtitle, "Add new Measure Definiton", newUrl);
				  var content = "<h4>Search for definitions by first choosing a Measure.</h4>" +
					  "<p>Please select a Measure Profile first to view the versions of definitons defined for that measure.</p>";
				  $('#q_resultslist').addClass("q_add-margin-top").html(content);
			  };
			  break;
	  }
	
}