function getlistDataDel_CorMedCtr(){
	
	var get_date = new Date("11/15/2015");
	
	var get_full_date = (get_date.getMonth() + 1) + "/" + get_date.getDate() + "/" + get_date.getFullYear();
	var get_year = get_date.getFullYear();
	var get_quarter = get_date.getMonth() + 1;
	var show_quarter ="";
	switch (true) {
		case (get_quarter < 4):
			show_quarter = "Q1";
			break;
		case (get_quarter < 7):
			show_quarter = "Q2";
			break;
		case (get_quarter < 10):
			show_quarter = "Q3";
			break;
		case (get_quarter < 13):
			show_quarter = "Q4";
			break;
	}
	var h3_title = $("<h2/>");
	var title = "Current Report, " + show_quarter + " " + get_year + " - Released: " + get_full_date;
	var ul_block = $("<div/>").addClass("ul_block q_add-margin-bottom q_add-marging-top"); 
	var divider = $("<div/>").addClass("q_dividerwide");
	
	var reports = ["Central Valey", "Diablo", "East Bay", "Fresno", "Greater", "Napa Solano", "North Valey", "Redwood City", "San Francisco", 
	"San Jose", "San Rafael", "Santa Clara", "Santa Rosa", "South Sac", "South Santa RosaRosa"];
	
	h3_title.append(title);
	$("#q_rptQtrCurrent_medctr").append(h3_title);
	for (var i = 0; i < reports.length; i++) {
		setContentRow_dataDoc (reports[i], "fileUrl", reports[i] + ".xls", ul_block);
	} 
	$("#q_rptQtrCurrent_medctr").append(ul_block);
	$("#q_rptQtrCurrent_medctr").append(divider);
}

function setContentRow_dataDoc (title, fileUrl, filename, main_div) {
	var linkhref = catroot + fileUrl + "/" + filename;
	var divNode = $("<div/>").addClass("main_div_node");
	var pNode = $("<p/>").attr({"src" : source});
	var aNode = $("<a/>").attr({"title" : title, "href" : linkhref, "target" : "_blank"});
	var source = "";
	switch (true) {
		case (filename.indexOf("xls") >= 0 || filename.indexOf("xlsm") >= 0):
			source = "/_layouts/15/images/icxls.png";
			break;
		case (filename.indexOf("xlxs") >= 0):
			source = "/_layouts/15/images/icxlxs.png";
			break;
		case (filename.indexOf("doc") >= 0):
			source = "/_layouts/15/images/icdoc.png";
			break;
		case (filename.indexOf("docx") >= 0):
			source = "/_layouts/15/images/icdocx.png";
			break;
		case (filename.indexOf("ppt") >= 0):
			source = "/_layouts/15/images/ppt.png";
			break;
		case (filename.indexOf("pptx") >= 0):
			source = "/_layouts/15/images/pptx.png";
			break;
		case (filename.indexOf("pdf") >= 0):
			source = "/_layouts/15/images/icpdf.png";
			break;
 		default:
			source = "/_layouts/15/images/ictxt.gif";
	}
	var imgNode = $("<img/>").attr({"src" : source});
	divNode.append(imgNode);
	aNode.append(title);
	pNode.append(aNode);
	divNode.append(pNode);
	$(main_div).append(divNode);
}
