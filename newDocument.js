// Javascript library for adding Page Text Items	 
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/jstest/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/jstest/";
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}
 
jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
		$.getScript( QOSscriptbase + "qos_ca_locationsdata.js",function () {console.log('qos_ca_locationsdata loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof loadProfileData == 'function' && typeof getMeasureGroupingsOptionList == 'function' && typeof getDocCategoriesOptionList == 'function' && typeof getDeliverablesOptionList == 'function' && typeof getSubregionsOptionList == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {	
	  if (contentarea == false || contentarea == null || contentarea == "undefined") {
		  return false;
	  }
	  SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	  SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	  SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		  SP.SOD.executeOrDelayUntilScriptLoaded(function () {
				overrideSharepointStyles();
				getWebUserData();
				if (ctype == "General") {
					getMeasureGroupingsOptionList();
					getDocCategoriesOptionList(true);
				} else {	
					getDeliverablesOptionList();
					//getSubregionsOptionList();
					loadServiceAreaOptions();
					loadMedicalCenterOptions();	
				}
				loadProfileData();
				var intervalStart = setInterval(setWindowValues, 500);	
				function setWindowValues(){
					if(loadProfileDataDone == true){
						$('#q_IsMeetingContent').on('click', function(){
							if ($(this).is(':checked')){
								$("#q_tbl-IsMeetingDate").removeClass("q_hide");
							}else{
								$("#q_tbl-IsMeetingDate").addClass("q_hide");
							}
						});
						if (ctype == "General") {
							$("#q_meetingDate").datepicker();				
							$("#q_taggedtext").spHtmlEditor({version: "onprem" });						
							$('#q_taggedtext').click(function() {setRibbonGroupVisibility_FT();});
						} else {	
							$("#q_rptdatadate, #q_rptreleasedate").datepicker();
						}
					  	if (curId != false) {		
						  	var savebuttontext = "Update File Metadata";
							loadItemData(curId);
						} else {
							alert("An id value must be supplied in the URL");
						}
						$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
						setRibbonGroupVisibility_PT();
						$("#q_cancelbutton").click(function(){ cancelModalWindow() });			
						$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation()});
						$(".se-pre-con").fadeOut("slow");
						clearInterval(intervalStart);
					}
				}
		  },'sp.ui.dialog.js');
	  },'sp.js');
}

function fieldValidation(){	
	var isValid = true;
	$("#q_errormessage").text("");
	if ($('#q_title').val() == null || $('#q_title').val() == "") {
		var error_node = $('<p>').text('Title is required.');
		$("#q_errormessage").append(error_node);
		isValid = false;
	}	
	if ($('#q_filename').val() == null || $('#q_filename').val() == "") {
		var error_node = $('<p>').text('File Name is required.');
		$("#q_errormessage").append(error_node);
		isValid = false;
	}
	if (ctype == "Report") {
		if ($('#q_rptdatadate').val() == null || $('#q_rptdatadate').val() == "") {
			var error_node = $('<p>').text('Report Data Date is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
		}
		if ($('#q_rptreleasedate').val() == null || $('#q_rptreleasedate').val() == "") {
			var error_node = $('<p>').text('Report Release Date is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
		}
		if (parseInt($("#q_listDeliverables option:selected").val()) < 0 || parseInt($("#q_listDeliverables option:selected").val()) == default_delId ) {
			var error_node = $('<p>').text('A data deliverable profile must be selected.');
			$("#q_errormessage").append(error_node);
			isValid = false;
		}
		if (parseInt($('#q_secgrpid').val()) < 10001 || parseInt($('#q_secgrpid').val()) > 99999) {
			var error_node = $('<p>').text('The Security Group ID must be within the range of 10001 to 99999.');
			$("#q_errormessage").append(error_node);
			isValid = false;
		}
	}else{
		if($("#q_listCategories").val() == "11" || $("#q_listCategories").val() == "12" || $("#q_listCategories").val() == "13"){
		  if ($('#q_IsMeetingContent').is(':checked')) { 
			if ($('#q_meetingDate').val() == "") { 
				var error_node = $('<p>').text('Meeting Date is required');
				$("#q_errormessage").append(error_node);
				isValid = false;
			}; 	
		  }; 		
		}
	}

	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');	
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		postItem();
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}

function showFields() {
		  switch (ctype) {
			  case "Report":
				  $("#q_tbl-listDeliverables").removeClass("q_hide");
				  $("#q_tbl-title").removeClass("q_hide");
				  $("#q_tbl-filename").removeClass("q_hide");
				  $("#q_tbl-isPHI").removeClass("q_hide");
				  $("#q_tbl-secgrpid").removeClass("q_hide");				  
				  $("#q_tbl-rptdatadate").removeClass("q_hide");
				  $("#q_tbl-rptreleasedate").removeClass("q_hide");
				  $("#q_listDeliverables").change(function() {
				 	var id = parseInt($(this).val());
				 	loadItemDeliverableId(id)
			  	  });
				  $("#q_actionbuttons").removeClass("q_hide");
				  /*var unhideflag = false;
				  $("#q_listSubregions option").each(function() {
					  if ($(this).val() != "") { unhideflag = true;}
				  });
				  if (unhideflag == true) {
				  		$("#q_tbl-listSubregions").removeClass("q_hide");
				  };*/
				  break;
			  case "General":
				  $("#q_tbl-listCategories").removeClass("q_hide");
				  $("#q_tbl-title").removeClass("q_hide");
				  $("#q_tbl-filename").removeClass("q_hide");
				  $("#q_tbl-taggedtext").removeClass("q_hide");
				  $("#q_tbl-listMgroupings").removeClass("q_hide");
				  $("#q_actionbuttons").removeClass("q_hide");
				  if($("#q_listCategories").val() == "12"){
						$("#q_tbl-IsMeetingContent").removeClass("q_hide");
				  }
				  $("#q_listCategories").on('change', function () {
						var currentId = $(this).val();
						if(currentId == "12" || currentId == "13" || currentId == "11"){
							$("#q_tbl-IsMeetingContent").removeClass("q_hide");
						}else{
							$("#q_tbl-IsMeetingContent, #q_tbl-IsMeetingDate").addClass("q_hide");
						}
				  });
				  break;
			  default:	  
		  };
}

function postItem(){
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Documents'); 
	this.oListItem = oList.getItemById(curId);
	
	var secgrpid = profiledata[10];
	var filename = $("#q_filename").val() + $('#q_fileextension').text();
	
	this.oListItem.set_item('Title', $('#q_title').val());
	this.oListItem.set_item('FileLeafRef', filename);
	
	if(ctype == "General"){
		this.oListItem.set_item('QOSTextContent', $('#q_taggedtext').spHtmlEditor("gethtml"));
		if ($("#q_listCategories option:selected").val() > 0) {
			var catvalue = $("#q_listCategories option:selected").text();
			var catId = new SP.FieldLookupValue();
			catId.set_lookupId(parseInt($("#q_listCategories option:selected").val()));
			this.oListItem.set_item('CategoryValue', catvalue);
			this.oListItem.set_item('LookupDocCategory', catId);
			}
		if (parseInt($("#q_listMGroupings option:selected").val()) > 0 && parseInt($("#q_listMGroupings option:selected").val()) != default_mgId) {
			var mgrp_text = $("#q_listMGroupings option:selected").text();
			var mgrpId = new SP.FieldLookupValue();
			mgrpId.set_lookupId(parseInt($("#q_listMGroupings option:selected").val()));
			var mgnavstr = "mg" + $("#q_listMGroupings option:selected").val();
			this.oListItem.set_item('MeasureGroupValue', mgrp_text); 
			this.oListItem.set_item('LookupMeasureGroupings', mgrpId);
			this.oListItem.set_item('MeasureNavString', mgnavstr);
		} else {
			var emptyStr = "";
			var mgrpId = new SP.FieldLookupValue();
			mgrpId.set_lookupId(parseInt(default_mgId));
			this.oListItem.set_item('MeasureGroupValue', emptyStr); 
			this.oListItem.set_item('LookupMeasureGroupings', mgrpId);
			this.oListItem.set_item('MeasureNavString', emptyStr);
		}
		if ($('#q_IsMeetingContent').is(':checked')) { 
			var IsMC = 1;
			var meetingDate = new Date($('#q_meetingDate').val());
			this.oListItem.set_item('MeetingDate', meetingDate.toISOString());	
		}else { 
			var IsMC = 0;
			if($("#q_listCategories").val() != "12" || $("#q_listCategories").val() != "13" || $("#q_listCategories").val() != "11"){
				this.oListItem.set_item('MeetingDate', "12/31/1969");	
			}
		} 
		this.oListItem.set_item('IsMeetingContent', IsMC);
	} else {
		// these fields are set for reports only
		if ($("#q_PHI").is(":checked")){q_PHI = 1;} else {q_PHI = 0;}
		this.oListItem.set_item('IsPHI', q_PHI);
		if (parseInt($('#q_secgrpid').val()) > 0) { secgrpid = $('#q_secgrpid').val(); }
			this.oListItem.set_item('SecurityGrpID', parseInt(secgrpid));
		if (parseInt($("#q_listDeliverables option:selected").val()) > 0 && parseInt($("#q_listDeliverables option:selected").val()) != default_delId ) {
			var deltype_text = $("#q_listDeliverables option:selected").text();
			var deltypeId = new SP.FieldLookupValue();
			deltypeId.set_lookupId(parseInt($("#q_listDeliverables option:selected").val()));
			var delnavstr = "del" + $("#q_listDeliverables option:selected").val();
			this.oListItem.set_item('DeliverableNameValue', deltype_text);
			this.oListItem.set_item('Deliverables', deltypeId);
			this.oListItem.set_item('DeliverableNavString', delnavstr);
		} else {
			var emptyStr = "";
			var deltypeId = new SP.FieldLookupValue();
			deltypeId.set_lookupId(parseInt(default_delId));
			this.oListItem.set_item('DeliverableNameValue', emptyStr);
			this.oListItem.set_item('Deliverables', deltypeId);
			this.oListItem.set_item('DeliverableNavString', emptyStr);
		}
		/*if ( parseInt($("#q_listSubregions option:selected").val()) > 0 ){
			var subregId = new SP.FieldLookupValue();
			subregId.set_lookupId(parseInt($("#q_listSubregions option:selected").val()));
			this.oListItem.set_item('LookupSubregions', subregId);
			this.oListItem.set_item('SubregionValue', $("#q_listSubregions option:selected").text());
		} else {
			var emptyStr = "";var subregId = new SP.FieldLookupValue();
			subregId.set_lookupId(parseInt("6"));
			this.oListItem.set_item('LookupSubregions', subregId);
			this.oListItem.set_item('SubregionValue', emptyStr);
		}*/
		var releaseDate = new Date($('#q_rptreleasedate').val());
		this.oListItem.set_item('ReleaseDate', releaseDate.toISOString());
		var rptdataDate = new Date($('#q_rptdatadate').val());
		this.oListItem.set_item('ReportDataDate', rptdataDate.toISOString());
		this.oListItem.set_item('ServiceAreaValue', " ");
		this.oListItem.set_item('MedicalCtrValue', " ");
		if ($("#q_tbl-listServiceArea").hasClass("q_hide") == false) { this.oListItem.set_item('ServiceAreaValue', $("#q_listServiceArea option:selected").text());}
		if ($("#q_tbl-listMedicalCtr").hasClass("q_hide") == false) { this.oListItem.set_item('MedicalCtrValue', $("#q_listMedicalCtr option:selected").text());}
	}
	
	if (profiledata[0].length >0 ) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1].length >0 ) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2].length >0 ) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3].length >0 ) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4].length >0 ) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5].length >0 ) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	this.oListItem.set_item('PublishingApproval', "Draft"); 
	
	this.oListItem.update();
		clientContext.load(this.oListItem);
		clientContext.executeQueryAsync(
			Function.createDelegate(this, this.onItemPostQuerySucceeded), 
			Function.createDelegate(this, this.onItemPostQueryFailed)
	);
}; 

function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function onItemPostQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function loadItemData(curId) {
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Documents');
	this.oListItem = oList.getItemById(curId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceeded(sender, args) {
	$('#q_title').val(oListItem.get_item("Title"));
	var fullfilename = this.oListItem.get_item('FileLeafRef');
	var fileext = "." + fullfilename.split('.').pop();
	var filename = fullfilename.split(fileext)[0];
	var medicalCtrValue = this.oListItem.get_item('MedicalCtrValue');
	var serviceAreaValue = this.oListItem.get_item('ServiceAreaValue');
	$("#q_fileextension").text(fileext);
	$('#q_filename').val(filename);
	if (ctype == "General") {
		$('#q_taggedtext').spHtmlEditor({method: "sethtml", html: oListItem.get_item("QOSTextContent")});
		var catId = oListItem.get_item("LookupDocCategory").get_lookupId();
		$("#q_listCategories option").each(function(){
			if($(this).val() == catId){
				$(this).prop("selected",true);
			} 
		});
		if (oListItem.get_item('LookupMeasureGroupings') != null) {
			var mgrpId = oListItem.get_item("LookupMeasureGroupings").get_lookupId();
			$("#q_listMGroupings option").each(function(){
				if($(this).val() == mgrpId){
					$(this).prop("selected",true);
				} 
			});
		}
		var IsMC = oListItem.get_item("IsMeetingContent");
		$("#q_listCategories").on('change', function () {
			var currentId = $(this).val();
			if(currentId != "12" || currentId != "13" || currentId != "11"){
				$("#q_meetingDate").val("");
				$("#q_IsMeetingContent").prop('checked', false); 
			}
		});
		if(IsMC == 1){ 
			$("#q_IsMeetingContent").prop('checked', true); 
			$("#q_tbl-IsMeetingDate").removeClass("q_hide");
			$("#q_tbl-IsMeetingContent").removeClass("q_hide");
			var meetDate = new Date(oListItem.get_item("MeetingDate"));
			var formatted_meetDate = (meetDate.getMonth() + 1) + "/" + meetDate.getDate() + "/" + meetDate.getFullYear();
			if (formatted_meetDate == '1/1/1900' || formatted_meetDate == '12/31/1969') { formatted_meetDate = ""; }
			$("#q_meetingDate").val(formatted_meetDate);
		}else{
			$("#q_IsMeetingContent").prop('checked', false); 
		}
	} else {
		//populate Report fields
		if (oListItem.get_item('Deliverables') != null) {
			var deltypeId = oListItem.get_item("Deliverables").get_lookupId();
			$("#q_listDeliverables option").each(function(){
				if($(this).val() == deltypeId){
					$(this).prop("selected",true);
				} 
			});
		}
		if (oListItem.get_item("IsPHI") == "true") { $('#q_isPHI').prop('checked', true); }
		var newDate = new Date(oListItem.get_item("ReleaseDate"));
		var releaseDate = (newDate.getMonth() + 1) + "/" + newDate.getDate() + "/" + newDate.getFullYear();
		var newDate2 = new Date(oListItem.get_item("ReportDataDate"));
		var rptDataDate = (newDate2.getMonth() + 1) + "/" + newDate2.getDate() + "/" + newDate2.getFullYear();
		$('#q_rptreleasedate').val(releaseDate);
		$('#q_rptdatadate').val(rptDataDate);
		if (oListItem.get_item("LookupSubregions") != null) {
			var subregId = oListItem.get_item("LookupSubregions").get_lookupId();
			$("#q_listSubregions option").each(function(){
				if($(this).val() == subregId){
					$(this).prop("selected", true);
				   } 
				});
		}
		if (oListItem.get_item("IsPHI") != 'undefined') { $('#q_isPHI').prop('checked', oListItem.get_item("IsPHI")); }
		$('#q_medicalcenter').val(oListItem.get_item("MedicalCtrValue"));
		$('#q_secgrpid').val(oListItem.get_item("SecurityGrpID"));
		var id = $("#q_listDeliverables option:selected").val();
		loadItemDeliverableId(parseInt(id));
		$("#q_listServiceArea option").each(function(){
			if($(this).text() == serviceAreaValue){
				$(this).prop("selected",true);
			} 
		});
		$("#q_listMedicalCtr option").each(function(){
			if($(this).text() == medicalCtrValue){
				$(this).prop("selected", true);
			}
		});
	}
	showFields();
}

function loadItemDeliverableId(id) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Deliverables');
	this.oListItem = oList.getItemById(id);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceededDeliverableId), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceededDeliverableId(sender, args) {
	var fileGranularity = oListItem.get_item('FileGranularity');
	console.log(fileGranularity);
	if(fileGranularity == "Medical Center"){
	 	$("#q_tbl-listMedicalCtr").removeClass("q_hide");
	}else {
	    $("#q_tbl-listMedicalCtr").addClass("q_hide");
	}
	if(fileGranularity == "Service Area"){
	 	$("#q_tbl-listServiceArea").removeClass("q_hide");
	}else {
	    $("#q_tbl-listServiceArea").addClass("q_hide");
	}			 
}