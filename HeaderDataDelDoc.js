function getHeader_DataDel() {
	  var selectStr = "$select=ID,QOSTextContent,IsPHI,CategoryValue,RptFrequency,DeliveryMethod,DeliverableNameValue,DeliverableNavString,ContactName/ID,ContactName/Title,ContactName/EMail&$expand=ContactName/ID,ContactName/Title,ContactName/EMail";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")" + "?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadHeaderDataDel,
			error: errorHandler
		  }
	  );		
}

function loadHeaderDataDel(data) {  
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  createDataDelHeaderBlock_noFile(results);
	  createDataDelContactBlock(results);
	  createDataDelDetailsBlock(results);
	  $("#q_listdeldesc").append($("<div>").addClass("q_dividerwide"));	
	  $("#q_listdeldesc").append($("<div>").html(results[0].QOSTextContent));
	  $("#q_listdeldesc").css("margin-bottom", "40px");	
	  var year = rptdt.slice(0,4); month = rptdt.slice(4,6); day = rptdt.slice(6,8);
	  var takeDate = new Date(year + "/" + month + "/" + day);
	  var currentDate = year + "/" + month + "/" + day;
	  takeDate.setDate(takeDate.getDate() - 1);
	  var rptDataDt = (takeDate.getMonth() + 1) + "/" + takeDate.getDate() + "/" + takeDate.getFullYear();
	  if (results[0].RptFrequency == "Quarterly") {	  
		  var selectStr = "$select=ID,Title,QOSTextContent,CategoryValue,FileLeafRef,FileDirRef,ReleaseDate,ReportDataDate";
		  var filterby = "$filter=DeliverableNavString%20eq%20%27" + results[0].DeliverableNavString + "%27%20and%20(ReportDataDate%20gt%20%27" + rptDataDt + "%27%20and%20ReportDataDate%20le%20%27" + currentDate + "%27)%20and%20MedicalCtrValue%20eq%20%27" + medctr + "%27";		  	
		  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?$top=1&" + selectStr + "&" + filterby;
		  var executor = new SP.RequestExecutor(catUrl);
		  executor.executeAsync(
			{
			  url: itemsUrl,
			  method: "GET",
			  headers: { "Accept": "application/json; odata=verbose" },
			  success: createDataDelHeaderBlock,
			  error: errorHandler
			});
	  } else { $("#q_header-datadel_norpt").removeClass("q_hide");}
}

function createDataDelHeaderBlock_noFile(data) {  
	  var subheadNode1 = $("<div/>").addClass("q_subhead");
	  if (data[0].DeliveryMethod == 'Email') {
		  var spriteNode = $("<div/>").addClass("q_sprite_email");
	  } else {
		  var spriteNode = $("<div/>").addClass("q_sprite");
	  }
	  var h1Node = $("<h1/>").text(results[0].DeliverableNameValue);
	  spriteNode.append(h1Node);
	  subheadNode1.append(spriteNode);
	  
	  var subheadNode2 = $("<div/>").addClass("q_related");
	  var pNode = $("<h3/>").addClass("q_nopadding");
	  var aNode1 = $("<a/>").attr({"title" : " Click to view Measure Measure Definitions", "href" : pagesUrl + "topicmeasuregrps.aspx?carea=" + contentarea + "&delnav=" + data[0].DeliverableNavString}).text("Measure Definitions");
	  pNode.append(aNode1);
	  subheadNode2.append(pNode);
	  var divNode = $("<div>").attr("id", "q_header-datadel_norpt").addClass("q_hide");
	  divNode.append(subheadNode1);
	  divNode.append(subheadNode2);
	  $("#q_header-datadel").append(divNode);	
}

function createDataDelHeaderBlock(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if ( results.length > 0) {
		var linkhref = catroot + results[0].FileDirRef + "/" + results[0].FileLeafRef;
		var get_date = new Date(results[0].ReportDataDate);
		var rptDataDt = (get_date.getMonth() + 1) + "/" + get_date.getDate() + "/" + get_date.getFullYear();
	
		var subheadNode1 = $("<div/>").addClass("q_subhead");
		var spriteNode = $("<div/>").addClass("q_sprite");
		var h1Node = $("<h1/>");
		var aNode1 = $("<a/>", {"title": "Click to view the most recent release", "href": linkhref, "target": "_blank"});
		aNode1.text(results[0].DeliverableNameValue);
		h1Node.append(aNode1);
		spriteNode.append(h1Node);
		subheadNode1.append(spriteNode);
		
		var subheadNode2 = $("<div/>").addClass("q_subhead q_nosprite-padleft q_notopmargin");
		var h3Node1 = $("<h3/>").addClass("q_nopaddingtop");
		var aNode2 = $("<a/>").attr({"title": "Click to view the most recent release", "href": linkhref, "target": "_blank"});
		aNode1.text(results[0].Title); 
		var emNode = $("<em/>").addClass("q_labeltext").text(" - Data as of: " + rptDataDt);
		aNode1.append(emNode);
		h3Node1.append(aNode1);
		subheadNode2.append(h3Node1);
		
		var subheadNode3 = $("<div/>").addClass("q_related");
		var pNode = $("<h3/>").addClass("q_nopadding");
		var aNode3 = $("<a/>").attr({"title" : " Click to view Measure Measure Definitions", "href" : pagesUrl + "topicmeasuregrps.aspx?carea=" + contentarea + "&delnav=" + results[0].DeliverableNavString}).text("Measure Definitions");
		var aNode4 = $("<a/>").attr({"title" : " Click to view Measure Resources", "href" : pagesUrl + "topicresources.aspx?carea=" + contentarea + "&curId=" + curId}).text("Resources");
		pNode.append(aNode3);
		pNode.append("&nbsp;&nbsp;|&nbsp;&nbsp;");
		pNode.append(aNode4);
		subheadNode3.append(pNode);
		alert("appending");
		$("#q_header-datadel").append(subheadNode1);
		$("#q_header-datadel").append(subheadNode2);
		$("#q_header-datadel").append(subheadNode3);
	} else { $("#q_header-datadel_norpt").removeClass("q_hide"); }
}

function createDataDelContactBlock(results) {
	var h4Node = $("<h4/>").text("Supported by");
	var ulNode = $("<ul/>");
	var liNode1 = $("<li/>");
	var aNode = $("<a/>").attr({"target": "_blank", 
		"title" : " Click to send email to "+results[0].ContactName, 
		"href" : "mailto:"+ results[0].ContactName.EMail}).text(results[0].ContactName.Title);
	var pNode = $("<p/>").text("Analytic Consulting");
	liNode1.append(aNode);
	liNode1.append(pNode);
	ulNode.append(liNode1);
	$("#q_delcontacts").addClass("q_contacts_nobackground").append(ulNode);
}

function createDataDelDetailsBlock(results) {
	var ulNode = $("<ul/>").addClass("q_detailsbox");
	var liNode1 = $("<li/>").append( $("<span/>").text(results[0].CategoryValue));
	var liNode2 = $("<li/>").append( $("<span/>").text("Produced " + results[0].RptFrequency));
	var liNode3 = $("<li/>").append( $("<span/>").text("Delivered via " + results[0].DeliveryMethod));
	ulNode.append(liNode1, liNode2, liNode3);
	if(results[0].IsPHI == true){
		var containsPHI = "Contains PHI and/or confidential information";
		var liNode4 = $("<li/>").append( $("<span/>").text(containsPHI));
		ulNode.append(liNode4);
	}
	ulNode.append(liNode1, liNode2, liNode3, liNode4);
    $("#q_deldetails").append(ulNode);
	$("#q_deldetails").addClass("q_textbox");
	
}

