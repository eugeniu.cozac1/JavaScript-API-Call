	 jQuery(document).ready(function ($) {
		// resources are in URLs in the form:
		// web_url/_layouts/15/resource
		var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/"; 
	
		// Load the js files and continue to the successHandler
		$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
	 });
	 
	 function execCrossDomainRequest() {
		$("#q_grid-33-right").addClass("q_borderleft-wide");
		changeBranding();
		getPageText_Banner();
		loadTypeBannerBar();
		SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog');
		SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
					setLinksFiles("Business Deliverable File", "General", "#q_sidebar-bizdocs", "mini");
					setLinksFiles("Data Deliverable File", "Report", "#q_sidebar-datadocs", "mini");
					setLinksContent("Page Text", "Textual", "content", "#q_sidebar-pagetext", "mini");
					setLinksContent("Support Contacts", "QOS", "Support%20Contacts", "#q_sidebar-contacts", "mini");
					setLinksContent("WebLinks", "Web", "links", "#q_sidebar-weblinks", "mini");
					setLinksLists("Data Deliverable Profiles", "Deliverables", "deliverables", "#q_sidebar-deltype", "mini");
					setLinksLists("MeasureProfiles - All", "Measure%20Details", "measuredetails", "#q_sidebar-measures", "mini");
					setLinksLists("Grouped Measures", "Measure%20Groupings", "mgroupings", "#q_sidebar-mgroupings", "mini");
					setLinksPubActions("Publish to Staging", "Draft", "#q_sidebar-staging", pushicon_mini);
					setLinksPubActions("Publish to Production", "Pending", "#q_sidebar-prod", pushicon_mini);
					setLinksPubActions("Unpublish Items", "Unpublish", "#q_sidebar-unpublish", unpubicon_mini);
					setLinksPubActions("To Archive", "Archive", "#q_sidebar-archive", archiveicon_mini);
					setLinksPubActions("Restore from Archive", "Recover", "#q_sidebar-recover", restoreicon_mini);
					setLinksPubActions("Permanently Delete", "Delete", "#q_sidebar-delete", deleteicon_mini);
					setLinksAssocActions("By Data Deliverable Profile", "Deliverables", "#q_sidebar-delassoc", editicon_mini);
					setLinksAssocActions("By Measure Profile", "Measure%20Details", "#q_sidebar-massoc", editicon_mini);
					setLinksAssocActions("By Grouped Measures", "Measure%20Groupings", "#q_sidebar-mgassoc", editicon_mini);
				},"sp.ui.dialog.js");

	 }
	 
