function getNavWebLinks() {
	var orderbyStr = "$orderby=WebURL";
	var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	  
	var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Web%20Links')/items?$top=6&" + orderbyStr + "&" + filterby;
	var executor = new SP.RequestExecutor(catUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadNavWebLinks,
		  error: errorHandler
		}
	);		
}

function loadNavWebLinks(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if(results.length > 0){
		var titleNode = $("<h4/>").text("Web Links");
		var ulNode = $("<ul>").attr("id", "q_boxlist_web");
		$("#q_navweblinks").addClass("q_boxlist");
		$("#q_navweblinks").append(titleNode);
		$("#q_navweblinks").append(ulNode);
		for (var i = 0; i < results.length; i++ ){
			setNavLinkWebRow (results[i].WebURL.Url, results[i].WebURL.Description, 'Click to visit this link');	
		}  
	}
	if(results.length > 6){
	   var link_view = pagesUrl;
	   setNavLinkWebRow (link_view, 'View more...', 'Click to view more.')
	}
}

function setNavLinkWebRow (linkhref, displaytitle, hovertext) {
	var liNode = $("<li>");
	var aNode = $("<a>");
	aNode.attr("title", hovertext).text(displaytitle);
	aNode.attr("href", linkhref);
	aNode.attr("target", "_blank");
	liNode.append(aNode);
	$("#q_boxlist_web").append(liNode);
}
