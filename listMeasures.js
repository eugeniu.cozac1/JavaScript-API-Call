function getMeasureGrouping() {
	  var selectStr = "$select=ID,MeasureGroupValue,MeasureDetailIDListValue";
	  if(delnav == false){
	  	var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	  
	  } else { 
	  	var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27";
	  }
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Measure%20Groupings')/items?" + selectStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadMeasureGroupings,
			error: errorHandler
		  }
	  );	
}

function loadMeasureGroupings (data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	var mdtlarray = [];
	for (var i = 0; i < results.length; i++) {
		var idArray = results[i].MeasureDetailIDListValue.split(";");
		m = mdtlarray.length;
		if (idArray.length != 0) {
			for (var j = 0; j < idArray.length; j++) {
			  mdtlarray[m] = idArray[j];
			  m++;
			}
		}
	}
	alert("length of mdtlarray: " + mdtlarray.length);
	if(mdtlarray.length > 0) {
		var unique = mdtlarray.filter(function(itm,i,a){
			return i==a.indexOf(itm);
		});
		mdtlarray = unique;
		loadListMeasureProfile(mdtlarray);
	} else {
		writeNoMeasuresFoundMsg();
	}
}

function loadListMeasureProfile(mdtlids) {
	for (var i = 0; i < mdtlids.length; i++) {
	  var selectStr = "$select=ID,MeasureNameValue,Definition,MeasureNavString";
	  var filterby = "$filter=MeasureNavString%20eq%20%27mg" + mdtlids[i] + "%27";	  
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Measure%20Details')/items?" + selectStr + "&" + filterby;
	  $.ajax({
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  cache: false,
		  async: false,
		  success: function(data) { 
				var list = (data.d.results == null) ? new Array(data.d) : data.d.results;
				setContentRow_Measure("#q_listmeasures", list[0].ID, list[0].MeasureNameValue, list[0].Definition, list[0].MeasureNavString );},
		  error: function (data) {
			  console.log("Error");
		  }
	  });	
	}
	$("#q_listmeasures .q_sortby").sort(function(a, b){
		return ($(b).attr("data-name")) < ($(a).attr("data-name")) ? 1 : -1;
	}).appendTo('#q_listmeasures');
	
}

function setContentRow_Measure(idval, itemId, nameStr, defcontent, mg) {
	var linkhref = pagesUrl + "measureversions.aspx?carea=" + contentarea + "&mdtlnav=" + mg; 
	var cellNode = $("<div>").addClass("grid-100 q_rt_rowcell-2-body q_sortby").attr({"data-name" : nameStr});
	var spriteNode = $("<div>").addClass("q_spriteindent-med");
	var node1 = $("<div>").addClass("q_rt_rowcell-2-title").append($("<a>").addClass("q_weblinksprite_med").attr({"href" : linkhref, "title" : "Click to view details and specific versions", "target" : "_self"}).text(nameStr));
	var node2 = $("<div>").addClass("q_rt_rowcell-2-body q_spriteindent-lefttext");
	var spanNode1 = $("<span>").addClass("q_clipline").html(defcontent);
	var spanNode2 = $("<span>").addClass("q_readmore");	
	var aNode2 = $("<a>").addClass("q_viewmore").attr({"href": linkhref, "title" : "Click to view details"}).text("More");
	var arrowNode = $("<span>").addClass("q_arrowsprite");
	aNode2.append(arrowNode);
	spanNode2.append(aNode2);
	node2.append(spanNode1, spanNode2);
	spriteNode.append(node1);
	spriteNode.append(node2);
	cellNode.append(spriteNode);

	$(idval).append(cellNode);
}
