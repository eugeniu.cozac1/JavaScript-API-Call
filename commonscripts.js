// Frequently used variables and functions 
var siteType = document.URL.split("/")[4];

var contentarea = getQueryStringParameter("carea");
var ctype = getQueryStringParameter("ctype");
var delnav = getQueryStringParameter("delnav");
var mdtlnav = getQueryStringParameter("mdtlnav");
var mdtlId = getQueryStringParameter("mdtlid");
var curId = getQueryStringParameter("curid");
var status = getQueryStringParameter("status");
var addtitle = decodeURIComponent(getQueryStringParameter("addtitle"));

var ct_report = "0x010100160712A496ABEB49A31DD60E199C904401";
var ct_bizdoc = "0x010100160712A496ABEB49A31DD60E199C904402";
var contentGUID;
var updateType;	//used to control add or edit post activity
var default_delId = 106;
var default_mgId = 106;
var default_mgGroupId = 16;
var rootUrl = "https://sites.sp.kp.org/pub";
alert(rootUrl + " " + siteType);
if (siteType == "qosstgpub" || siteType == "qosstgcat") {
  pagesUrl = rootUrl + "/qostgpub/Pages/";
  capagesUrl = rootUrl + "/qosstgpub/ca/Pages/";
  caformsUrl = rootUrl + "/qosstgcat/SitePages/";
  carootUrl = rootUrl + "/qosstgcat/";
  casiteUrl = rootUrl + "/qosstgcat/" + contentarea;
} else {	
  pagesUrl = rootUrl + "/qosdevcat/pub/Pages/";
  capagesUrl = rootUrl + "/qosdevcat/ca/Pages/";
  caformsUrl = rootUrl + "/qosdevcat/ca/SitePages/";
  carootUrl = rootUrl + "/qosdevcat/";
  casiteUrl = rootUrl + "/qosdevcat/" + contentarea;
  default_delId = 2;
  default_mgId =1;
  default_mgGroupId = 14;
  ct_report = "0x01010060855D976BDB594ABEDF9CA7729BBD3E02";
  ct_bizdoc = "0x01010060855D976BDB594ABEDF9CA7729BBD3E01";
}
alert(carootUrl);
var addicon = rootUrl + "/qosstgpub/SiteCollectionImages/addItem_24x24.png";
var editicon = rootUrl + "/qosstgpub/SiteCollectionImages/edit-notes_24x24.png";
var pushicon = rootUrl + "/qosstgpub/SiteCollectionImages/pushicon_24x24.png";
var unpubicon = rootUrl + "/qosstgpub/SiteCollectionImages/unpublish_icon_24x23.png";
var archiveicon = rootUrl + "/qosstgpub/SiteCollectionImages/uploadfile_32x24.png";
var restoreicon = rootUrl + "/qosstgpub/SiteCollectionImages/restore_24x24.png";
var deleteicon = rootUrl + "/qosstgpub/SiteCollectionImages/delete_24x24.png";
var addicon_mini = rootUrl + "/qosstgpub/SiteCollectionImages/addItem_18x18.png";
var editicon_mini = rootUrl + "/qosstgpub/SiteCollectionImages/edit-notes_18x18.png";
var pushicon_mini = rootUrl + "/qosstgpub/SiteCollectionImages/pushicon_18x18.png";
var unpubicon_mini = rootUrl + "/qosstgpub/SiteCollectionImages/unpublish_icon_18x18.png";
var archiveicon_mini = rootUrl + "/qosstgpub/SiteCollectionImages/uploadfile_24x18.png";
var restoreicon_mini = rootUrl + "/qosstgpub/SiteCollectionImages/restore_18x18.png";
var deleteicon_mini = rootUrl + "/qosstgpub/SiteCollectionImages/delete_18x18.png";
var mdtlids = [];
var mgrpids = [];
var categoryflags = [];
var profiledata = [];
var loadNameDone = false;
var loadProfileDataDone = false;
var loadDocCategoryDone = false;
var loadMeasureGroupsDone = false;
var loadDeliverablesOptionsDone = false;
var loadMeasureGroupingsDone = false;
var loadItemCategoryListDone = false;
var loadSubRegionsDone = false;
var loadUserHasPermission = false;
var userHasPermission = false;
var loadTemplateOptionsDone = false;
var flagRibbonShowHide = "hide";
var loadSupportUsersCount = 0;
var global_user = "";
var parse_initiatives = [];

function changeBranding() {
	$('#suiteBarLeft').each(function () {
    	this.style.setProperty( 'background-color', '#65adc3', 'important' );
	});
	$(".grid-navbar-dec").each(function () {
    	this.style.setProperty( 'background-color', '#7f7b75', 'important' );
	});
	if (siteType.indexOf("stg") > 0) {
		$('#suiteBarLeft .ms-core-brandingText').addClass("ms-core-brandingText-ca");
	} else {
		$('#suiteBarLeft .ms-core-brandingText').addClass("ms-core-brandingText-dev");
	}
}

function getPageText_Banner () {
	var selectStr = "$select=ID,Title,SiteLogoImageValue,Sponsors";
	var filterby = "$filter=PublishingApproval%20ne%20%27Archive%27%20and%20CategoryValue%20eq%20%27Page%20Banner%27";	  
	var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?" + selectStr + "&" + filterby;
	var executor = new SP.RequestExecutor(casiteUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadPageText_Banner,
		  error: errorHandler
		}
	);	
}
	 
function getDeliverablesOptionList() {
	var selectStr = "$select=ID,DeliverableNameValue";
	var orderbyStr = "$orderby=DeliverableNameValue";
	var filterby = "$filter=PublishingApproval%20ne%20%27Archive%27%20and%20AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Deliverables')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadDeliverablesOptions,
		  error: errorHandler
		}
	);	
}

function getTemplateOptionList() {
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Page%20Templates')/items";
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadTemplateOptions,
		  error: errorHandler
		}
	);	
}

function getMeasureGroupsOptionList() {
	var selectStr = "$select=ID,Title";
	var orderbyStr = "$orderby=Title";
	var filterby = "$filter=Title%20ne%20%27default%27";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groups')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadMeasureGroups,
		  error: errorHandler
		}
	);
}

function getMeasureGroupingsOptionList() {
	var selectStr = "$select=ID,MeasureGroupValue";
	var orderbyStr = "$orderby=MeasureGroupValue";
	var filterby = "$filter=PublishingApproval%20ne%20%27Archive%27%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Groupings')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadMeasureGroupings,
		  error: errorHandler
		}
	);
}

function getSubregionsOptionList() {
	var selectStr = "$select=ID,Title";
	var orderbyStr = "$orderby=Title";
	var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Subregions')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadSubregions,
		  error: errorHandler
		}
	);
}

function getItemCategoriesOptionList() {
	var selectStr = "$select=ID,Title,AssocToDeliverable,AssocToMeasureGrp";
	var orderbyStr = "$orderby=Title";
	var filterbyStr = "$filter=Title%20ne%20%27Page%20Banner%27";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Item%20Categories')/items?" + selectStr + "&" + orderbyStr + "&" + filterbyStr;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadItemCategoryList,
		  error: errorHandler
		}
	);
}

function getDocCategoriesOptionList(docflag) {
	var typeflag = "ne";
	if (docflag == true) {
		typeflag = "eq";
	};
	var selectStr = "$select=ID,Title";
	var orderbyStr = "$orderby=Title";
	var filterbyStr = "$filter=ReportsOnly%20" + typeflag + "%20true";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Document%20Categories')/items?" + selectStr + "&" + orderbyStr + "&" + filterbyStr;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadDocCategoryList,
		  error: errorHandler
		}
	);
}

function getNamesOptionsList() {
	var selectStr = "$select=Title,LoginName";
	var orderbyStr = "$orderby=Title";
	var filterby = "";	  
	var itemsUrl = "https://sites.sp.kp.org/teams/qos/_api/web/SiteGroups/GetById(48)/Users" + "?" + selectStr + "&" + orderbyStr;
	var executor = new SP.RequestExecutor("https://sites.sp.kp.org/teams/qos/");
	executor.executeAsync(
		{
		 url: itemsUrl,
		 method: "GET",
		 headers: { "Accept": "application/json; odata=verbose" },
		 success: loadNameOptions,
		 error: errorHandler
		}
	);
}

function getMeasuresOptionList() {
	var selectStr = "$select=ID,MeasureNameValue";
	var orderbyStr = "$orderby=MeasureNameValue";
	var filterby = "$filter=PublishingApproval%20ne%20%27Archive%27";	  
	var itemsUrl = carootUrl + "_api/web/lists/getbytitle('Measure%20Details')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadMeasureDetails,
		  error: errorHandler
		}
	);
}

function loadItemCategoryList(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$("#q_listCategories").append($("<option>").attr({"value": "0"}));
	for (var i = 0; i < results.length; i++ ){
	  var text = results[i].Title;
	  var var_id = results[i].ID;
	  var opt = $("<option>").text(text);
	  opt.val(var_id);
	  $("#q_listCategories").append(opt);
	  categoryflags.push([results[i].ID, results[i].AssocToDeliverable, results[i].AssocToMeasureGrp]);
	}  
	loadItemCategoryListDone = true;
} 

function loadDocCategoryList(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$(results).each(function(){
	  var text = $(this)[0].Title ;
	  var var_id = $(this)[0].ID;
	  var opt = $("<option>").text(text);
	  opt.val(var_id);
	  $("#q_listCategories").append(opt);
	});
	loadDocCategoryDone = true;
} 

function loadDeliverablesOptions(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$("#q_listDeliverables").append($("<option>").attr({"value": "0"}).text(" "));
	$(results).each(function(){
	  var text = $(this)[0].DeliverableNameValue ;
	  var var_id = $(this)[0].ID;
	  var opt = $("<option>").text(text);
	  opt.val(var_id);
	  $("#q_listDeliverables").append(opt);
	});
	loadDeliverablesOptionsDone = true;
} 

function loadTemplateOptions(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$(results).each(function(){
	  var text = $(this)[0].Title ;
	  var var_id = $(this)[0].Id;
	  var page_name = $(this)[0].Page_x0020_Name;
	  var opt = $("<option>").text(text).attr({"page_name": page_name});
	  opt.val(var_id);
	  $("#q_pageTemplate").append(opt);
	});
	loadTemplateOptionsDone = true;
} 

function loadMeasureGroupings(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$("#q_listMGroupings").append($("<option>").attr({"value": "0"}).text(" "));
	$(results).each(function(){
	  var text = $(this)[0].MeasureGroupValue ;
	  if (text.indexOf("_del") < 0) {
		  var var_id = $(this)[0].ID;
		  var opt = $("<option>").text(text);
		  opt.val(var_id);
		  $("#q_listMGroupings").append(opt);
	  }
	});
	loadMeasureGroupingsDone = true;
}

function loadMeasureGroups(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$(results).each(function(){
	  var text = $(this)[0].Title ;
	  var var_id = $(this)[0].ID;
	  var opt = $("<option>").text(text);
	  opt.val(var_id);
	  $("#q_listMGroups").append(opt);
	});
	loadMeasureGroupsDone = true;
}

function loadSubregions(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$(results).each(function(){
	  var text = $(this)[0].Title ;
	  var var_id = $(this)[0].ID;
	  var opt = $("<option>").text(text);
	  opt.val(var_id);
	  $("#q_listSubregions").append(opt);
	});
	loadSubRegionsDone = true;
}

function loadMeasureDetails(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$("#q_listMeasures").append($("<option>").attr({"value": "0"}).text(" "));
	$(results).each(function(){
	  var text = $(this)[0].MeasureNameValue ;
	  var var_id = $(this)[0].ID;
	  var opt = $("<option>").text(text);
	  opt.val(var_id);
	  $("#q_listMeasures").append(opt);
	});
	loadMeasureDetailsDone = true;
}

function loadServiceAreaOptions(){ 
	$("#q_listServiceArea").append($("<option>").attr({"value": ""}).prop('selected', true));
	$.each(serviceAreas, function(index, value ) {
	 var opt = $("<option>").text(value[1]);
	 opt.val(value[0]);
	 $("#q_listServiceArea").append(opt);
	});
}

function loadMedicalCenterOptions(){ 
	$("#q_medicalcenter").append($("<option>").attr({"value": ""}).prop('selected', true));
	$.each(medcenters, function(index, value ) {
	 var opt = $("<option>").text(value[1]);
	 opt.val(value[0]);
	 $("#q_medicalcenter").append(opt);
	});
}

function loadNameOptions(data){ 
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
	$("#q_listNames").append($("<option>").attr({"value": ""}).prop('selected', true));
	alert("before");
	$(results).each(function(){
	  var text = $(this)[0].Title ;
	  var var_id = $(this)[0].LoginName;
	  var opt = $("<option>").text(text);
	  opt.val(var_id);
	  $("#q_listNames").append(opt);
	});
	alert("after");
	loadNameDone = true;
}



function setLinksFiles(linktitle, contenttype, idname, iconsize, pagename) {
  	var editUrl = capagesUrl + "casplash.aspx?carea=" + contentarea + "&ctype=" + contenttype;
  	var windowTitle = "Upload " + linktitle; 
	var newUrl = carootUrl + "/SitePages/" + pagename +".aspx?carea=" + contentarea;
	if (contenttype == "General") {
		newUrl = newUrl + "&ctype=General";
	} else {
		newUrl = newUrl + "&ctype=Report";
	}
	var onclickstr = "javascript: showModal('" + newUrl + "', '" + windowTitle + "'); return false;";  
	createRow(linktitle, editUrl, newUrl, onclickstr, idname, iconsize);
}

function setLinksContent(linktitle, contenttype, listname, idname, iconsize, pagename) {
  	var editUrl = capagesUrl + "casplash.aspx?carea=" + contentarea + "&ctype=" + contenttype;
	var windowTitle = "Add " + linktitle;
	var newUrl = carootUrl + "/SitePages/" + pagename +".aspx?carea=" + contentarea;
	var onclickstr = "javascript: showModal('" + newUrl + "', '" + windowTitle + "'); return false;";
  	createRow(linktitle, editUrl, newUrl, onclickstr, idname, iconsize);
}
function setLinksLists(linktitle, contenttype, listname, idname, iconsize, pagename) {
  	var editUrl = capagesUrl + "casplash.aspx?carea=" + contentarea + "&ctype=" + contenttype;
	var windowTitle = "Add " + linktitle;
	var newUrl = carootUrl + "/SitePages/" + pagename +".aspx?carea=" + contentarea;
	var onclickstr = "javascript: showModal('" + newUrl + "', '" + windowTitle + "'); return false;";
  	createRow(linktitle, editUrl, newUrl, onclickstr, idname, iconsize); 
}

function createRow(linktitle, editUrl, newUrl, onclickstr, idname, iconsize) {
  var newaddicon = addicon;
  var newediticon = editicon;
  if (iconsize == "mini") {
	  newaddicon = addicon_mini;
	  newediticon = editicon_mini;
  }
  var cell1_node = $("<td>").addClass("q_iconcell");
  var a1_node = $("<a>").attr({"href": "javascript:;", "onClick": onclickstr});  
  var img1_node = $("<img>").attr({"src": newaddicon});
  img1_node.appendTo(a1_node);
  a1_node.appendTo(cell1_node);
  $(idname).append(cell1_node);
  
  var cell2_node = $("<td>").addClass("q_iconcell");
  var a2_node = $("<a>").attr({"href": editUrl}); 
  var img2_node = $("<img>").attr({"src": newediticon});
  img2_node.appendTo(a2_node);
  a2_node.appendTo(cell2_node);
  $(idname).append(cell2_node);
  
  var cell3_node = $("<td>").addClass("q_celltitle");
  var a3_node = $("<a>").attr({"href": editUrl}).text(linktitle); 
  a3_node.appendTo(cell3_node);
  $(idname).append(cell3_node);
}


function setLinksPubActions(linktitle, pubstatus, idname, newicon) {
  var newUrl = capagesUrl + "pubstatus.aspx?carea=" + contentarea + "&status=" + pubstatus; 

  var cell1_node = $("<td>").addClass("q_iconcell");
  var a1_node = $("<a>").attr({"href": newUrl}); 
  var img1_node = $("<img>").attr({"src": newicon});
  img1_node.appendTo(a1_node);
  a1_node.appendTo(cell1_node);
  $(idname).append(cell1_node);
  
  var cell2_node = $("<td>").addClass("q_celltitle");
  var a2_node = $("<a>").attr({"href": newUrl}).text(linktitle); 
  a2_node.appendTo(cell2_node);
  $(idname).append(cell2_node);

}
function setLinksAssocActions(linktitle, assoctype, idname, newicon) {
  var newUrl = capagesUrl + "ca_associations.aspx?carea=" + contentarea + "&ctype=" + assoctype; 

  var cell1_node = $("<td>").addClass("q_iconcell");
  var a1_node = $("<a>").attr({"href": newUrl}); 
  var img1_node = $("<img>").attr({"src": newicon});

  img1_node.appendTo(a1_node);
  a1_node.appendTo(cell1_node);
  $(idname).append(cell1_node);
  
  var cell2_node = $("<td>").addClass("q_celltitle");
  var a2_node = $("<a>").attr({"href": newUrl}).text(linktitle); 
  a2_node.appendTo(cell2_node);
  $(idname).append(cell2_node);
}

function loadPageText_Banner (data) {
	var jsonObject = JSON.parse(data.body);
	var results = jsonObject.d.results;
  	for (var i = 0; i < results.length; i++) {
		var div_bannerouter = $("<div>").addClass("q_bannerbackground");
		var div_container = $("<div>").addClass("q_bannertitlelogo");
		var div_bannertext =$("<div>").addClass("q_pagebannertext");
		var div_bannerlogo =$("<div>").addClass("q_pagebannerlogo hide-on-mobile");
		var div_bannersponsor = $("<div>").attr({"id": "q_sponsorbar"}).html(results[i].Sponsors);
		div_bannertext.append($("<h1>").text(results[i].Title));
		div_bannerlogo.append($("<img>").attr({"src": results[i].SiteLogoImageValue + "?RenditionID=2"}));
		div_container.append(div_bannertext);
		div_container.append(div_bannerlogo);
		div_container.append(div_bannersponsor);
		div_bannerouter.append(div_container);
		$("#q_commonpagebanner").append(div_bannerouter);
	}
}

function loadBreadcrumbsplash(){
	var fullUrl = capagesUrl + "casplash.aspx?carea=" + contentarea + "&ctype=" + ctype;
	var home_link = capagesUrl + "cahome.aspx?carea=" + contentarea;
	switch (ctype) {
		case "Textual":
			var ctypeText = "Page Text";
			break;
		case "QOS":
			var ctypeText = "Support Contacts";
			break;
		case "Web":
			var ctypeText = "Web Links";
			break;
		case "General":
			var ctypeText = "Business Deliverable Files";
			break;
		case "Report":
			var ctypeText = "Data Deliverables Files";
			break;
		case "Deliverables":
			var ctypeText = "Data Deliverable Profiles";
			break;
		case "Measure%20Details":
			var ctypeText = "Measure Profiles";
			break;
		case "Measure%20Groupings":
			var ctypeText = "Grouped Measures";
			break;
		default:
			var ctypeText = "";

	}
	$("#q_cabreadcrumblinkbar").addClass("q_nav q_nav_breadcrumb");
	$("#q_cabreadcrumblinkbar").prepend("<ul></ul>");
	$("<li><a href='" + home_link + "' target='_self' title='Home'>Home</a></li>").appendTo("#q_cabreadcrumblinkbar ul");
	$("<li><a href='" + fullUrl + "' target='_self' title='"+ ctype +"'>"+ ctypeText +"</a></li>").appendTo("#q_cabreadcrumblinkbar ul");	
	$("#q_backhometext").prepend($("<a/>").attr("href", home_link).text("Back to Home"));
}

function loadListTypeBanner (titletext, linktitle, newUrl) {
	var onclickstr = "javascript: showModal('" + newUrl + "', '" + linktitle + "'); return false;";
	var h1_node = $("<h1>").text(titletext);
	var h3_node = $("<h3>");
	var a_node = $("<a>").attr({"href": "javascript:;", "onClick": onclickstr}).text("  " + linktitle);
	var img_node = $("<img>").attr({"src": addicon});
	var a_img_node = $("<a>").attr({"href": "javascript:;", "onClick": onclickstr});
	a_img_node.append(img_node);
	h3_node.append(a_img_node);
	h3_node.append(a_node);
	$("#q_listheader").append(h1_node);
	$("#q_listheader").append(h3_node);
}

function loadTypeBannerBar () {		  
		var selectStr = "$select=ID,Title";
		var itemsUrl = casiteUrl + "/_api/web/lists/getbytitle('Content%20Area%20Profile')/items?" + selectStr;				
		var executor = new SP.RequestExecutor(casiteUrl);
		executor.executeAsync(
			  {
				url: itemsUrl,
				method: "GET",
				headers: { "Accept": "application/json; odata=verbose" },
				success: loadBannerBar,
				error: errorHandler
			  }
		 );
		 return true;
};

function loadBannerBar(data){
		var jsonObject = JSON.parse(data.body);
		var results = jsonObject.d.results;

		for (var i = 0; i < results.length; i++) {
			var returnUrl = capagesUrl + "cahome.aspx?carea=" + contentarea;
			var idvar = "#q_cabreadcrumblinkbar";
			var div_node = $("<div>").addClass("q_nav q_nav_breadcrumb");
			var ul_node = $("<ul>");
			var li_node = $("<li>");
			var a_node = $("<a>").attr({"href": returnUrl, "title": "Click to return to " + results[i].Title + " Home Page"}).text("Back to " + results[i].Title + " Home");
			var a2_node = $("<a>").attr({"href": returnUrl, "title": "Click to return to " + results[i].Title + " Home Page"}).text("Back to " + results[i].Title + " Home");
			a_node.appendTo(li_node);
			li_node.appendTo(ul_node);
			ul_node.appendTo(div_node);
			$(idvar).append(div_node);
			$("#q_backhometext").append(a2_node);
	 	};
		return true;
}

function loadProfileData() {
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Content Area Profile'); 
	this.oListItem = oList.getItemById(1);
	clientContext.load(oListItem);        
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onProfileQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onProfileQuerySucceeded(sender, args) {
	if (oListItem.get_item('AOWNavString') == null || oListItem.get_item('AOWNavString') == "undefined") {
		profiledata[0] = "";	
	} else {
		profiledata[0] = oListItem.get_item('AOWNavString');
	}
	if (oListItem.get_item('PgmNavString') == null || oListItem.get_item('PgmNavString') == "undefined") {
		profiledata[1] = "";	
	} else {
		profiledata[1] = oListItem.get_item('PgmNavString');
	}
	if (oListItem.get_item('LookupAOW') != null) {
		profiledata[2] = oListItem.get_item('LookupAOW').get_lookupValue();	
		profiledata[6] = oListItem.get_item('LookupAOW').get_lookupId();
	} else {
		profiledata[2] = "";
		profiledata[6] = 0;
	}
	if (oListItem.get_item('LookupInitiative') != null) {
		profiledata[3] = oListItem.get_item('LookupInitiative').get_lookupValue();	
		profiledata[7] = oListItem.get_item('LookupInitiative').get_lookupId();	
	} else {
		profiledata[3] = "";
		profiledata[7] = 0;
	}
	if (oListItem.get_item('LookupProgram') != null) {
		profiledata[4] = oListItem.get_item('LookupProgram').get_lookupValue();		
		profiledata[8] = oListItem.get_item('LookupProgram').get_lookupId();
	} else {
		profiledata[4] = "";
		profiledata[8] = 0;
	}
	if (oListItem.get_item('LookupSubProgram') != null) {
		profiledata[5] = oListItem.get_item('LookupSubProgram').get_lookupValue();	
		profiledata[9] = oListItem.get_item('LookupSubProgram').get_lookupId();	
	} else {
		profiledata[5] = "";
		profiledata[9] = 0;
	}
	if (oListItem.get_item('SecurityGrpID') != null) {
		profiledata[10]= oListItem.get_item('SecurityGrpID');	
	} else {
		profiledata[10] = 10001;
	}
	loadProfileDataDone = true;
}

function onQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function showModal(tUrl, tTitle) {
		 var options = {
             url: tUrl,
             title: tTitle,
			 width: 775,
			 height: 600,
             dialogReturnValueCallback: onPopUpCloseCallBackWithData
         };
         SP.UI.ModalDialog.showModalDialog(options);
 }
  
function onPopUpCloseCallBackWithData(result, returnValue) {
		 SP.UI.ModalDialog.RefreshPage(1);
		 //SP.UI.ModalDialog.commonModalDialogClose(1, popData);
 }
 
function cancelModalWindow() {
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(0, popData);
}

function getQueryStringParameter(paramToRetrieve){
  var params = document.URL.split("?")[1].split("&");
  var strParams = "";
  for (var i = 0; i < params.length; i = i + 1) {
	var singleParam = params[i].split("=");
	if (singleParam[0].toLowerCase() == paramToRetrieve.toLowerCase()) 
	  return singleParam[1]; 
  }
  return false;
}

function parseUrlFolders(url, segments){
  var firstSplit = url.split("//");
  var protocol = firstSplit[0];
  var folders = firstSplit[1].split("/");
  var retValue = '';
  for (i = 0; i < segments; i++) {
	  if (i < folders.length - 1) {
		  retValue += folders[i] + "/";
	  }
  }
  return protocol + "//" + retValue;
}

function writeNoRecordsMessage() {
		var content = "<h4>No results have been found for this content type.</h4>" +
			"<p>Content may not exist or you may not belong to the security group with rights to view or access this content.</p>" +
			"<p>If you believe you should be eligible to see content in this area, please <a href='mailto:qos.web@kp.org'>email QOS Web</a> for help.</p>";
		$('#q_resultslist').addClass("q_add-margin-top").html(content);
}

function errorHandler(data, errorCode, errorMessage) {
  $("#q_errormessage").text("Could not complete cross-domain call: " + errorMessage);
}

function getWebUserData() {
	var clientContext = new SP.ClientContext.get_current(carootUrl);
    this.website = clientContext.get_web();
    this.currentUser = website.get_currentUser();
    clientContext.load(currentUser);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onSuccessWebUserData), 
		Function.createDelegate(this, this.onQueryFailed));
}
function onSuccessWebUserData() {
	global_user = currentUser.get_loginName();
	if (siteType.indexOf("stg") > 0) {
		var ownersgrp = 4;
		var supportgrp = 185;
	} else {
		var ownersgrp = 4;
		var supportgrp = 6;
	}
	loadSupportUserCount = 0;	
	flagRibbonShowHide = "hide";
	checkUserForSupportRole(ownersgrp);
	checkUserForSupportRole(supportgrp); 
	var intervalUserCheck = setInterval(testUsers, 100);
	function testUsers(){
		if (loadSupportUserCount = 2) {
			if (flagRibbonShowHide == "hide") { hideFormatTextTabSections(); hidePageTabSections(); }
			clearInterval(intervalUserCheck);
		}
	}
}

function checkUserForSupportRole(groupId) {
	var selectStr = "$select=LoginName";
	var itemsUrl = carootUrl + "_api/web/SiteGroups/getbyid(" + groupId + ")/users" + "?" + selectStr;
	var executor = new SP.RequestExecutor(carootUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: validateUserSupportRole,
		  error: errorHandler
		}
	);
	loadSupportUsersCount = loadSupportUsersCount + 1;
}

function validateUserSupportRole (data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	$(results).each(function(){
		 if($(this)[0].LoginName == global_user){ flagRibbonShowHide = "show";}
	});
}

function hideFormatTextTabSections() {
	var intervalStr = setInterval(hideGroups, 100);
	function hideGroups () {
		if ($('li[id="Ribbon.EditingTools.CPEditTab.Clipboard"]').length > 0 || $('li[id="Ribbon.EditingTools.CPInsert.Tables"]').length > 0) {
			$('li[id="Ribbon.EditingTools.CPEditTab.EditAndCheckout"]').css("display", "none");
			$('li[id="Ribbon.EditingTools.CPEditTab.Font"]').css("display", "none");		
			$('li[id="Ribbon.EditingTools.CPEditTab.SpellCheck"]').css("display", "none");		
			$('li[id="Ribbon.EditingTools.CPEditTab.Layout"]').css("display", "none");		
			$('li[id="Ribbon.EditingTools.CPEditTab.Markup"]').css("display", "none");
			$('li[id="Ribbon.EditingTools.CPInsert.Media"]').css("display", "none");
			$('li[id="Ribbon.EditingTools.CPInsert.Links"]').css("display", "none");	
			$('li[id="Ribbon.EditingTools.CPInsert.Content"]').css("display", "none");	
			$('li[id="Ribbon.EditingTools.CPInsert.WebParts"]').css("display", "none");	
			$('li[id="Ribbon.EditingTools.CPInsert.Embed"]').css("display", "none");
			clearInterval(intervalStr);
		}
	}
}

function hidePageTabSections() {
	var intervalStr = setInterval(hideGroups, 100);
	function hideGroups () {
		if ($('li[id="Ribbon.WebPartPage.TagsAndNotes"]').length > 0) {
			$('li[id="Ribbon.WebPartPage.Edit"]').css("display", "none");
			$('li[id="Ribbon.WebPartPage.Manage"]').css("display", "none");
			$('li[id="Ribbon.WebPartPage.Approval"]').css("display", "none");
			$('li[id="Ribbon.WebPartPage.Workflow"]').css("display", "none");
			$('li[id="Ribbon.WebPartPage.Actions"]').css("display", "none");
			$('li[id="Ribbon.WebPartPage.TagsAndNotes"]').css("display", "none");
			$('a[id="Ribbon.WebPartPage.Share.ViewAnalyticsReport-Large"]').css("display", "none");	
			clearInterval(intervalStr);
		}
	}
}

function setRibbonGroupVisibility_FT() {
	var intervalRibbonFTLoad = setInterval(testFormatTab, 100);
	function testFormatTab(){
		if ($('li[id="Ribbon.EditingTools.CPEditTab.Clipboard"]').length > 0) {
			$("li[id='Ribbon.EditingTools.CPEditTab-title'] a")[0].click(function() {getWebUserData();});
			$("li[id='Ribbon.EditingTools.CPInsert-title'] a")[0].click(function() {getWebUserData();});
			getWebUserData();
			clearInterval(intervalRibbonFTLoad);
		}
	}	
}

function setRibbonGroupVisibility_PT() {
	var intervalRibbonPTLoad = setInterval(testPageTab, 100);
	function testPageTab(){
		if ($('li[id="Ribbon.WebPartPage-title"] a').length > 0) {
			$("li[id='Ribbon.WebPartPage-title'] a")[0].click(function() {getWebUserData();});
			getWebUserData();
			clearInterval(intervalRibbonPTLoad);
		}
	}
}

function overrideSharepointStyles(){
	$("#s4-ribbonrow").addClass("q_fixedheight35").css( "position", "relative" );;
	$("#s4-workspace").css( "overflow-y", "auto" );
	$("#globalNavBox").css({ "z-index" : "500","position" : "absolute" } );
} 

function getAuthorizeUserData() {
	var context = new SP.ClientContext.get_current(carootUrl);
    this.website = context.get_web();
    this.currentUser = website.get_currentUser();
    context.load(currentUser);
    context.executeQueryAsync(Function.createDelegate(this, this.onSuccessMethoddetectUser), Function.createDelegate(this, this.onFailureMethod));
}

function onSuccessMethoddetectUser(){
	var global_user = currentUser.get_loginName();
	var user_found = false;
	var secgrp = contentarea + "%20Approvers";
	var user_found = checkUserPermission(secgrp, global_user);
	if (user_found == false){ secgrp = contentarea + "%20Approvers"; user_found = checkUserPermission(secgrp, global_user);}
	if (user_found == false){ secgrp = "QOS%20Owners"; user_found = checkUserPermission(secgrp, global_user);}
	if (user_found == false){ secgrp = "QOS%20Support%20Team"; user_found = checkUserPermission(secgrp, global_user);}
	userHasPermission = user_found;
	if (user_found == false) {
		var content = "<h4>You do not have access right necessary to perform this action.</h4>" +
			"<p>Content may not exist or you may not belong to the security group with rights to view or access this content.</p>" +
			"<p>If you believe you should be eligible to see content in this area, please <a href='mailto:qos.web@kp.org'>email QOS Web</a> for help.</p>";
		$('#q_noaccessrights').addClass("q_add-margin-top").removeClass("q_hide").html(content);
	}
	loadUserHasPermission = true;
}

function checkUserPermission(securityGroupName, global_user){
	var selectStr = "$select=Title,LoginName";
	var itemsUrl = carootUrl + "_api/web/SiteGroups/getByName('" + securityGroupName + "')/users" + "?" + selectStr;
	var matchfound = false;
	$.ajax({
		 url: itemsUrl,
		 method: "GET",
		 headers: { "Accept": "application/json; odata=verbose" },
		 cache: false,
		 async: false,
		 dataType: "text",
		 success: function(data){
			var results = (data.d.results == null) ? new Array(data.d) : data.d.results;
			results = data.d.results;
			$(results).each(function(){ if($(this)[0].LoginName == global_user){matchfound = true;} });
		 },
		 error:function () {
			  console.log("Error");
		  }
		}
	);
	return matchfound;
}