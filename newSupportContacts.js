// Javascript library for adding Page Support
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/js/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/js/";
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}

jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof loadProfileData == 'function' && typeof getNamesOptionsList == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {	
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
			overrideSharepointStyles();
			getWebUserData();
			loadProfileData();
			getNamesOptionsList();
			var intervalStart = setInterval(setWindowValues, 500);	
			function setWindowValues(){
				if (loadNameDone == true && loadProfileDataDone == true){	
					var savebuttontext = "Save";
					var updateType = "post";
					if (curId != false) {	
						loadItemData(curId);
						savebuttontext = "Update";
						updateType = "update";	
					};
					$("#q_cancelbutton").click( function() { cancelModalWindow(); });
					$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation(updateType)});
					$("#q_grpemail").after("  @kp.org");
					setRibbonGroupVisibility_PT();
					$("#q_hideinputfields").removeClass("q_hide");
					$(".se-pre-con").fadeOut("slow");
					clearInterval(intervalStart);
				}
			}	
		},"sp.ui.dialog.js");	
	},"sp.js");
}

function fieldValidation(updateType) {	
	var isValid = true;
	$("#q_errormessage").text("");
	var nameval = $("#q_listNames option:selected").text();
	var grpemail = $("#q_grpemail").val();
	if (nameval == "" && grpemail == "") {	
		var error_node = $('<p>').text('Either a Contact Person OR a Group Email Name is required.');
		$("#q_errormessage").append(error_node);
		isValid = false;
	}; 
	var validEmail = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
	if (grpemail.indexOf('@') > 0 || validEmail.test(grpemail)) {	
		var error_node = $('<p>').text('Email needs to be like "specimen" without "&" or ".com"');
		$("#q_errormessage").append(error_node);
		isValid = false;
	}; 
	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		postItem(updateType);
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}
	
function postItem(updateType){	
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Support Contacts'); 
	if (updateType == "post") {
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	} else {
		this.oListItem = oList.getItemById(curId);
	}
	if ($("#q_listNames option:selected").text() != "") {
		var singleUser = SP.FieldUserValue.fromUser($("#q_listNames option:selected").text());
		this.oListItem.set_item('ContactName', singleUser);
	}
	if ( $('#q_grpemail').val().length > 0) { this.oListItem.set_item('GroupEmailName',  $('#q_grpemail').val()); }
	this.oListItem.set_item('ContactType', $("#q_contacttype input:checked").val());
	this.oListItem.set_item('Title', "Support Contact");
	if (profiledata[0] != "" && profiledata[0] != null) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1] != "" && profiledata[1] != null) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2] != "" && profiledata[2] != null) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3] != "" && profiledata[3] != null) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4] != "" && profiledata[4] != null) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5] != "" && profiledata[5] != null) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	this.oListItem.set_item('PublishingApproval', "Draft");  
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
} 

function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function onItemPostQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function loadItemData(curId) {
	var clientContext = new SP.ClientContext(casiteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Support Contacts');
	this.oListItem = oList.getItemById(curId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceeded(sender, args) {
	var get_contact_type = oListItem.get_item("ContactType");
	var contactStr = oListItem.get_item("ContactName");
	if(contactStr == null || contactStr == ''){
		$('#q_grpemail').val(oListItem.get_item('GroupEmailName'));
		$("#q_listNames").closest(".q_optionalData").hide();
	}
	$("#q_contacttype input").each(function(){
    if($(this).val() == get_contact_type){
       	$(this).prop('checked', true);
       } 
    });
	$("#q_listNames option").each(function(){
    if($(this).text() == contactStr.$2e_1){
       	$(this).prop('selected', true);
		$("#q_grpemail").closest(".q_optionalData").hide();
       } 
    });

}