function histDataDel_Docs () {

	alert("gran: " + granularity + " freq: " + frequency + " visibleHist: " + visibleHistory);
	if (granularity != "Region") {
		fetchDataDelDocs();
	} else { 
		getDataDel_HistDocs();
	}

	function fetchDataDelDocs() {
		alert("fetch select list");
		var rptDt = new Date();
		rptDt.setDate(rptDt.getDate() -180); // to make sure we don't fetch too many records, get only the past 6 months
		var dateFilterStr = (rptDt.getMonth() + 1) + "/" + rptDt.getDate() + "/" + rptDt.getFullYear();
		var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,MedicalCtrValue";
		var orderbyStr = "$orderby=ReportDataDate%20desc,MedicalCtrValue";
		var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27";	
		var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
		var executor = new SP.RequestExecutor(catUrl);
		alert("itemsUrl: " + itemsUrl);
		executor.executeAsync(
			{
			  url: itemsUrl,
			  method: "GET",
			  headers: { "Accept": "application/json; odata=verbose" },
			  success: loadLocNameValues,
			  error: errorHandler
			}
		);		
	}
	
	function loadLocNameValues(data) {
		var jsonObject = JSON.parse(data.body);
		var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
		for (var i = 0; i < results.length; i++ ){
		}
		var mcarray = [];
		for (var i = 0; i < results.length; i++) {
			mcarray[i] = results[i].MedicalCtrValue;
		};
		var unique = mcarray.filter(function(itm,i,a){
			return i==a.indexOf(itm);
		});
		mcarray = unique;
		mcarray.sort();
		for (var i = 0; i < mcarray.length; i++ ){
		  var text = mcarray[i];
		  var opt = $("<option>").text(text);
		  if (i == 0) { opt.attr("selected", "selected"); }
		  opt.val(text);
		  $("#select_report").append(opt);
		}
		getDataDel_HistDocs(); 
		$('#select_report').change(function() { getDataDel_HistDocs(); } );  
	}
	
	function getDataDel_HistDocs() {
		  var jsonObject = JSON.parse(data.body);
		  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
		  var curYear = new Date().getFullYear();
		  var dateFilterStr = "12/31/" + (curYear-3);
		  var selectedmedctr = $("#select_report option:selected").text();
		  var selectStr = "$select=ID,FileLeafRef,FileRef,ReleaseDate,ReportDataDate,MedicalCtrValue,ServiceAreaValue,FacilityValue";
		  var orderbyStr = "$orderby=ReportDataDate%20desc";
		  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27%20and%20ReportDataDate%20gt%20%27" + dateFilterStr + "%27%20and%20";
		  if (granularity == "Medical Center") { 
		  		orderbyStr = orderbyStr + ",MedicalCtrValue";
				filterby = filterby + "MedicalCtrValue%20eq%20%27" + selectedmedctr + "%27";
		  }
		  if (granularity == "Service Area") { 
		  		orderbyStr = orderbyStr + ",ServiceAreaValue";
				filterby = filterby + "ServiceAreaValue%20eq%20%27" + selectedmedctr + "%27"; 
		  }
		  if (granularity == "Facility") { orderbyStr = orderbyStr + ",FacilityValue";
				filterby = filterby + "FacilityValue%20eq%20%27" + selectedmedctr + "%27"; 
		  }
		  	
		  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
		  var executor = new SP.RequestExecutor(catUrl);
		  executor.executeAsync(
			  {
				url: itemsUrl,
				method: "GET",
				headers: { "Accept": "application/json; odata=verbose" },
				success: loadDataDel_histMo,
				error: errorHandler
			  }
		  );		
	}
	
	
	function loadDataDel_histMo(data) {
		var jsonObject = JSON.parse(data.body);
		var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
		var lastYear = "";	
		var curYear = new Date().getFullYear();
		$("#q_histList").empty();
		$("#q_medctrhistorytitle").empty();
		$("#q_medctrhistorytitle").append($("<h2>").text("Historical Reports for " + $("#select_report option:selected").text()));
		for (var i = 0; i < results.length; i++ ){
			var newYear = new Date(results[i].ReportDataDate).getFullYear();			
			if ((curYear - newYear) < 3) {	
				var idval = "q_row";
				if (granularity == "Monthly" || granularity == "Quarterly") {
					if (lastYear == "" || lastYear != newYear) {
						lastYear = new Date(results[i].ReportDataDate).getFullYear();
						var colNode = $("<div/>").addClass("q_delhistrow");
						colNode.append($("<div>").addClass("q_delhistrow_title").append($("<h3>").text(lastYear)));
						colNode.append($("<div>").addClass("q_delhistrow_content").append($("<div/>").addClass("q_colcontainer_4").attr("id", idval + lastYear)));
						$("#q_histList").append(colNode);	
					}
				}
				setHistContentItem("#" + idval + lastYear, results[i].ReportDataDate, results[i].FileRef, results[i].FileLeafRef);	
			}
		}
	}

	function setHistContentItem (idval, reportDate, fileRef, filename) {
		var linkhref = catroot + fileRef;
		var source = getIconFile(filename);
		var curRptDate = new Date(reportDate);
		if (frequency == "Quarterly") {
			var qtrStr = Math.floor((curRptDate.getMonth() + 3) / 3);
			var periodStr = "Q" + qtrStr;
		}
		if (frequency == "Monthly") {
			var qtrStr = curRptDate.getMonth() + 1;
			Date.prototype.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
			Date.prototype.getMonthName = function() {return this.monthNames[this.getMonth()];};
			var periodStr = curRptDate.getMonthName() + " " + curRptDate.getFullYear();
		}
		if (frequency == "Weekly") {
			var formattedRptDate = (curRptDate.getMonth() + 1) + "/" + curRptDate.getDate() + "/" + curRptDate.getFullYear();
			var periodStr = "Wk of " + formattedRptDate;
		}
		var divNode1 = $("<div>");
		var divNode2 = $("<div>").addClass("q_column_aligntop");
		var aNode2 = $("<a/>").attr({"title" : periodStr, "href" : linkhref});
		var divNode3 = $("<div>").addClass("q_column_linkwidth");
		var aNode3 = $("<a/>").attr({"title" : periodStr, "href" : linkhref}).text(periodStr);
		var imgNode = $("<img/>").attr({"src" : source});
		aNode2.append(imgNode);
		divNode2.append(aNode2);
		divNode3.append(aNode3);
		divNode1.append(divNode2, divNode3);
		$(idval).append(divNode1);
	}
}
