// Javascript library for adding Page Deliverables
var parentsite = document.URL.split("/")[4];
if (parentsite.indexOf("stg") > 0) {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosstgcat/_catalogs/masterpage/QOS/js/";
} else {
	var QOSscriptbase = "https://sites.sp.kp.org/pub/qosdevcat/_catalogs/masterpage/QOS/js/";
}	
if (document.URL.indexOf("IsDlg") < 0 ){
	$(".ms-breadcrumb-top").each( function() {
		$(this).addClass("q_ca_navbar_backgroundcolor");
	});
	$("#q_spinner").addClass("q_hide");
} else {
	$("#q_spinner").removeClass("q_hide");
}
 
jQuery(document).ready(function ($) {
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		$.getScript( QOSscriptbase + "jQuery.spHtmlEditor.js", function () {console.log('jQuery.spHtmlEditor loaded');});
		$.getScript( QOSscriptbase + "qos_ca_commonscripts.js",function () {console.log('qos_ca_commonscripts loaded');} );
	}, 'sp.ribbon.js');
	var jsInterval = setInterval(waitforJS, 200);
	function waitforJS() {
		if (typeof overrideSharepointStyles == 'function' && typeof getWebUserData == 'function' && typeof loadProfileData == 'function' && typeof getNamesOptionsList == 'function' && typeof getDocCategoriesOptionList == 'function' && typeof getTemplateOptionList == 'function') {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
});

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	SP.SOD.executeFunc('sp.js', 'SP.ClientContext');
	SP.SOD.executeFunc('sp.ui.dialog.js', 'SP.UI.ModalDialog.commonModalDialogClose');
	SP.SOD.executeOrDelayUntilScriptLoaded(function () {
		SP.SOD.executeOrDelayUntilScriptLoaded(function () {
				overrideSharepointStyles();
				getWebUserData();
				loadProfileData();
				getNamesOptionsList();
				getDocCategoriesOptionList(false);
				getTemplateOptionList();
				var intervalStart = setInterval(setWindowValues, 500);	
				function setWindowValues(){
					if(loadNameDone == true && loadProfileDataDone == true && loadDocCategoryDone == true && loadTemplateOptionsDone){	
						var savebuttontext = "Save";
						var updateType = "post";
						var select_v3 = $("#q_pageTemplate option[value='3']");
						select_v3.remove().clone();
						$("#q_pageTemplate").prop("disabled", false);
						$("#q_listCategories option[value='7']").prop("selected",true);	
						if (curId != false) {	
							loadItemData(curId);
							savebuttontext = "Update";
							updateType = "update";
						};
						$("#q_taggedtext").spHtmlEditor({version: "onprem" });						
						$('#q_taggedtext').click(function() {setRibbonGroupVisibility_FT();});
						setRibbonGroupVisibility_PT();
						$("#q_cancelbutton").click(function(){ cancelModalWindow() });
						$("#q_submitbutton").text(savebuttontext).click(function() {fieldValidation(updateType)});
						$('.DCContent div').attr('DefaultPasteModeResolver', 'PasteClean');
						$("#q_delmethod").change(function() {  
							if( $("#q_delmethod input:checked").val() != "Email"){
								$("#q_pageTemplate option[value='3']").remove();
								$("#q_pageTemplate").prop("disabled", false);
								select_v3.prop("selected",false);
							} else {
								select_v3.appendTo("#q_pageTemplate");
								select_v3.prop("selected",true);
								$("#q_pageTemplate").prop("disabled", true);
							}
						});
						$("#q_frequency").change(function() { 
							if($(this).val() == "Weekly"){
							   $("#weeks").closest(".q_tbl-editrow").removeClass("q_hide");
							}else{
							   $("#weeks").closest(".q_tbl-editrow").addClass("q_hide");
							}
						});
						$("#q_hideinputfields").removeClass("q_hide");
						$(".se-pre-con").fadeOut("slow");
						clearInterval(intervalStart);
				   }
				}							
		},"sp.ui.dialog.js");
	},"sp.js");
}

function fieldValidation(updateType){	
	// required values are Text Category, Title, and Content Text Block.
	var isValid = true;
	$("#q_errormessage").text("");
	if ($('#q_title').val() == null || $('#q_title').val() == "") {
			var error_node = $('<p>').text('A name for this Data Deliverable Profile is required.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 
	var othervar = $('#q_taggedtext').spHtmlEditor("gethtml");
	if (othervar.length < 7) {
			var error_node = $('<p>').text('A brief general description is required. Web audiences will see this description.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}; 	
	if ($("#q_listNames option:selected").text() == "") { 
			var error_node = $('<p>').text('A Support Contact must be assigned to this Data Deliverable Profile.');
			$("#q_errormessage").append(error_node);
			isValid = false;
	}
	if( $("#q_listCategories option:selected").text() == "Report"){
		if ($("#q_pageTemplate option:selected").text() == "") { 
				var error_node = $('<p>').text('A Page Template must be assigned to this Data Deliverable Profile.');
				$("#q_errormessage").append(error_node);
				isValid = false;
		}
	}
	if (isValid == true){
		$("#q_errormessage").removeClass('q_show');
		$("#q_errormessage").addClass('q_hide');
		$(".se-pre-con").fadeIn("slow");
		$("#q_spinner").removeClass('q_hide');
		postItem(updateType);
	} else {
		$("#q_errormessage").addClass('q_show');
	}
	return isValid;
}

function postItem(updateType) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Deliverables'); 
	if (updateType == "post") {
		var itemCreateInfo = new SP.ListItemCreationInformation();
		this.oListItem = oList.addItem(itemCreateInfo);
	} else {
		this.oListItem = oList.getItemById(curId);
	}
	var singleUser = SP.FieldUserValue.fromUser($("#q_listNames option:selected").text());	
	this.oListItem.set_item('ContactName', singleUser);
	var catId = new SP.FieldLookupValue();
	catId.set_lookupId(parseInt($("#q_listCategories option:selected").val()));	
	this.oListItem.set_item('LookupDocCategory', catId);
	this.oListItem.set_item('CategoryValue', $("#q_listCategories option:selected").text());
	this.oListItem.set_item('QOSTextContent', $('#q_taggedtext').spHtmlEditor("gethtml"));
	this.oListItem.set_item('DeliveryMethod', $("#q_delmethod input:checked").val());
	this.oListItem.set_item('RptFrequency', $('#q_frequency option:selected').val());
	this.oListItem.set_item('FileGranularity', $('#q_filegranularity option:selected').val());
	if($("#q_frequency").val() == "Weekly"){
		this.oListItem.set_item('VisibleHistory', $('#weeks').val());
	}
	this.oListItem.set_item('DeliverableNameValue', $("#q_title").val());
	if ($("#q_PHI").is(":checked")){ var IsPHI = 1;} else { var IsPHI = 0;}	
	this.oListItem.set_item('IsPHI', IsPHI);
	var tempId = new SP.FieldLookupValue();
	tempId.set_lookupId(parseInt($("#q_pageTemplate option:selected").val()));	
	this.oListItem.set_item('LookupPageTemplate', tempId);
	this.oListItem.set_item('PageNameValue', $("#q_pageTemplate option:selected").attr("page_name"));
	if (profiledata[0] != "" && profiledata[0] != null) { this.oListItem.set_item('AOWNavString', profiledata[0]); }
	if (profiledata[1] != "" && profiledata[1] != null) { this.oListItem.set_item('PgmNavString', profiledata[1]); }
	if (profiledata[2] != "" && profiledata[2] != null) { this.oListItem.set_item('AreaOfWorkValue', profiledata[2]); }
	if (profiledata[3] != "" && profiledata[3] != null) { this.oListItem.set_item('InitiativeValue', profiledata[3]); }
	if (profiledata[4] != "" && profiledata[4] != null) { this.oListItem.set_item('ProgramValue', profiledata[4]); }
	if (profiledata[5] != "" && profiledata[5] != null) { this.oListItem.set_item('SubprogramValue', profiledata[5]); }
	if (profiledata[6] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[6]));	
		this.oListItem.set_item('LookupAOW', newId);
	}
	if (profiledata[7] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[7]));	
		this.oListItem.set_item('LookupInitiative', newId);
	}
	if (profiledata[8] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[8]));	
		this.oListItem.set_item('LookupProgram', newId); 
	}
	if (profiledata[9] != 0) {
		var newId = new SP.FieldLookupValue(); 
		newId.set_lookupId(parseInt(profiledata[9]));	
		this.oListItem.set_item('LookupSubProgram', newId); 
	}
	this.oListItem.set_item('PublishingApproval', "Draft"); 
	this.oListItem.update();
	clientContext.load(this.oListItem);
	clientContext.executeQueryAsync(
		Function.createDelegate(this, this.onItemPostQuerySucceeded), 
		Function.createDelegate(this, this.onItemPostQueryFailed)
	);
} 
	
function onItemPostQuerySucceeded() {
	if (document.URL.indexOf("IsDlg") < 0 ){ $(".se-pre-con").fadeOut("slow"); alert("Item has been Updated");}
	var popData = [];
	SP.UI.ModalDialog.commonModalDialogClose(1, popData);
}

function onItemPostQueryFailed(sender, args) {
    alert('Request failed. ' + args.get_message() + 
        '\n' + args.get_stackTrace());
	if (document.URL.indexOf("IsDlg") > 0 ){
		var popData = [];
		SP.UI.ModalDialog.commonModalDialogClose(1, popData);
	}
}

function loadItemData(curId) {
	var clientContext = new SP.ClientContext(carootUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Deliverables');
	this.oListItem = oList.getItemById(curId);
	clientContext.load(oListItem);      
	clientContext.executeQueryAsync(Function.createDelegate(this, this.onLoadQuerySucceeded), Function.createDelegate(this, this.onQueryFailed)); 
}

function onLoadQuerySucceeded(sender, args) {
	$('#q_taggedtext').spHtmlEditor({method: "sethtml", html: oListItem.get_item("QOSTextContent")});
	$('#q_title').val(oListItem.get_item('DeliverableNameValue'));
	var IsPHI = oListItem.get_item("IsPHI");
	if(IsPHI == 1){ $("#q_PHI").prop('checked', true); }
	var delmethod = oListItem.get_item("DeliveryMethod");
	$("#q_delmethod input").each(function(){
		if($(this).val() == delmethod){
			$(this).prop('checked', true);
		   } 
    });
	var select_v3 = $("#q_pageTemplate option[value='3']");
	if( $("#q_delmethod input:checked").val() != "Email"){
		$("#q_pageTemplate").prop("disabled", false);
		select_v3.remove();
		select_v3.prop("selected",false);
	} else {
		var select_v3 = $("<option/>").attr({"page_name" : "rptemail.aspx", "value" : "3"}).text("Email and Non-Web Files");
		$("#q_pageTemplate").append(select_v3);
		select_v3.prop("selected",true);
		$("#q_pageTemplate").prop("disabled", true);
	}
	var frequency = oListItem.get_item('RptFrequency');
	$("#q_frequency option").each(function(){
		if($(this).val() == frequency){
			$(this).prop('selected', true);
		   } 
    });
	if (oListItem.get_item('ContactName') != null) {
		var contactname = oListItem.get_item("ContactName").get_lookupValue();
		var contactId = oListItem.get_item("ContactName").get_lookupId();
		$("#q_listNames option").each(function(){
			if($(this).text() == contactname){
				$(this).prop("selected",true);
			   } 
		});
	}
	var catId = oListItem.get_item("LookupDocCategory").get_lookupId();
    $("#q_listCategories option").each(function(){
		if($(this).val() == catId){
			$(this).prop("selected",true);
		   } 
    });
	
	var tempId = oListItem.get_item("LookupPageTemplate").get_lookupId();
    $("#q_pageTemplate option").each(function(){
		if($(this).val() == tempId){
			$(this).prop("selected",true);
		   } 
    });
}
