function getlistDataDel_listMedCtr(){
	var h2_title = $("<h2/>");
	var title = "Historical Reports";
	var h4_subtitle = $("<h4/>");
	var subtitle = "Change Med Center";
	var years_list = $("<div/>").addClass("years");
	var select_length = ["", "CVL", "CVV", "LCV"];
	for (var i = 0; i < select_length.length; i++) {
		var option_report = $("<option value=" + select_length[i] + ">" + select_length[i] + "</option>");
		$("#select_report").append(option_report);
	} 
	var select_title = $("<h4/>").attr("id" , "select_title");
	var q_listdel_curyr = $("<div/>").attr({"id" : "q_listmedctr_curyr"});
	var q_listdel_prioryr = $("<div/>").attr({"id" : "q_listmedctr_prioryr"});
	var q_listdel_2yrprior = $("<div/>").attr({"id" : "q_listmedctr_2yrprior"});
	var rptQtrHist_medctr = $("#q_rptQtrHist_medctr");
	h4_subtitle.append(subtitle);
	h2_title.append(title);
	rptQtrHist_medctr.append(h2_title, h4_subtitle, select_report, select_title, years_list);
	years_list.append(q_listdel_curyr, q_listdel_prioryr, q_listdel_2yrprior);
	getDataDel_listMedCtr();
	$("#select_report").on('change', function() {
		var _this = $(this).val();
		$("#select_title").text(_this);
	});
}

function getDataDel_listMedCtr() {
	  var selectStr = "$select=ID,DeliverableNavString";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items(" + curId + ")?" + selectStr;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: getDataDel_DocumentsMedCtr,
			error: errorHandler
		  }
	  );		
}

function getDataDel_DocumentsMedCtr(data) {
	  var jsonObject = JSON.parse(data.body);
	  var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	  var delnav = results[0].DeliverableNavString;
	  var selectStr = "$select=ID,Title,QOSTextContent,CategoryValue,FileLeafRef,FileDirRef,ReleaseDate,ReportDataDate";
	  var orderbyStr = "$orderby=ReportDataDate%20desc";
	  var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27";
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?" + selectStr + "&" + orderbyStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: loadListDataDel_DocumentsMedCtr,
			error: errorHandler
		  }
	  );		
}

function loadListDataDel_DocumentsMedCtr(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;

	var lastYear = "";	
	var curYear = new Date().getFullYear();
	for (var i = results.length; --i>=0;){
		var newYear = new Date(results[i].ReportDataDate).getFullYear();
		if ((curYear - newYear) < 3) {	
			if (lastYear == "" || lastYear != newYear) {
				lastYear = new Date(results[i].ReportDataDate).getFullYear();
				var difference = parseInt(curYear) - parseInt(lastYear);
				switch (difference) {
					case 0:
						idval = "q_listmedctr_curyr";
						break;
					case 1:
						idval = "q_listmedctr_prioryr"
						break;
					case 2:
						idval = "q_listmedctr_2yrprior"
						break;
				}
				setCategoryGrp_dataDoc("#" + idval, lastYear);
				$("#" + idval).append($("<div>").addClass("q_doclist_medctr"));	
			}
			setContentDocumentsMedCtr("#" + idval, results[i].ReleaseDate, results[i].FileDirRef, results[i].FileLeafRef);
		}
	}
}

function setCategoryGrp_dataDoc(idval, yearStr) {
	var h2Node = $("<h4>").text(yearStr);
	$(idval).append(h2Node);
}

function setContentDocumentsMedCtr(idval, releaseDate, fileUrl, filename) {
	var linkhref = catroot + fileUrl + "/" + filename;
	var source = ""; 
	switch (true) {
		case (filename.indexOf("xls") >= 0 || filename.indexOf("xlsm") >= 0):
			source = "/_layouts/15/images/icxls.png";
			break;
		case (filename.indexOf("xlxs") >= 0):
			source = "/_layouts/15/images/icxlxs.png";
			break;
		case (filename.indexOf("doc") >= 0):
			source = "/_layouts/15/images/icdoc.png";
			break;
		case (filename.indexOf("docx") >= 0):
			source = "/_layouts/15/images/icdocx.png";
			break;
		case (filename.indexOf("ppt") >= 0):
			source = "/_layouts/15/images/ppt.png";
			break;
		case (filename.indexOf("pptx") >= 0):
			source = "/_layouts/15/images/pptx.png";
			break;
		case (filename.indexOf("pdf") >= 0):
			source = "/_layouts/15/images/icpdf.png";
			break;
 		default:
			source = "/_layouts/15/images/ictxt.gif";
		}
	var imgNode = $("<img/>").attr("src", source);
	var get_date = new Date(releaseDate);
	var get_full_date = (get_date.getMonth() + 1) + "/" + get_date.getDate() + "/" + get_date.getFullYear();
	var get_quarter = get_date.getMonth() + 1;
	var aNode = $("<a/>").attr({"href" : linkhref, "target" : "_blank", "title" : get_full_date});
	var show_quarter ="";
	switch (true) {
		case (get_quarter < 4):
			show_quarter = "Q1";
			break;
		case (get_quarter < 7):
			show_quarter = "Q2";
			break;
		case (get_quarter < 10):
			show_quarter = "Q3";
			break;
		case (get_quarter < 13):
			show_quarter = "Q4";
			break;
	}
	aNode.append(imgNode);
	aNode.append(show_quarter);
	$(idval + " div").append(aNode);
}