function setNavPageLinks(){
	var titleNode = $("<h4/>").text("Topics");
	var ulNode = $("<ul>").attr("id", "q_boxlist_topics");
	$("#q_navtopiclinks").addClass("q_boxlist");
	$("#q_navtopiclinks").append(titleNode);
	$("#q_navtopiclinks").append(ulNode);
	
	
	if (document.URL.indexOf("about") < 0) { var linkhref = pagesUrl + "topicabout.aspx?carea="+ contentarea; } else { var linkhref = ""}
	setNavLinkPageRow(linkhref, "About", "Click to view measures for this content area", "about");
	if (document.URL.indexOf("report") < 0) { linkhref = pagesUrl + "topicreports.aspx?carea="+ contentarea; } else { linkhref = ""}
	setNavLinkPageRow(linkhref, "Reporting and Data", "Click to view measures for this content area", "reporting");
	if (document.URL.indexOf("topicmeasuregrps") < 0 || (document.URL.indexOf("topicmeasuregrps") > 0 && document.URL.indexOf("delnav") > 0) ) { linkhref = pagesUrl + "topicmeasuregrps.aspx?carea="+ contentarea; } else { linkhref = ""}
	setNavLinkPageRow(linkhref, "Measures", "Click to view measures for this content area", "measures");
	if (document.URL.indexOf("resource") < 0) { linkhref = pagesUrl + "topicresources.aspx?carea="+ contentarea; } else { linkhref = ""}
	setNavLinkPageRow(linkhref, "Resources", "Click to view measures for this content area", "resources");
	if (document.URL.indexOf("meeting") < 0) { linkhref = pagesUrl + "topicmeetings.aspx?carea="+ contentarea; } else { linkhref = ""}
	setNavLinkPageRow(linkhref, "Meetings", "Click to view measures for this content area", "meetings");


	aboutTextualContent();
	aboutDeliverables();
	aboutMeasures();
	aboutResources();
	aboutMeetings();
}

function aboutTextualContent() {
	  var selectStr = "$select=ID";
	  var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?$top=1&" + selectStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: function(data) {
				var jsonObject = JSON.parse(data.body);
				var results = jsonObject.d.results;
				if (results.length > 0){
                    $("#about").removeClass("q_hide");
				}
			},
			error: errorHandler
		  }
	  );		
}

function aboutMeetings() {
	  var selectStr = "$select=ID";
	  var filterby = "$filter=substringof(%27Meetings%27,CategoryValue)%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?$top=1&" + selectStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: function(data) {
				var jsonObject = JSON.parse(data.body);
				var results = jsonObject.d.results;
				if (results.length > 0){
					$("#meetings").removeClass("q_hide");
				}
			},
			error: errorHandler
		  }
	  );		
}

function aboutResources() {			
	  var selectStr = "$select=ID";
	  var filterby = "$filter=CategoryValue%20ne%20%27Reports%27%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Documents')/items?$top=1&" + selectStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: function(data) {
				var jsonObject = JSON.parse(data.body);
				var results = jsonObject.d.results;
				if (results.length > 0){
					$("#resources").removeClass("q_hide");
				}
			},
			error: errorHandler
		  }
	  );	
}

function aboutMeasures() {
	  var selectStr = "$select=ID";
	  var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Measure%20Groupings')/items?$top=1&" + selectStr  + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: function(data) {
				var jsonObject = JSON.parse(data.body);
				var results = jsonObject.d.results;
				if (results.length > 0){
					$("#measures").removeClass("q_hide");
					
				}
			},
			error: errorHandler
		  }
	  );		
}

function aboutDeliverables() {	
	  var selectStr = "$select=ID";
	  var filterby = "$filter=AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27";	
	  var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items?$top=1&" + selectStr + "&" + filterby;
	  var executor = new SP.RequestExecutor(catUrl);
	  executor.executeAsync(
		  {
			url: itemsUrl,
			method: "GET",
			headers: { "Accept": "application/json; odata=verbose" },
			success: function(data) {
				var jsonObject = JSON.parse(data.body);
				var results = jsonObject.d.results;
				if (results.length > 0){
					$("#reporting").removeClass("q_hide");	
				}
			},
			error: errorHandler
		  }
	  );
}

function setNavLinkPageRow (linkhref, displaytitle, hovertext, id) {
	var liNode = $("<li>").addClass("q_hide").attr("id", id);
	if (linkhref != "") {
		var aNode = $("<a>");
		aNode.attr("title", hovertext).text(displaytitle);
		aNode.attr("href", linkhref);
		aNode.attr("target", "_self");
		liNode.append(aNode);
	} else {
		liNode.addClass("q_onlink").text(displaytitle);
	}
	$("#q_boxlist_topics").append(liNode);
}