function getPageText_Banner () {
	var filterby = "$filter=CategoryValue%20eq%20%27Page%20Banner%27%20and%20(AOWNavString%20eq%20%27" + contentarea + "%27%20or%20PgmNavString%20eq%20%27" + contentarea + "%27)";	
	var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Textual%20Content')/items?" + filterby;
	var executor = new SP.RequestExecutor(catUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadPageText_Banner,
		  error: writeNoAccessMessage
		}
	);	
}

function loadPageText_Banner (data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	if (results.length <= 0) {
    	restrictedContent = true;
		writeNoAccessMessage();
  	} else {
		restrictedContent = false;
		for (var i = 0; i < results.length; i++) {
			var div_bannerouter = $("<div>").addClass("q_bannerbackground");
			var div_container = $("<div>").addClass("q_bannertitlelogo");
			var div_bannertext = $("<div>").addClass("q_pagebannertext");
			var div_bannerlogo = $("<div>").addClass("q_pagebannerlogo hide-on-mobile");
			var div_bannersponsor = $("<div>").attr({"id": "q_sponsorbar"}).html(results[i].Sponsors);
			div_bannertext.append($("<h1>").text(results[i].Title));
			div_bannerlogo.append($("<img>").attr({"src": results[i].SiteLogoImageValue + "?RenditionID=2"}));
			div_container.append(div_bannertext);
			div_container.append(div_bannerlogo);
			div_container.append(div_bannersponsor);
			div_bannerouter.append(div_container);
			$("#q_pageHeaderBar").append(div_bannerouter);

			var aownav = results[i].AOWNavString; 
			var pgmnav = results[i].PgmNavString; 
			var aowvalue = results[i].AreaOfWorkValue; 
			var initvalue = results[i].InitiativeValue;
			var pgmvalue = results[i].ProgramValue; 
			var subpgmvalue = results[i].SubprogramValue;
			var ulNode = $("<ul>");
			if(aownav != null && aownav != 'undefined'){
				var title1 = aowvalue;
				var link1 = pagesUrl;
				var title2 = initvalue;
				var link2 = pagesUrl + "topicabout.aspx?carea=" + aownav;
				var crumb1 = $("<li><a href='" + link1 + "' target='_self' title='" + title1 + "'>" + title1 + "</a></li>");
				var crumb2 = $("<li><a href='" + link2 + "' target='_self' title='" + title2 + "'>" + title2 + "</a></li>");
				ulNode.append(crumb1);
				ulNode.append(crumb2);
			} else {
				if (subpgmvalue != null && subpgmvalue != "undefined"){
					var title1 = pgmvalue;
					var link1 = pagesUrl;
					var title2 = subpgmvalue;
					var link2 = pagesUrl + "topicabout.aspx?carea=" + pgmnav;
					var crumb1 = $("<li><a href='" + link1 + "' target='_self' title='" + title1 + "'>" + title1 + "</a></li>");
					var crumb2 = $("<li><a href='" + link2 + "' target='_self' title='" + title2 + "'>" + title2 + "</a></li>");
					ulNode.append(crumb1);
					ulNode.append(crumb2);
				} else {
					var title1 = pgmvalue;
					var link1 = pagesUrl + "topicabout.aspx?carea=" + pgmnav;
					var crumb1 = $("<li><a href='" + link1 + "' target='_self' title='" + title1 + "'>" + title1 + "</a></li>");
					ulNode.append(crumb1);
				}
			}	
			$("#qos-NavBreadcrumbList").append(ulNode);
		}
	}
	loadPageText_BannerDone = true;
}