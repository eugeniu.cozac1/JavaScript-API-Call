jQuery(document).ready(function ($) {
	var ourInterval = setInterval(loopInterval, 500);
	var jsInterval = setInterval(waitforJS, 500);
	function loopInterval(){
		if (siteType != "" || siteType != null){
			$.getScript(QOSscriptbase + "qos_pub_PageBanner.js", function() { console.log('qos_pub_PageBanner loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_Pages.js", function() { console.log('qos_pub_NavLinks_Pages loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_Deliverables.js", function() { console.log('qos_pub_NavLinks_Deliverables loaded');});
			$.getScript(QOSscriptbase + "qos_pub_NavLinks_WebLinks.js", function() { console.log('qos_pub_NavLinks_WebLinks loaded');});
			$.getScript(QOSscriptbase + "qos_pub_listSupportContacts.js", function() { console.log('qos_pub_listSupportContacts loaded');});
			$.getScript(QOSscriptbase + "qos_pub_HeaderDataDeliverable.js", function() { console.log('qos_pub_HeaderDataDeliverable loaded');});
			$.getScript(QOSscriptbase + "qos_pub_listDataDel_CurDocs.js", function() { console.log('qos_pub_listDataDel_CurDocs loaded');});
			$.getScript(QOSscriptbase + "qos_pub_listDataDel_HistDocs.js", function() { console.log('qos_pub_listDataDel_HistDocs loaded');});
			clearInterval(ourInterval);	
		}
	}
	function waitforJS() {
		if (typeof getPageText_Banner == 'function' && typeof setNavPageLinks == 'function' && typeof getNavDeliverableLinks == 'function' && typeof getNavWebLinks == 'function' && typeof getSupportContacts == 'function' && typeof getHeader_DataDel == 'function' && typeof currentDataDel_Docs == 'function' && typeof histDataDel_Docs == 'function'
		 ) {
			clearInterval(jsInterval);
			var scriptbase = "https://sites.sp.kp.org/pub/" + siteType + "/_layouts/15/";
			$.getScript(scriptbase + "SP.RequestExecutor.js", execCrossDomainRequest);
		}
	}
 });

function execCrossDomainRequest() {
	if (contentarea == false || contentarea == null || contentarea == "undefined") {
		return false;
	}
	getPageText_Banner();
	setDelDisplayCriteria();
	var waitInterval = setInterval(writePageContent, 200);
	function writePageContent() {
		if (loadPageText_BannerDone == true && loadDelCriteriaDone == true) {
			if(restrictedContent == false){
				setNavPageLinks();
				getNavDeliverableLinks();
				getNavWebLinks();
				getSupportContacts();
				getHeader_DataDel();
				currentDataDel_Docs();
				histDataDel_Docs();
			}
			clearInterval(waitInterval);
		}
	}
}

function setDelDisplayCriteria () {
	var selectStr = "$select=ID,VisibleHistory,RptFrequency,FileGranularity";
	var filterby = "$filter=DeliverableNavString%20eq%20%27" + delnav + "%27";	
	var itemsUrl = catUrl + "/_api/web/lists/getbytitle('Deliverables')/items?" + selectStr + "&" + filterby;
	var executor = new SP.RequestExecutor(catUrl);
	alert("itemsUrl: " + itemsUrl);
	executor.executeAsync(
		{
		  url: itemsUrl,
		  method: "GET",
		  headers: { "Accept": "application/json; odata=verbose" },
		  success: loadDeliverable,
		  error: errorHandler
		}
	);		
}

function loadDeliverable(data) {
	var jsonObject = JSON.parse(data.body);
	var results = (jsonObject.d.results == null) ? new Array(jsonObject.d) : jsonObject.d.results;
	frequency = results[0].RptFrequency;
	granularity = results[0].FileGranularity;
	visibleHistory = results[0].VisibleHistory;
	loadDelCriteriaDone = true;
}
